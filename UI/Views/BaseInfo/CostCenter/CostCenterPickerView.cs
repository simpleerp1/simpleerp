﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class CostCenterPickerView : UserControl, Controls.IEntityPickerView
    {
        public CostCenterPickerView()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.CostCenterService.Query";
            this.dgList.GetQueryArgsFunc = () => { return new object[] { null }; };
            this.Load += (s, e) => { this.LoadData(); };
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get
            {
                var id = this.dgList.SelectedEntityID;
                if (id == Guid.Empty)
                    return null;

                return new CostCenter(dps.Common.Data.Entity.Retrieve(CostCenter.EntityModelID, id));
            }
        }

        public void LoadData()
        {
            this.dgList.LoadData();
        }

        public void FocusInput()
        { }

        public void MoveUp()
        {
            mzerp.UI.Controls.DataGridNavigationHelper.MoveUp(this.dgList.GridView);
        }

        public void MoveDown()
        {
            mzerp.UI.Controls.DataGridNavigationHelper.MoveDown(this.dgList.GridView);
        }
    }
}
