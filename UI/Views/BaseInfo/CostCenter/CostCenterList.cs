﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class CostCenterList : UserControl
    {
        public CostCenterList()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.CostCenterService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;

            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            return new object[] { this.qpName.Text };
        }

        #region Event handlers
        private void OnCreate()
        {
            var view = new CostCenterView();
            view.Entity = new CostCenter();
            MainForm.Instance.ShowView(view, "成本中心信息");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new CostCenter(dps.Common.Data.Entity.Retrieve(CostCenter.EntityModelID, id));

            var view = new CostCenterView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "成本中心信息");
        }
        #endregion
    }
}
