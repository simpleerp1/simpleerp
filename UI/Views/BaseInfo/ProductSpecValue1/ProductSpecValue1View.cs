﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class ProductSpecValue1View : UserControl
    {

        private mzerp.Entities.ProductSpecValue1 _entity;
        public mzerp.Entities.ProductSpecValue1 Entity
        {
            get { return _entity; }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public ProductSpecValue1View()
        {
            InitializeComponent();

            this.btNew.Click += (s, e) => { Create(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.btDelete.Click += (s, e) => { Delete(); };
            this.tbValue.ValueChanged += OnValueChanged;
        }

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (tbValue.Value == 0)
                this.tbName.Text = "±0";
            else if (tbValue.Value > 0)
                this.tbName.Text = "+" + tbValue.Value.ToString();
            else
                this.tbName.Text = tbValue.Value.ToString();
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbValue.Value = _entity.Value;
            this.OnValueChanged(this.tbValue, EventArgs.Empty);
        }

        private void FlushData()
        {
            _entity.Name = this.tbName.Text;
            _entity.Value = (int)this.tbValue.Value;
        }

        private void Create()
        {
            this.Entity = new Entities.ProductSpecValue1();
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                dps.Common.SysService.Invoke("mzerp", "ProductSpecService", "SaveSpecValue1", _entity.Instance);
                _entity.Instance.AcceptChanges();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "ProductSpecService", "DeleteSpecValue1", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
