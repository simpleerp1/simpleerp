﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class ProductSpecValue1List : UserControl
    {
        public ProductSpecValue1List()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.ProductSpecService.QuerySpecValue1";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;

            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            return new object[] { this.qpName.Text };
        }

        #region Event handlers
        private void OnCreate()
        {
            var entity = new ProductSpecValue1();
            var view = new ProductSpecValue1View();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "产品规值");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new ProductSpecValue1(dps.Common.Data.Entity.Retrieve(ProductSpecValue1.EntityModelID, id));

            var view = new ProductSpecValue1View();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "产品规值");
        }
        #endregion
    }
}
