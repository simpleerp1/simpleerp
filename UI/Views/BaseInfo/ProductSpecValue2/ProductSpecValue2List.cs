﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class ProductSpecValue2List : UserControl
    {
        public ProductSpecValue2List()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.ProductSpecService.QuerySpecValue2";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;

            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            return new object[] { this.qpName.Text };
        }

        #region Event handlers
        private void OnCreate()
        {
            var entity = new ProductSpecValue2();
            var view = new ProductSpecValue2View();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "产品分规值");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new ProductSpecValue2(dps.Common.Data.Entity.Retrieve(ProductSpecValue2.EntityModelID, id));

            var view = new ProductSpecValue2View();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "产品分规值");
        }
        #endregion
    }
}
