﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class ProductLevelList : UserControl
    {
        public ProductLevelList()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.ProductLevelService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            var clName = new GridViewTextBoxColumn("级别名称","Name");
            clName.Width = 150;
            this.dgList.Columns.Add(clName);
            var clMemo = new GridViewTextBoxColumn("备注","Memo");
            clMemo.Width = 200;
            this.dgList.Columns.Add(clMemo);

            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            return new object[] { this.qpName.Text };
        }

        #region Event handlers
        private void OnCreate()
        {
            var entity = new ProductLevel();
            var view = new ProductLevelView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "产品级别");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new ProductLevel(dps.Common.Data.Entity.Retrieve(ProductLevel.EntityModelID, id));

            var view = new ProductLevelView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "产品级别");
        }
        #endregion

    }
}
