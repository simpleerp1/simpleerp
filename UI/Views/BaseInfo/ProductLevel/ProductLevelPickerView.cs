﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class ProductLevelPickerView : UserControl,Controls.IEntityPickerView
    {
        public ProductLevelPickerView()
        {
            InitializeComponent();

            this.dgList.AllowPage = false;
            this.dgList.QueryMethod = "mzerp.ProductLevelService.Query";
            this.dgList.GetQueryArgsFunc = () => { return new object[] { null }; };
            var clName = new GridViewTextBoxColumn("级别名称", "Name");
            clName.Width = 150;
            this.dgList.Columns.Add(clName);
            var clMemo = new GridViewTextBoxColumn("备注", "Memo");
            clMemo.Width = 200;
            this.dgList.Columns.Add(clMemo);
            this.Load += (s, e) => { this.LoadData(); };
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get {
                var id = this.dgList.SelectedEntityID;
                if (id == Guid.Empty)
                    return null;

                return new ProductLevel(dps.Common.Data.Entity.Retrieve(ProductLevel.EntityModelID, id));
            }
        }

        public void LoadData()
        {
            this.dgList.LoadData();
        }

        public void FocusInput()
        { }

        public void MoveUp()
        {
            mzerp.UI.Controls.DataGridNavigationHelper.MoveUp(this.dgList.GridView);
        }

        public void MoveDown()
        {
            mzerp.UI.Controls.DataGridNavigationHelper.MoveDown(this.dgList.GridView);
        }
    }
}
