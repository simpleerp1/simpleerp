﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class ProductMaterialView : UserControl
    {

        private mzerp.Entities.ProductMaterial _entity;
        public mzerp.Entities.ProductMaterial Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public ProductMaterialView()
        {
            InitializeComponent();

            this.btNew.Click += (s, e) => { Create(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.btDelete.Click += (s, e) => { Delete(); };
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbName.Text = _entity.Name;
            this.tbMemo.Text = _entity.Memo;
        }

        private void FlushData()
        {
            _entity.Name = this.tbName.Text;
            _entity.Memo = this.tbMemo.Text;
        }

        private void Create()
        {
            this.Entity = new Entities.ProductMaterial();
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                if (string.IsNullOrEmpty(_entity.Name))
                    throw new System.Exception("级别名称不能为空");

                dps.Common.SysService.Invoke("mzerp", "ProductMaterialService", "Save", _entity.Instance);
                _entity.Instance.AcceptChanges();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "ProductMaterialService", "Delete", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
