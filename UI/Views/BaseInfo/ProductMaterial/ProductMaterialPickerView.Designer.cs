﻿namespace mzerp.UI
{
    partial class ProductMaterialPickerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.dgList = new mzerp.UI.Controls.DataGrid();
            this.SuspendLayout();
            // 
            // dgList
            // 
            this.dgList.AllowPage = true;
            gridViewTextBoxColumn1.FieldName = "Name";
            gridViewTextBoxColumn1.HeaderText = "材料名称";
            gridViewTextBoxColumn1.Name = "clName";
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.FieldName = "Memo";
            gridViewTextBoxColumn2.HeaderText = "备注";
            gridViewTextBoxColumn2.Name = "clMemo";
            gridViewTextBoxColumn2.Width = 200;
            this.dgList.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.PageSize = 20;
            this.dgList.Size = new System.Drawing.Size(393, 304);
            this.dgList.TabIndex = 4;
            // 
            // ProductMaterialPickerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgList);
            this.Name = "ProductMaterialPickerView";
            this.Size = new System.Drawing.Size(393, 304);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.DataGrid dgList;
    }
}
