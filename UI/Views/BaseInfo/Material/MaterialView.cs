﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class MaterialView : UserControl
    {

        private mzerp.Entities.Material _entity;
        public mzerp.Entities.Material Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public MaterialView()
        {
            InitializeComponent();

            this.tbCatalog.PickerView = new MaterialCatalogPickerView();
            this.tbCatalog.DisplayMember = "Name";

            this.btNew.Click += (s, e) => { Create(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.btDelete.Click += (s, e) => { Delete(); };
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbCode.Text = _entity.Code;
            this.tbName.Text = _entity.Name;
            this.tbSpec.Text = _entity.Spec;
            this.tbMeasureUnit.Text = _entity.MeasureUnit;
            this.tbUnitPrice.Value = _entity.StandardUnitPrice;
            this.tbCatalog.SelectedEntity = _entity.Catalog;
            this.tbLowerStocks.Value = _entity.LowerNotifyQuantity;
        }

        private void FlushData()
        {
            _entity.Code = this.tbCode.Text;
            _entity.Name = this.tbName.Text;
            _entity.Spec = this.tbSpec.Text;
            _entity.MeasureUnit = this.tbMeasureUnit.Text;
            _entity.StandardUnitPrice = this.tbUnitPrice.Value;
            _entity.ValuationMethod = Enums.InventoryValuationMethod.MovingAverage; //Todo:暂全部移动平均
            _entity.Catalog = (MaterialCatalog)this.tbCatalog.SelectedEntity;
            _entity.LowerNotifyQuantity = tbLowerStocks.Value;
        }

        public void Create()
        {
            this.Entity = new Entities.Material();
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                if (string.IsNullOrEmpty(_entity.Code) || string.IsNullOrEmpty(_entity.Name))
                    throw new System.Exception("编号或名称不能为空");

                dps.Common.SysService.Invoke("mzerp", "MaterialService", "Save", _entity.Instance);
                _entity.Instance.AcceptChanges();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "MaterialService", "Delete", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
       
    }
}
