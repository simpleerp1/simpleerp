﻿namespace mzerp.UI
{
    partial class MaterialView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.btDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbUnitPrice = new Telerik.WinControls.UI.RadSpinEditor();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMeasureUnit = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSpec = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCode = new Telerik.WinControls.UI.RadTextBox();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCatalog = new mzerp.UI.Controls.EntityPicker();
            this.label7 = new System.Windows.Forms.Label();
            this.tbLowerStocks = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbUnitPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMeasureUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCatalog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLowerStocks)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(461, 30);
            this.radCommandBar1.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btSave,
            this.btDelete});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "打开";
            this.btSave.AccessibleName = "打开";
            this.btSave.DisplayName = "commandBarButton1";
            this.btSave.DrawText = true;
            this.btSave.Image = global::mzerp.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btDelete
            // 
            this.btDelete.AccessibleDescription = "删除";
            this.btDelete.AccessibleName = "删除";
            this.btDelete.DisplayName = "commandBarButton2";
            this.btDelete.DrawText = true;
            this.btDelete.Image = global::mzerp.UI.Properties.Resources.Delete16;
            this.btDelete.Name = "btDelete";
            this.btDelete.Text = "删除";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox1.Controls.Add(this.tbLowerStocks);
            this.radGroupBox1.Controls.Add(this.label7);
            this.radGroupBox1.Controls.Add(this.tbUnitPrice);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.tbMeasureUnit);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.tbSpec);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.tbCode);
            this.radGroupBox1.Controls.Add(this.tbName);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.tbCatalog);
            this.radGroupBox1.HeaderText = "物料信息";
            this.radGroupBox1.Location = new System.Drawing.Point(3, 36);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(455, 340);
            this.radGroupBox1.TabIndex = 9;
            this.radGroupBox1.Text = "物料信息";
            // 
            // tbUnitPrice
            // 
            this.tbUnitPrice.DecimalPlaces = 4;
            this.tbUnitPrice.Location = new System.Drawing.Point(126, 200);
            this.tbUnitPrice.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.tbUnitPrice.Name = "tbUnitPrice";
            this.tbUnitPrice.Size = new System.Drawing.Size(272, 20);
            this.tbUnitPrice.TabIndex = 12;
            this.tbUnitPrice.TabStop = false;
            this.tbUnitPrice.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "标准单价：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(74, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "分类：";
            // 
            // tbMeasureUnit
            // 
            this.tbMeasureUnit.Location = new System.Drawing.Point(126, 130);
            this.tbMeasureUnit.Name = "tbMeasureUnit";
            this.tbMeasureUnit.Size = new System.Drawing.Size(272, 20);
            this.tbMeasureUnit.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "计量单位：";
            // 
            // tbSpec
            // 
            this.tbSpec.Location = new System.Drawing.Point(126, 96);
            this.tbSpec.Name = "tbSpec";
            this.tbSpec.Size = new System.Drawing.Size(272, 20);
            this.tbSpec.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "规格：";
            // 
            // tbCode
            // 
            this.tbCode.Location = new System.Drawing.Point(126, 28);
            this.tbCode.Name = "tbCode";
            this.tbCode.Size = new System.Drawing.Size(272, 20);
            this.tbCode.TabIndex = 4;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(126, 62);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(272, 20);
            this.tbName.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "编号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "名称：";
            // 
            // tbCatalog
            // 
            this.tbCatalog.DisplayMember = null;
            this.tbCatalog.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbCatalog.Location = new System.Drawing.Point(126, 164);
            this.tbCatalog.Name = "tbCatalog";
            this.tbCatalog.PickerView = null;
            this.tbCatalog.SelectedEntity = null;
            this.tbCatalog.Size = new System.Drawing.Size(272, 22);
            this.tbCatalog.TabIndex = 1;
            this.tbCatalog.TabStop = false;
            this.tbCatalog.ThemeName = "ControlDefault";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "库存下限提醒值：";
            // 
            // tbLowerStocks
            // 
            this.tbLowerStocks.DecimalPlaces = 4;
            this.tbLowerStocks.Location = new System.Drawing.Point(126, 234);
            this.tbLowerStocks.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.tbLowerStocks.Name = "tbLowerStocks";
            this.tbLowerStocks.Size = new System.Drawing.Size(272, 20);
            this.tbLowerStocks.TabIndex = 13;
            this.tbLowerStocks.TabStop = false;
            this.tbLowerStocks.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // MaterialView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "MaterialView";
            this.Size = new System.Drawing.Size(461, 379);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbUnitPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMeasureUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCatalog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLowerStocks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.CommandBarButton btDelete;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox tbMeasureUnit;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox tbSpec;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadTextBox tbCode;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private mzerp.UI.Controls.EntityPicker tbCatalog;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadSpinEditor tbUnitPrice;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadSpinEditor tbLowerStocks;
        private System.Windows.Forms.Label label7;

    }
}
