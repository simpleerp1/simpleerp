﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class MaterialList : UserControl
    {
        public MaterialList()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.MaterialService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.dgList.GridView.ShowGroupPanel = true;

            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.btImport.Click += (s, e) => { this.Import(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            return new object[] { this.qpName.Text };
        }

        #region Event handlers
        private void OnCreate()
        {
            var view = new MaterialView();
            view.Create();
            MainForm.Instance.ShowView(view, "物料信息");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new Material(dps.Common.Data.Entity.Retrieve(Material.EntityModelID, id));

            var view = new MaterialView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "物料信息");
        }
        #endregion

        private void Import()
        {
            var fd = new OpenFileDialog();
            var dr = fd.ShowDialog();
            if (dr != DialogResult.OK)
                return;

            Dictionary<string, mzerp.Entities.MaterialCatalog> catalogs = new Dictionary<string, Entities.MaterialCatalog>();
            Dictionary<string, mzerp.Entities.Material> materials = new Dictionary<string, Entities.Material>();
            int catalogCodeIndex = 0;
            System.IO.StreamReader reader = null;
            mzerp.Entities.MaterialCatalog catalog = null;
            try
            {
                reader = new System.IO.StreamReader(fd.FileName);
                reader.ReadLine(); //忽略第一行
                do
                {
                    var line = reader.ReadLine();
                    if (string.IsNullOrEmpty(line))
                        break;

                    string[] sr = line.Split(',');
                    try
                    {
                        var code = sr[0];
                        var name = sr[1];
                        var spec = sr[2];
                        var catalogName = sr[3];
                        var measure = sr[4];
                        var unitPrice = sr[5];

                        if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(catalogName) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(measure))
                            throw new System.Exception("编号或名称或分类或计量单位为空");

                        if (!catalogs.TryGetValue(catalogName, out catalog))
                        {
                            catalog = new Entities.MaterialCatalog();
                            catalogCodeIndex++;
                            catalog.Code = catalogCodeIndex.ToString("D2");
                            catalog.Name = catalogName;
                            catalogs.Add(catalogName, catalog);
                        }

                        if (materials.ContainsKey(code))
                            throw new System.Exception("物料编号重复");

                        var material = new mzerp.Entities.Material();
                        material.CatalogID = catalog.Instance.ID;
                        material.Code = code;
                        material.MeasureUnit = measure;
                        material.Name = name;
                        material.Spec = spec;
                        decimal up = 0m;
                        if (decimal.TryParse(unitPrice, out up))
                            material.StandardUnitPrice = up;
                        material.ValuationMethod = Enums.InventoryValuationMethod.MovingAverage;
                        materials.Add(code, material);
                    }
                    catch (Exception ex)
                    {
                        throw new System.Exception("导入文件行[" + line + "]格式错误：" + ex.Message);
                    }

                } while (true);

                //调用服务保存
                var cs1 = catalogs.Values.ToArray();
                dps.Common.Data.Entity[] cs2 = new dps.Common.Data.Entity[cs1.Length];
                for (int i = 0; i < cs1.Length; i++)
                {
                    cs2[i] = cs1[i].Instance;
                }
                var ms1 = materials.Values.ToArray();
                dps.Common.Data.Entity[] ms2 = new dps.Common.Data.Entity[ms1.Length];
                for (int i = 0; i < ms1.Length; i++)
                {
                    ms2[i] = ms1[i].Instance;
                }

                dps.Common.SysService.Invoke("mzerp", "MaterialService", "Import", cs2, ms2);
                this.dgList.LoadData();
                RadMessageBox.Show(this, "导入成功", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "导入错误：\r\n" + ex.Message, "导入错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                fd.Dispose();
            }
        }
    }
}
