﻿namespace mzerp.UI
{
    partial class MaterialList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaterialList));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.cmdBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btOpen = new Telerik.WinControls.UI.CommandBarButton();
            this.btDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.btImport = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.qpName = new Telerik.WinControls.UI.CommandBarTextBox();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.dgList = new mzerp.UI.Controls.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(595, 30);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.cmdBar});
            this.commandBarRowElement1.Text = "";
            // 
            // cmdBar
            // 
            this.cmdBar.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.cmdBar.DisplayName = "commandBarStripElement1";
            this.cmdBar.EnableDragging = false;
            this.cmdBar.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.cmdBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btOpen,
            this.btDelete,
            this.btImport,
            this.commandBarSeparator1,
            this.commandBarLabel1,
            this.qpName,
            this.btSearch});
            this.cmdBar.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.cmdBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.cmdBar.StretchHorizontally = true;
            this.cmdBar.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.cmdBar.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btOpen
            // 
            this.btOpen.AccessibleDescription = "打开";
            this.btOpen.AccessibleName = "打开";
            this.btOpen.DisplayName = "commandBarButton1";
            this.btOpen.DrawText = true;
            this.btOpen.Image = global::mzerp.UI.Properties.Resources.Notepad16;
            this.btOpen.Name = "btOpen";
            this.btOpen.Text = "编辑";
            this.btOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btOpen.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btDelete
            // 
            this.btDelete.AccessibleDescription = "删除";
            this.btDelete.AccessibleName = "删除";
            this.btDelete.DisplayName = "commandBarButton2";
            this.btDelete.DrawText = true;
            this.btDelete.Image = global::mzerp.UI.Properties.Resources.Delete16;
            this.btDelete.Name = "btDelete";
            this.btDelete.Text = "删除";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btImport
            // 
            this.btImport.AccessibleDescription = "导入";
            this.btImport.AccessibleName = "导入";
            this.btImport.DisplayName = "commandBarButton1";
            this.btImport.DrawText = true;
            this.btImport.Image = ((System.Drawing.Image)(resources.GetObject("btImport.Image")));
            this.btImport.Name = "btImport";
            this.btImport.Text = "导入";
            this.btImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btImport.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.AccessibleDescription = "级别：";
            this.commandBarLabel1.AccessibleName = "级别：";
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "编号或名称：";
            this.commandBarLabel1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // qpName
            // 
            this.qpName.DisplayName = "commandBarTextBox1";
            this.qpName.Name = "qpName";
            this.qpName.Text = "";
            this.qpName.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dgList
            // 
            this.dgList.AllowPage = true;
            gridViewTextBoxColumn1.FieldName = "Code";
            gridViewTextBoxColumn1.HeaderText = "编号";
            gridViewTextBoxColumn1.Name = "clCode";
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.FieldName = "Name";
            gridViewTextBoxColumn2.HeaderText = "名称";
            gridViewTextBoxColumn2.Name = "clName";
            gridViewTextBoxColumn2.Width = 150;
            gridViewTextBoxColumn3.FieldName = "Spec";
            gridViewTextBoxColumn3.HeaderText = "规格";
            gridViewTextBoxColumn3.Name = "clSpec";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.FieldName = "MeasureUnit";
            gridViewTextBoxColumn4.HeaderText = "计量单位";
            gridViewTextBoxColumn4.Name = "clMeasureUnit";
            gridViewTextBoxColumn4.Width = 60;
            gridViewTextBoxColumn5.FieldName = "Catalog";
            gridViewTextBoxColumn5.HeaderText = "分类";
            gridViewTextBoxColumn5.Name = "clCatalog";
            gridViewTextBoxColumn5.Width = 80;
            this.dgList.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 30);
            this.dgList.Name = "dgList";
            this.dgList.PageSize = 20;
            this.dgList.Size = new System.Drawing.Size(595, 376);
            this.dgList.TabIndex = 5;
            // 
            // MaterialList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "MaterialList";
            this.Size = new System.Drawing.Size(595, 406);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement cmdBar;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btOpen;
        private Telerik.WinControls.UI.CommandBarButton btDelete;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarTextBox qpName;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Controls.DataGrid dgList;
        private Telerik.WinControls.UI.CommandBarButton btImport;
    }
}
