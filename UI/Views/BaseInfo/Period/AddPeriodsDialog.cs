﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class AddPeriodsDialog : UserControl
    {
        public AddPeriodsDialog()
        {
            InitializeComponent();

            this.tbYear.Value = DateTime.Now.Year;

            this.btNew.Click += OnCreate;
        }

        private void OnCreate(object sender, EventArgs e)
        {
            try
            {
                dps.Common.SysService.Invoke("mzerp", "PeriodService", "Create", (int)tbYear.Value, (int)tbBeginMonth.Value, (int)tbEndMonth.Value);
                RadMessageBox.Show(this, "创建会计期间成功", "操作提示", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
            catch (Exception ex)
            {
               RadMessageBox.Show(this, "创建会计期间出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

    }
}
