﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class PeriodListView : UserControl
    {
        public PeriodListView()
        {
            InitializeComponent();

            this.qpYear.Text = DateTime.Now.Year.ToString();

            this.dgList.QueryMethod = "mzerp.PeriodService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;

            this.btNew.Click += OnCreate;
            this.btClose.Click += OnClosePeriod;
            this.btOpen.Click += OnOpenPeriod;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.Load += OnLoad;
        }

        private void OnOpenPeriod(object sender, EventArgs e)
        {
            if (this.dgList.GridView.SelectedRows.Count != 1)
            {
                RadMessageBox.Show(this, "请先选择需要打开的会计期间", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var row = ((DataRowView)this.dgList.GridView.SelectedRows[0].DataBoundItem).Row;
            if (row == null)
                return;

            this.DisableControlBeforeAction("正在打开会计期间，请稍候 ...");

            int year = (int)row["Year"];
            int month = (int)row["Month"];

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    dps.Common.SysService.Invoke("mzerp", "PeriodService", "OpenPeriod", year, month);
                    this.Invoke((MethodInvoker)delegate()
                    {
                        EnableControlAfterActionDone();
                        this.dgList.LoadData();
                        RadMessageBox.Show(this, "成功打开会计期间", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        EnableControlAfterActionDone();
                        RadMessageBox.Show(this, "打开会计期间出现异常：\r\n" + ex.Message, "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void OnClosePeriod(object sender, EventArgs e)
        {
            if (this.dgList.GridView.SelectedRows.Count != 1)
            {
                RadMessageBox.Show(this, "请先选择需要关闭的会计期间", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var row = ((DataRowView)this.dgList.GridView.SelectedRows[0].DataBoundItem).Row;
            if (row == null)
                return;

            this.DisableControlBeforeAction("正在关闭会计期间，请稍候 ...");

            int year = (int)row["Year"];
            int month = (int)row["Month"];

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    dps.Common.SysService.Invoke("mzerp", "PeriodService", "ClosePeriod", year, month);
                    this.Invoke((MethodInvoker)delegate()
                    {
                        EnableControlAfterActionDone();
                        this.dgList.LoadData();
                        RadMessageBox.Show(this, "成功关闭会计期间", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        EnableControlAfterActionDone();
                        RadMessageBox.Show(this, "关闭会计期间出现异常：\r\n" + ex.Message, "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void DisableControlBeforeAction(string watingBarInfo)
        {
            this.cmdBar.Enabled = false;
            this.dgList.Enabled = false;
            this.tbWatingBar.Visible = true;
            this.tbWatingBar.Text = watingBarInfo;
        }

        private void EnableControlAfterActionDone()
        {
            this.cmdBar.Enabled = true;
            this.dgList.Enabled = true;
            this.tbWatingBar.Visible = false;
        }

        private void OnCreate(object sender, EventArgs e)
        {
            var form = new AddPeriodsDialog();
            var dialog = new RadForm();
            dialog.Text = "创建会计期间";
            dialog.ShowInTaskbar = false;
            dialog.MaximizeBox = false;
            dialog.MinimizeBox = false;
            dialog.StartPosition = FormStartPosition.CenterParent;
            dialog.Controls.Add(form);
            dialog.Size = form.Size;
            dialog.ShowDialog(this);
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            return new object[] { this.qpYear.Text };
        }
    }
}
