﻿namespace mzerp.UI
{
    partial class PeriodListView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            mzerp.UI.Controls.EnumColumn enumColumn1 = new mzerp.UI.Controls.EnumColumn();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btFreeze = new Telerik.WinControls.UI.CommandBarButton();
            this.btClose = new Telerik.WinControls.UI.CommandBarButton();
            this.btOpen = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.qpYear = new Telerik.WinControls.UI.CommandBarTextBox();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.cmdBar = new Telerik.WinControls.UI.RadCommandBar();
            this.tbWatingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.dgList = new mzerp.UI.Controls.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWatingBar)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btFreeze,
            this.btClose,
            this.btOpen,
            this.commandBarSeparator1,
            this.commandBarLabel1,
            this.qpYear,
            this.btSearch});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btFreeze
            // 
            this.btFreeze.AccessibleDescription = "冻结";
            this.btFreeze.AccessibleName = "冻结";
            this.btFreeze.DisplayName = "commandBarButton2";
            this.btFreeze.DrawText = true;
            this.btFreeze.Image = global::mzerp.UI.Properties.Resources.Lock16;
            this.btFreeze.Name = "btFreeze";
            this.btFreeze.Text = "冻结";
            this.btFreeze.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btFreeze.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btClose
            // 
            this.btClose.AccessibleDescription = "关闭";
            this.btClose.AccessibleName = "关闭";
            this.btClose.DisplayName = "commandBarButton1";
            this.btClose.DrawText = true;
            this.btClose.Image = global::mzerp.UI.Properties.Resources.folder_close;
            this.btClose.Name = "btClose";
            this.btClose.Text = "关闭";
            this.btClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClose.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btOpen
            // 
            this.btOpen.AccessibleDescription = "打开";
            this.btOpen.AccessibleName = "打开";
            this.btOpen.DisplayName = "commandBarButton1";
            this.btOpen.DrawText = true;
            this.btOpen.Image = global::mzerp.UI.Properties.Resources.folder_open;
            this.btOpen.Name = "btOpen";
            this.btOpen.Text = "打开";
            this.btOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btOpen.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.AccessibleDescription = "级别：";
            this.commandBarLabel1.AccessibleName = "级别：";
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "年度：";
            this.commandBarLabel1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // qpYear
            // 
            this.qpYear.DisplayName = "commandBarTextBox1";
            this.qpYear.Name = "qpYear";
            this.qpYear.Text = "";
            this.qpYear.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // cmdBar
            // 
            this.cmdBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdBar.Location = new System.Drawing.Point(0, 0);
            this.cmdBar.Name = "cmdBar";
            this.cmdBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.cmdBar.Size = new System.Drawing.Size(507, 30);
            this.cmdBar.TabIndex = 2;
            // 
            // tbWatingBar
            // 
            this.tbWatingBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWatingBar.Location = new System.Drawing.Point(162, 174);
            this.tbWatingBar.Name = "tbWatingBar";
            this.tbWatingBar.ShowText = true;
            this.tbWatingBar.Size = new System.Drawing.Size(191, 29);
            this.tbWatingBar.TabIndex = 8;
            this.tbWatingBar.Text = "正在关闭会计期间，请稍候 ...";
            this.tbWatingBar.Visible = false;
            // 
            // dgList
            // 
            this.dgList.AllowPage = false;
            gridViewTextBoxColumn1.FieldName = "Code";
            gridViewTextBoxColumn1.HeaderText = "编号";
            gridViewTextBoxColumn1.Name = "clCode";
            gridViewTextBoxColumn1.Width = 100;
            gridViewDateTimeColumn1.FieldName = "BeginDate";
            gridViewDateTimeColumn1.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn1.HeaderText = "开始日期";
            gridViewDateTimeColumn1.Name = "clBeginDate";
            gridViewDateTimeColumn1.Width = 100;
            gridViewDateTimeColumn2.FieldName = "EndDate";
            gridViewDateTimeColumn2.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn2.HeaderText = "结束日期";
            gridViewDateTimeColumn2.Name = "clEndDate";
            gridViewDateTimeColumn2.Width = 100;
            enumColumn1.EnumModelID = "mzerp.PeriodState";
            enumColumn1.FieldName = "State";
            enumColumn1.HeaderText = "状态";
            enumColumn1.Name = "clState";
            enumColumn1.Width = 60;
            this.dgList.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            enumColumn1});
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 30);
            this.dgList.Name = "dgList";
            this.dgList.PageSize = 0;
            this.dgList.Size = new System.Drawing.Size(507, 383);
            this.dgList.TabIndex = 7;
            // 
            // PeriodListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tbWatingBar);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.cmdBar);
            this.Name = "PeriodListView";
            this.Size = new System.Drawing.Size(507, 413);
            ((System.ComponentModel.ISupportInitialize)(this.cmdBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWatingBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarTextBox qpYear;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Telerik.WinControls.UI.RadCommandBar cmdBar;
        private Controls.DataGrid dgList;
        private Telerik.WinControls.UI.CommandBarButton btOpen;
        private Telerik.WinControls.UI.CommandBarButton btFreeze;
        private Telerik.WinControls.UI.CommandBarButton btClose;
        private Telerik.WinControls.UI.RadWaitingBar tbWatingBar;

    }
}
