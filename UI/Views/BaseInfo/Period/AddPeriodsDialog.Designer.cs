﻿namespace mzerp.UI
{
    partial class AddPeriodsDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbYear = new Telerik.WinControls.UI.RadSpinEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbBeginMonth = new Telerik.WinControls.UI.RadSpinEditor();
            this.tbEndMonth = new Telerik.WinControls.UI.RadSpinEditor();
            this.btNew = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.tbYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBeginMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEndMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btNew)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "会计年度：";
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(115, 33);
            this.tbYear.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.tbYear.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(100, 20);
            this.tbYear.TabIndex = 1;
            this.tbYear.TabStop = false;
            this.tbYear.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "开始月份：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "结束月份：";
            // 
            // tbBeginMonth
            // 
            this.tbBeginMonth.Location = new System.Drawing.Point(115, 78);
            this.tbBeginMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.tbBeginMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbBeginMonth.Name = "tbBeginMonth";
            this.tbBeginMonth.Size = new System.Drawing.Size(100, 20);
            this.tbBeginMonth.TabIndex = 4;
            this.tbBeginMonth.TabStop = false;
            this.tbBeginMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbEndMonth
            // 
            this.tbEndMonth.Location = new System.Drawing.Point(115, 123);
            this.tbEndMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.tbEndMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbEndMonth.Name = "tbEndMonth";
            this.tbEndMonth.Size = new System.Drawing.Size(100, 20);
            this.tbEndMonth.TabIndex = 5;
            this.tbEndMonth.TabStop = false;
            this.tbEndMonth.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // btNew
            // 
            this.btNew.Location = new System.Drawing.Point(79, 178);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(110, 24);
            this.btNew.TabIndex = 6;
            this.btNew.Text = "开始创建";
            // 
            // AddPeriodsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btNew);
            this.Controls.Add(this.tbEndMonth);
            this.Controls.Add(this.tbBeginMonth);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.label1);
            this.Name = "AddPeriodsDialog";
            this.Size = new System.Drawing.Size(268, 245);
            ((System.ComponentModel.ISupportInitialize)(this.tbYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBeginMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEndMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btNew)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadSpinEditor tbYear;
        private Telerik.WinControls.UI.RadSpinEditor tbBeginMonth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadSpinEditor tbEndMonth;
        private Telerik.WinControls.UI.RadButton btNew;
    }
}
