﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class BizPartnerPickerView : UserControl, Controls.IEntityPickerView
    {
        private bool _hasCustomers = true;
        private bool _hasVendors = true;

        public BizPartnerPickerView(bool hasCustomers,bool hasVendors)
        {
            InitializeComponent();

            _hasCustomers = hasCustomers;
            _hasVendors = hasVendors;

            this.dgList.QueryMethod = "mzerp.BizPartnerService.Query";
            this.dgList.GetQueryArgsFunc = () => { return new object[] { this.qpName.Text, this._hasCustomers, this._hasVendors }; };
            this.btSearch.Click += (s, e) => { this.LoadData(); };
            this.Load += (s, e) => { this.LoadData(); };
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get
            {
                var id = this.dgList.SelectedEntityID;
                if (id == Guid.Empty)
                    return null;

                return new BizPartner(dps.Common.Data.Entity.Retrieve(BizPartner.EntityModelID, id));
            }
        }

        public void LoadData()
        {
            this.dgList.LoadData();
        }

        public void FocusInput()
        {
            this.qpName.Focus();
        }

        public void MoveUp()
        {
            mzerp.UI.Controls.DataGridNavigationHelper.MoveUp(this.dgList.GridView);
        }

        public void MoveDown()
        {
            mzerp.UI.Controls.DataGridNavigationHelper.MoveDown(this.dgList.GridView);
        }
    }
}
