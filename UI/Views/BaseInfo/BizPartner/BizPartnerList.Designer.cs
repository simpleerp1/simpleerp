﻿namespace mzerp.UI
{
    partial class BizPartnerList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btOpen = new Telerik.WinControls.UI.CommandBarButton();
            this.btDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.qpName = new Telerik.WinControls.UI.CommandBarTextBox();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.dgList = new mzerp.UI.Controls.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(709, 30);
            this.radCommandBar1.TabIndex = 2;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btOpen,
            this.btDelete,
            this.commandBarSeparator1,
            this.commandBarLabel1,
            this.qpName,
            this.btSearch});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btOpen
            // 
            this.btOpen.AccessibleDescription = "打开";
            this.btOpen.AccessibleName = "打开";
            this.btOpen.DisplayName = "commandBarButton1";
            this.btOpen.DrawText = true;
            this.btOpen.Image = global::mzerp.UI.Properties.Resources.Notepad16;
            this.btOpen.Name = "btOpen";
            this.btOpen.Text = "编辑";
            this.btOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btOpen.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btDelete
            // 
            this.btDelete.AccessibleDescription = "删除";
            this.btDelete.AccessibleName = "删除";
            this.btDelete.DisplayName = "commandBarButton2";
            this.btDelete.DrawText = true;
            this.btDelete.Image = global::mzerp.UI.Properties.Resources.Delete16;
            this.btDelete.Name = "btDelete";
            this.btDelete.Text = "删除";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.AccessibleDescription = "级别：";
            this.commandBarLabel1.AccessibleName = "级别：";
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "名称：";
            this.commandBarLabel1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // qpName
            // 
            this.qpName.DisplayName = "commandBarTextBox1";
            this.qpName.Name = "qpName";
            this.qpName.Text = "";
            this.qpName.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dgList
            // 
            this.dgList.AllowPage = true;
            gridViewTextBoxColumn1.FieldName = "Name";
            gridViewTextBoxColumn1.HeaderText = "单位名称";
            gridViewTextBoxColumn1.Name = "clName";
            gridViewTextBoxColumn1.Width = 200;
            gridViewCheckBoxColumn1.FieldName = "IsCustomer";
            gridViewCheckBoxColumn1.HeaderText = "客户";
            gridViewCheckBoxColumn1.Name = "clIsCustomer";
            gridViewCheckBoxColumn2.FieldName = "IsVendor";
            gridViewCheckBoxColumn2.HeaderText = "供应商";
            gridViewCheckBoxColumn2.Name = "clIsVendor";
            gridViewTextBoxColumn2.FieldName = "legalPerson";
            gridViewTextBoxColumn2.HeaderText = "法人";
            gridViewTextBoxColumn2.Name = "clLegalPerson";
            gridViewTextBoxColumn2.Width = 55;
            gridViewTextBoxColumn3.FieldName = "Tel1";
            gridViewTextBoxColumn3.HeaderText = "电话";
            gridViewTextBoxColumn3.Name = "clTel1";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "Fax";
            gridViewTextBoxColumn4.HeaderText = "传真";
            gridViewTextBoxColumn4.Name = "clFax";
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "Bank";
            gridViewTextBoxColumn5.HeaderText = "开户银行";
            gridViewTextBoxColumn5.Name = "clBank";
            gridViewTextBoxColumn5.Width = 150;
            gridViewTextBoxColumn6.FieldName = "BankAccount";
            gridViewTextBoxColumn6.HeaderText = "帐号";
            gridViewTextBoxColumn6.Name = "clBankAccount";
            gridViewTextBoxColumn6.Width = 120;
            gridViewTextBoxColumn7.FieldName = "Address";
            gridViewTextBoxColumn7.HeaderText = "地址";
            gridViewTextBoxColumn7.Name = "clAddress";
            gridViewTextBoxColumn7.Width = 150;
            this.dgList.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 30);
            this.dgList.Name = "dgList";
            this.dgList.PageSize = 20;
            this.dgList.Size = new System.Drawing.Size(709, 358);
            this.dgList.TabIndex = 3;
            // 
            // BizPartnerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "BizPartnerList";
            this.Size = new System.Drawing.Size(709, 388);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btOpen;
        private Telerik.WinControls.UI.CommandBarButton btDelete;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarTextBox qpName;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Controls.DataGrid dgList;
    }
}
