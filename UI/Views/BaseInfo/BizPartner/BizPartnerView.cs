﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class BizPartnerView : UserControl
    {

        private mzerp.Entities.BizPartner _entity;
        public mzerp.Entities.BizPartner Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public BizPartnerView()
        {
            InitializeComponent();

            this.btNew.Click += (s, e) => { Create(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.btDelete.Click += (s, e) => { Delete(); };
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbName.Text = _entity.Name;
            this.tbIsCustomer.Checked = _entity.IsCustomer;
            this.tbIsVendor.Checked = _entity.IsVendor;
            this.tbLegalPerson.Text = _entity.LegalPerson;
            this.tbTel1.Text = _entity.Tel1;
            this.tbFax.Text = _entity.Fax;
            this.tbAddress.Text = _entity.Address;
            this.tbBank.Text = _entity.Bank;
            this.tbBankAccount.Text = _entity.BankAccount;
            this.tbMemo.Text = _entity.Memo;

            this.tbIsCustomer.ReadOnly = !mzerp.Permissions.Customer_Edit.IsOwn;
            this.tbIsVendor.ReadOnly = !mzerp.Permissions.Vendor_Edit.IsOwn;
        }

        private void FlushData()
        {
            _entity.Name = this.tbName.Text;
            _entity.IsCustomer = this.tbIsCustomer.Checked;
            _entity.IsVendor = this.tbIsVendor.Checked;
            _entity.LegalPerson = this.tbLegalPerson.Text;
            _entity.Tel1 = this.tbTel1.Text;
            _entity.Fax = this.tbFax.Text;
            _entity.Address = this.tbAddress.Text;
            _entity.Bank = this.tbBank.Text;
            _entity.BankAccount = this.tbBankAccount.Text;
            _entity.Memo = this.tbMemo.Text;
        }

        private void Create()
        {
            this.Entity = new Entities.BizPartner();
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                if (_entity.IsCustomer == false && _entity.IsVendor == false)
                    throw new System.Exception("业务单位既不是客户也不是供应商");

                if (string.IsNullOrEmpty(_entity.Name))
                    throw new System.Exception("业务单位名称不能为空");

                dps.Common.SysService.Invoke("mzerp", "BizPartnerService", "Save", _entity.Instance);
                _entity.Instance.AcceptChanges();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "CustomerService", "Delete", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
