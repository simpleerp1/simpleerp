﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class WarehouseView : UserControl
    {

        private mzerp.Entities.Warehouse _entity;
        public mzerp.Entities.Warehouse Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public WarehouseView()
        {
            InitializeComponent();


            this.btNew.Click += (s, e) => { Create(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.btDelete.Click += (s, e) => { Delete(); };
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbCode.Text = _entity.Code;
            this.tbName.Text = _entity.Name;
        }

        private void FlushData()
        {
            _entity.Code = this.tbCode.Text;
            _entity.Name = this.tbName.Text;
        }

        public void Create()
        {
            var obj = new Entities.Warehouse();
            obj.DefaultLocation.Code = "Default";
            obj.DefaultLocation.Warehouse = obj;
            this.Entity = obj;
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                if (string.IsNullOrEmpty(_entity.Code) || string.IsNullOrEmpty(_entity.Name))
                    throw new System.Exception("编号或名称不能为空");

                dps.Common.SysService.Invoke("mzerp", "WarehouseService", "Save", _entity.Instance);
                _entity.Instance.AcceptChanges();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "WarehouseService", "Delete", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
