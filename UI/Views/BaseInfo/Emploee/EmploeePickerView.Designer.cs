﻿namespace mzerp.UI
{
    partial class EmploeePickerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvOrgUnits = new Telerik.WinControls.UI.RadTreeView();
            ((System.ComponentModel.ISupportInitialize)(this.tvOrgUnits)).BeginInit();
            this.SuspendLayout();
            // 
            // tvOrgUnits
            // 
            this.tvOrgUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvOrgUnits.Location = new System.Drawing.Point(0, 0);
            this.tvOrgUnits.Name = "tvOrgUnits";
            this.tvOrgUnits.Size = new System.Drawing.Size(297, 290);
            this.tvOrgUnits.SpacingBetweenNodes = -1;
            this.tvOrgUnits.TabIndex = 0;
            this.tvOrgUnits.Text = "radTreeView1";
            // 
            // EmploeePickerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tvOrgUnits);
            this.Name = "EmploeePickerView";
            this.Size = new System.Drawing.Size(297, 290);
            ((System.ComponentModel.ISupportInitialize)(this.tvOrgUnits)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTreeView tvOrgUnits;
    }
}
