﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class EmploeePickerView : UserControl, Controls.IEntityPickerView
    {
        public EmploeePickerView()
        {
            InitializeComponent();

            this.Load += (s, e) => { this.LoadData(); };
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get
            {
                var ou = this.tvOrgUnits.SelectedNode.Tag as OrgUnitInfo;
                if (ou == null)
                    return null;

                var emp = ou.Base as EmploeeInfo;
                if (emp == null)
                    return null;


                return new sys.Entities.Emploee(emp.Instance);
            }
        }

        public void LoadData()
        {
            OrgUnitManager.LoadOrgUnitTreeAsync(this.tvOrgUnits, null);
        }

        public void FocusInput()
        { }

        public void MoveUp()
        {
            if (this.tvOrgUnits.SelectedNode == null)
            {
                if (this.tvOrgUnits.Nodes.Count > 0)
                    this.tvOrgUnits.SelectedNode = this.tvOrgUnits.Nodes[0];
                return;
            }

            if (this.tvOrgUnits.SelectedNode.PrevNode != null)
            {
                this.tvOrgUnits.SelectedNode = this.tvOrgUnits.SelectedNode.PrevNode;
                return;
            }
        }

        public void MoveDown()
        {
            if (this.tvOrgUnits.SelectedNode == null)
            {
                if (this.tvOrgUnits.Nodes.Count > 0)
                    this.tvOrgUnits.SelectedNode = this.tvOrgUnits.Nodes[0];
                return;
            }

            if (this.tvOrgUnits.SelectedNode.NextNode != null)
            {
                this.tvOrgUnits.SelectedNode = this.tvOrgUnits.SelectedNode.NextNode;
                return;
            }
        }
    }
}
