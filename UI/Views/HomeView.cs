﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mzerp.UI
{
    public partial class HomeView : UserControl
    {
        public HomeView()
        {
            InitializeComponent();

            this.dgLowerStocks.QueryMethod = "mzerp.MaterialStockQueryService.GetLowerStocks";
            this.dgLowerStocks.GetQueryArgsFunc = () => { return null; };
            this.Load += (s, e) => { this.dgLowerStocks.LoadData(); };
        }
    }
}
