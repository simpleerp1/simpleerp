﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace mzerp.UI
{
    public partial class MaterialStockQuery : UserControl
    {
        public MaterialStockQuery()
        {
            InitializeComponent();

            //this.qpLevel.PickerView = new ProductLevelPickerView();
            //this.qpLevel.DisplayMember = "Name";

            this.dgList.GridView.ShowGroupPanel = true;
            this.dgList.QueryMethod = "mzerp.MaterialStockQueryService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btExport.Click += (s, e) => { ExportService.ExportGridViewToExcel(this.dgList.GridView, "当前库存表"); };

            //初始化分组汇总
            GridViewSummaryRowItem item1 = new GridViewSummaryRowItem();
            item1.Add(new GridViewSummaryItem("clValue", "合计:{0}", GridAggregateFunction.Sum));
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item1);
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item2);
            this.dgList.GridView.MasterTemplate.SummaryRowsBottom.Add(item1);
            this.dgList.GridView.MasterTemplate.ShowTotals = true;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[4];
            args[0] = Guid.Empty;
            args[1] = Guid.Empty;
            args[2] = Guid.Empty;
            args[3] = this.qpShowZero.Checked;
            return args;
        }
    }
}
