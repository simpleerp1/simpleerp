﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class PurchaseReturnList : UserControl
    {
        public PurchaseReturnList()
        {
            InitializeComponent();

            this.qpWarehouse.PickerView = new WarehousePickerView();
            this.qpWarehouse.DisplayMember = "Name";
            this.qpVendor.PickerView = new CostCenterPickerView();
            this.qpVendor.DisplayMember = "Name";
            this.qpTakeBy.PickerView = new EmploeePickerView();
            this.qpTakeBy.DisplayMember = "Name";
            this.dgList.QueryMethod = "mzerp.MaterialOutstockService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            //处理当前用的权限
            //this.btNew.Enabled = Permissions.ProductInstock_Create.IsOwn;
            //this.btOpen.Enabled = Permissions.ProductInstock_Edit.IsOwn;
            //this.btDelete.Enabled = Permissions.ProductInstock_Delete.IsOwn;

            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[6];
            if (qcDate.Checked)
            {
                args[0] = qpStartDate.Value;
                args[1] = qpEndDate.Value;
            }

            if (qcVendor.Checked)
                args[2] = qpVendor.SelectedEntityID;
            if (qcPurchaseBy.Checked)
                args[3] = qpTakeBy.SelectedEntityID;
            if (qcWharehouse.Checked)
                args[4] = qpWarehouse.SelectedEntityID;

            args[5] = (int)mzerp.Enums.MaterialOutstockType.PurchaseReturn;
            return args;
        }

        #region Event handlers
        private void OnCreate()
        {
            var view = new PurchaseReturnView();
            view.Create();
            MainForm.Instance.ShowView(view, "采购退货");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new MaterialOutstockTask(dps.Common.Data.Entity.Retrieve(MaterialOutstockTask.EntityModelID, id));

            var view = new PurchaseReturnView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "采购退货");
        }
        #endregion
        
    }
}
