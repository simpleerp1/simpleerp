﻿using dps.Common;
using mzerp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class PurchaseReturnPickForm : Telerik.WinControls.UI.RadForm
    {
        private mzerp.Entities.MaterialOutstockTask _task;
        public mzerp.Entities.MaterialOutstockTask Task
        {
            get { return _task; }
            set
            {
                _task = value;
                this.qpWarehouse.Text = _task.Warehouse.Name;
                this.qpVendor.Text = _task.BizPartner.Name;
            }
        }

        public PurchaseReturnPickForm()
        {
            InitializeComponent();

            this.qpMaterial.PickerView = new MaterialPickerView();
            this.qpMaterial.DisplayMember = "Code";
            this.qpMaterial.SelectedChanged += OnMaterialChanged;

            this.btSearch.Click += (s, e) => { this.Search(); };
            this.btAdd.Click += (s, e) => { this.AddToList(); };
            this.btClose.Click += (s, e) => { this.Close(); };
        }

        private void OnMaterialChanged(object sender, EventArgs e)
        {
            if (qpMaterial.SelectedEntity == null)
            {
                this.qpName.Text = null;
                this.qpSpec.Text = null;
                this.qpMeasureUnit.Text = null;
            }
            else
            {
                var m = (Material)qpMaterial.SelectedEntity;
                this.qpName.Text = m.Name;
                this.qpSpec.Text = m.Spec;
                this.qpMeasureUnit.Text = m.MeasureUnit;
            }
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[3];
            args[0] = this._task.BizPartnerID;
            args[1] = this._task.WarehouseID;
            if (this.qpMaterial.SelectedEntityID.HasValue)
                args[2] = this.qpMaterial.SelectedEntityID.Value;
            return args;
        }

        private object[] GetQueryArgs2()
        {
            object[] args = new object[2];
            args[0] = this._task.WarehouseID;
            if (this.qpMaterial.SelectedEntityID.HasValue)
                args[1] = this.qpMaterial.SelectedEntityID.Value;
            return args;
        }

        private void Search()
        {
            if (this.qpMaterial.SelectedEntity == null)
            {
                RadMessageBox.Show(this, "请先选择退货物料", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var args = GetQueryArgs();
            var args2 = GetQueryArgs2();
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var dt = dps.Common.SysService.Invoke("mzerp", "MaterialInstockService", "QueryForReturn", args) as dps.Common.Data.DataTable;
                    if (dt == null)
                        throw new System.Exception("查询结果不正确");
                    dt.Columns.Add("OutQuantity", typeof(Decimal));
                    Guid materialID = this.qpMaterial.SelectedEntityID.Value;
                    //检查存在的退回入库项
                    if (_task.Items.Count > 0)
                    {
                        Guid instockItemID;
                        //string lotNo;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            instockItemID = (Guid)dt.Rows[i]["ID"];
                            for (int j = 0; j < _task.Items.Count; j++)
                            {
                                if (_task.Items[j].MaterialID == materialID)
                                {
                                    var item = _task.Items[j];
                                    for (int k = 0; k < item.ReturnedItems.Count; k++)
                                    {
                                        if (item.ReturnedItems[k].InstockItemID == instockItemID)
                                        {
                                            dt.Rows[i]["OutQuantity"] = item.ReturnedItems[k].Quantity;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var dt2 = dps.Common.SysService.Invoke("mzerp", "MaterialStockQueryService", "QueryForOutstock", args2) as dps.Common.Data.DataTable;
                    if (dt2 == null)
                        throw new System.Exception("查询结果不正确");
                    dt2.Columns.Add("OutQuantity", typeof(Decimal));
                    //检查存在的已拣货量
                    if (_task.Items.Count > 0)
                    {
                        Guid locationID;
                        //string lotNo;
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            locationID = (Guid)dt2.Rows[i]["LocationID"];
                            for (int j = 0; j < _task.Items.Count; j++)
                            {
                                if (_task.Items[j].MaterialID == materialID)
                                {
                                    var item = _task.Items[j];
                                    for (int k = 0; k < item.Locations.Count; k++)
                                    {
                                        if (item.Locations[k].LocationID == locationID)
                                        {
                                            dt2.Rows[i]["OutQuantity"] = item.Locations[k].Quantity;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dgInstockItems.DataSource = dt;
                        this.dgItems.DataSource = dt2;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void AddToList()
        {
            var dt = this.dgInstockItems.DataSource as dps.Common.Data.DataTable;
            if (dt == null)
                return;
            var dt2 = this.dgItems.DataSource as dps.Common.Data.DataTable;
            if (dt2 == null)
                return;

            MaterialOutstockItem item = null;
            bool needAdd = false;
            decimal totalPickedQuantity = 0m;

            object outQuantityValue;
            decimal availableQuantity = 0m;
            decimal outQuantity = 0m;
            Guid materialID = this.qpMaterial.SelectedEntityID.Value;
            Guid locationID, instockItemID;
            //string lotNo;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                #region ----处理退货物料----
                outQuantityValue = dt.Rows[i]["OutQuantity"];
                if (outQuantityValue != DBNull.Value)
                {
                    availableQuantity = (decimal)dt.Rows[i]["AvaliableQuantity"];
                    instockItemID = (Guid)dt.Rows[i]["ID"];
                    outQuantity = (decimal)outQuantityValue;
                    if (outQuantity > 0)
                    {
                        //先查找有没有相同物料的出库项存在
                        for (int j = 0; j < _task.Items.Count; j++)
                        {
                            if (_task.Items[j].MaterialID == materialID)
                            {
                                item = _task.Items[j];
                                break;
                            }
                        }
                        if (item == null)
                        {
                            item = new MaterialOutstockItem();
                            item.MaterialID = materialID;
                            item.Quantity = 0m;
                            needAdd = true;//_task.Items.Add(item);
                        }

                        //再查找对应的ReturnedInstockItem并计算差异量
                        decimal diff = 0m;
                        bool founded = false;
                        for (int k = 0; k < item.ReturnedItems.Count; k++)
                        {
                            //Todo:注意：个别计价需要判断批号是否相同
                            if (item.ReturnedItems[k].InstockItemID == instockItemID)
                            {
                                decimal oldvalue = item.ReturnedItems[k].Quantity;
                                item.ReturnedItems[k].Quantity = outQuantity;
                                diff = outQuantity - oldvalue;
                                founded = true;
                                break;
                            }
                        }
                        if (founded)
                            item.Quantity += diff;
                        else
                        {
                            var returnedItem = new MaterialOutstockReturnedItem();
                            returnedItem.InstockItemID = instockItemID;
                            returnedItem.Quantity = outQuantity;
                            item.ReturnedItems.Add(returnedItem);
                            item.Quantity += outQuantity;
                        }
                    }
                }
                #endregion
            }

            //注意：前面已经查找过
            if (item == null)
            {
                RadMessageBox.Show(this, "请先选择退货物料及数量", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                #region ----处理拣货清单----
                outQuantityValue = dt2.Rows[i]["OutQuantity"];
                if (outQuantityValue != DBNull.Value)
                {
                    availableQuantity = (decimal)dt2.Rows[i]["Quantity"];
                    outQuantity = (decimal)outQuantityValue;
                    locationID = (Guid)dt2.Rows[i]["LocationID"];
                    if (outQuantity > 0)
                    {
                        //查找对应的location并计算总的出库数量
                        bool founded = false;
                        for (int k = 0; k < item.Locations.Count; k++)
                        {
                            //Todo:注意：个别计价需要判断批号是否相同
                            if (item.Locations[k].LocationID == locationID)
                            {
                                item.Locations[k].Quantity = outQuantity;
                                founded = true;
                                break;
                            }
                        }
                        if (!founded)
                        {
                            var location = new MaterialOutstockLocation();
                            location.LocationID = locationID;
                            location.Quantity = outQuantity; //Todo:个别计价需要设置批号及价格
                            item.Locations.Add(location);
                        }
                        totalPickedQuantity += outQuantity;
                    }
                }
                #endregion
            }

            if (item.Quantity != totalPickedQuantity)
            {
                RadMessageBox.Show(this, "退货数量[" + item.Quantity.ToString() + "]与出库数量[" + totalPickedQuantity + "]不一致"
                    , "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            if (needAdd)
                _task.Items.Add(item);
        }
    }
}
