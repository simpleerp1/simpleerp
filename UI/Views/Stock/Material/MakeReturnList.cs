﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class MakeReturnList : UserControl
    {
        public MakeReturnList()
        {
            InitializeComponent();

            this.qpWarehouse.PickerView = new WarehousePickerView();
            this.qpWarehouse.DisplayMember = "Name";
            this.qpCostCenter.PickerView = new CostCenterPickerView();
            this.qpCostCenter.DisplayMember = "Name";
            this.qpTakeBy.PickerView = new EmploeePickerView();
            this.qpTakeBy.DisplayMember = "Name";
            this.dgList.QueryMethod = "mzerp.MaterialInstockService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            //处理当前用的权限
            //this.btNew.Enabled = Permissions.ProductInstock_Create.IsOwn;
            //this.btOpen.Enabled = Permissions.ProductInstock_Edit.IsOwn;
            //this.btDelete.Enabled = Permissions.ProductInstock_Delete.IsOwn;

            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[6];
            if (qcDate.Checked)
            {
                args[0] = qpStartDate.Value;
                args[1] = qpEndDate.Value;
            }

            if (qcVendor.Checked)
                args[2] = qpCostCenter.SelectedEntityID;
            if (qcPurchaseBy.Checked)
                args[3] = qpTakeBy.SelectedEntityID;
            if (qcWharehouse.Checked)
                args[4] = qpWarehouse.SelectedEntityID;

            args[5] = (int)mzerp.Enums.MaterialInstockType.MakeReturn;
            return args;
        }

        #region Event handlers
        private void OnCreate()
        {
            var view = new MakeReturnView();
            view.Create();
            MainForm.Instance.ShowView(view, "退料入库");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new MaterialInstockTask(dps.Common.Data.Entity.Retrieve(MaterialInstockTask.EntityModelID, id));

            var view = new MakeReturnView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "退料入库");
        }
        #endregion
    }
}
