﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.UI.Controls;
using mzerp.Entities;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class OtherOutstockView : UserControl
    {
        private mzerp.Entities.MaterialOutstockTask _entity;
        public mzerp.Entities.MaterialOutstockTask Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        private OtherOutstockType[] _outstockTypes;

        public OtherOutstockView()
        {
            InitializeComponent();

            _outstockTypes = new OtherOutstockType[1];
            _outstockTypes[0] = new OtherOutstockType("其他出库", (int)mzerp.Enums.MaterialOutstockType.OtherOut);
            this.tbOutstockType.DataSource = _outstockTypes;

            this.tbWarehouse.PickerView = new WarehousePickerView();
            this.tbWarehouse.DisplayMember = "Name";
            this.tbBizPartner.PickerView = new BizPartnerPickerView(false, true);
            this.tbBizPartner.DisplayMember = "Name";
            this.tbCostCenter.PickerView = new CostCenterPickerView();
            this.tbCostCenter.DisplayMember = "Name";
            this.tbTakeBy.PickerView = new EmploeePickerView();
            this.tbTakeBy.DisplayMember = "Name";

            this.btSave.Click += (s, e) => { this.Save(); };
            this.btPick.Click += (s, e) => { this.Pick(); };
            //this.btPrint.Click += (s, e) => { this.Print(); };
            //this.dgItems.DefaultValuesNeeded += OnNewItemDefaultValuesNeeded;
            //this.dgItems.CellValueChanged += OnCellValueChanged;
        }
       
        public void Create()
        {
            var task = new mzerp.Entities.MaterialOutstockTask();
            task.OutstockDate = task.CreateTime = DateTime.Now;
            task.CreateBy = SystemService.CurrentEmploee;
            this.Entity = task;
        }

        private void Pick()
        {
            if (this.tbWarehouse.SelectedEntity != null)
                _entity.Warehouse = (Warehouse)this.tbWarehouse.SelectedEntity;
            if (!_entity.Instance["WarehouseID"].HasValue)
            {
                RadMessageBox.Show(this, "请先选择仓库", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var dlg = new MakeOutstockPickForm();
            dlg.Task = _entity;
            dlg.ShowDialog();

            //Todo:
            foreach (var row in dgItems.Rows)
            {
                row.InvalidateRow();
            }
            dlg.Dispose();
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbTaskNo.Text = _entity.TaskNo;
            this.tbInstockDate.Value = _entity.OutstockDate;
            this.tbOutstockType.SelectedValue = (int)_entity.OutstockType;
            this.tbWarehouse.SelectedEntity = _entity.Warehouse;
            this.tbBizPartner.SelectedEntity = _entity.BizPartner;
            this.tbTakeBy.SelectedEntity = _entity.TakeBy;
            this.tbRefTaskNo.Text = _entity.RefTaskNo;
            this.tbCreateBy.Text = _entity.CreateBy.Base.Name;
            this.tbCreateTime.Text = _entity.CreateTime.ToString();
            this.tbMemo.Text = _entity.Memo;
            this.dgItems.DataSource = _entity.Items;

            this.CheckState();
        }

        private void FlushData()
        {
            _entity.Warehouse = (Warehouse)this.tbWarehouse.SelectedEntity;
            _entity.OutstockDate = this.tbInstockDate.Value;
            _entity.OutstockType = (Enums.MaterialOutstockType)((int)this.tbOutstockType.SelectedValue);
            _entity.BizPartner = (BizPartner)this.tbBizPartner.SelectedEntity;
            _entity.CostCenter = (CostCenter)this.tbCostCenter.SelectedEntity;
            _entity.TakeBy = (sys.Entities.Emploee)this.tbTakeBy.SelectedEntity;
            _entity.RefTaskNo = this.tbRefTaskNo.Text;
            _entity.Memo = this.tbMemo.Text;

            this.dgItems.EndEdit();
        }

        private void CheckState()
        {
            //处理当前用户的权限
            this.btNew.Enabled = Permissions.OtherOutstock_Edit.IsOwn;

            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.btSave.Enabled = Permissions.OtherOutstock_Edit.IsOwn;
                this.btPick.Enabled = false;

                this.tbWarehouse.Enabled = false;
                this.tbCostCenter.Enabled = false;
                this.tbInstockDate.Enabled = false;

                this.dgItems.AllowAddNewRow = false;
                this.dgItems.AllowDeleteRow = false;
            }
            else
            {
                this.btSave.Enabled = Permissions.OtherOutstock_Edit.IsOwn;
                this.btPick.Enabled = true;

                this.tbWarehouse.Enabled = true;
                this.tbCostCenter.Enabled = true;
                this.tbInstockDate.Enabled = true;

                this.dgItems.AllowAddNewRow = false;
                this.dgItems.AllowDeleteRow = true;
            }
        }

        private void Save()
        {
            try
            {
                this.FlushData();
                if (!_entity.Instance["WarehouseID"].HasValue)
                    throw new System.Exception("必须指定出库仓库");
                if (_entity.Items.Count == 0)
                    throw new System.Exception("出库单至少需要一项产品");

                if (RadMessageBox.Show(this, "保存后将不允许删除及更改出库信息，确认保存吗?", "确认信息",
                    MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                    return;

                var r = (dps.Common.Data.Entity)dps.Common.SysService.Invoke("mzerp", "MaterialOutstockService", "SaveNew", _entity.Instance);
                this.Entity = new MaterialOutstockTask(r);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private const int PageRows = 12;
        private void Print()
        {
            //var view = new ReportViewerForm();
            //view.Width = 900;
            //view.Height = 700;

            //var report = new ProductInstockReport();
            //report.ReportParameters["Company"].Value = Global.Company;
            //report.TaskDataSource.DataSource = new List<ProductInstockTask> { _entity };

            //var items = new List<ProductInstockTaskItem>();
            //items.AddRange(_entity.Items);
            //int rowsToAdd = PageRows - (items.Count % PageRows);
            //for (int i = 0; i < rowsToAdd; i++)
            //{ items.Add(null); }
            //report.ItemsDataSource.DataSource = items;

            //var reportSource = new Telerik.Reporting.InstanceReportSource();
            //reportSource.ReportDocument = report;

            //view.ReportViewer.ReportSource = reportSource;

            //view.ShowDialog();
        }
    }

    public class OtherOutstockType
    {
        public string Name { get; private set; }

        public int Value { get; private set; }

        public OtherOutstockType(string name, int value)
        {
            this.Name = name;
            this.Value = value;
        }
    }
}
