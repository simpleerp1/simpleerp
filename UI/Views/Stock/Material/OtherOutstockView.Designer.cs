﻿namespace mzerp.UI
{
    partial class OtherOutstockView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbCostCenter = new mzerp.UI.Controls.EntityPicker();
            this.label11 = new System.Windows.Forms.Label();
            this.tbOutstockType = new Telerik.WinControls.UI.RadDropDownList();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRefTaskNo = new Telerik.WinControls.UI.RadTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbBizPartner = new mzerp.UI.Controls.EntityPicker();
            this.tbMemo = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCreateTime = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCreateBy = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInstockDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbTaskNo = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbWarehouse = new mzerp.UI.Controls.EntityPicker();
            this.tbTakeBy = new mzerp.UI.Controls.EntityPicker();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btPick = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btPrint = new Telerik.WinControls.UI.CommandBarButton();
            this.dgItems = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCostCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOutstockType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRefTaskNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBizPartner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInstockDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTakeBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbCostCenter);
            this.radGroupBox1.Controls.Add(this.label11);
            this.radGroupBox1.Controls.Add(this.tbOutstockType);
            this.radGroupBox1.Controls.Add(this.label10);
            this.radGroupBox1.Controls.Add(this.tbRefTaskNo);
            this.radGroupBox1.Controls.Add(this.label9);
            this.radGroupBox1.Controls.Add(this.label8);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.label7);
            this.radGroupBox1.Controls.Add(this.tbBizPartner);
            this.radGroupBox1.Controls.Add(this.tbMemo);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.tbCreateTime);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.tbCreateBy);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.tbInstockDate);
            this.radGroupBox1.Controls.Add(this.tbTaskNo);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.tbWarehouse);
            this.radGroupBox1.Controls.Add(this.tbTakeBy);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "其他出库单";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(812, 211);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.Text = "其他出库单";
            // 
            // tbCostCenter
            // 
            this.tbCostCenter.DisplayMember = null;
            this.tbCostCenter.Location = new System.Drawing.Point(104, 105);
            this.tbCostCenter.Name = "tbCostCenter";
            this.tbCostCenter.PickerView = null;
            this.tbCostCenter.SelectedEntity = null;
            this.tbCostCenter.Size = new System.Drawing.Size(332, 20);
            this.tbCostCenter.TabIndex = 3;
            this.tbCostCenter.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "成本中心：";
            // 
            // tbOutstockType
            // 
            this.tbOutstockType.AutoCompleteDisplayMember = "Name";
            this.tbOutstockType.AutoCompleteValueMember = "Value";
            this.tbOutstockType.DisplayMember = "Name";
            this.tbOutstockType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.tbOutstockType.Location = new System.Drawing.Point(322, 53);
            this.tbOutstockType.Name = "tbOutstockType";
            this.tbOutstockType.Size = new System.Drawing.Size(114, 20);
            this.tbOutstockType.TabIndex = 33;
            this.tbOutstockType.ValueMember = "Value";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(245, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "出库类型：";
            // 
            // tbRefTaskNo
            // 
            this.tbRefTaskNo.Location = new System.Drawing.Point(322, 131);
            this.tbRefTaskNo.Name = "tbRefTaskNo";
            this.tbRefTaskNo.Size = new System.Drawing.Size(114, 20);
            this.tbRefTaskNo.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(270, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "仓库：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(244, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "相关单号：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "出库人：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "业务单位：";
            // 
            // tbBizPartner
            // 
            this.tbBizPartner.DisplayMember = null;
            this.tbBizPartner.Location = new System.Drawing.Point(104, 79);
            this.tbBizPartner.Name = "tbBizPartner";
            this.tbBizPartner.PickerView = null;
            this.tbBizPartner.SelectedEntity = null;
            this.tbBizPartner.Size = new System.Drawing.Size(332, 20);
            this.tbBizPartner.TabIndex = 2;
            this.tbBizPartner.TabStop = false;
            // 
            // tbMemo
            // 
            this.tbMemo.Location = new System.Drawing.Point(104, 183);
            this.tbMemo.Name = "tbMemo";
            this.tbMemo.Size = new System.Drawing.Size(549, 20);
            this.tbMemo.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "备注：";
            // 
            // tbCreateTime
            // 
            this.tbCreateTime.Location = new System.Drawing.Point(322, 157);
            this.tbCreateTime.Name = "tbCreateTime";
            this.tbCreateTime.ReadOnly = true;
            this.tbCreateTime.Size = new System.Drawing.Size(114, 20);
            this.tbCreateTime.TabIndex = 10;
            this.tbCreateTime.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(244, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "制单日期：";
            // 
            // tbCreateBy
            // 
            this.tbCreateBy.Location = new System.Drawing.Point(105, 157);
            this.tbCreateBy.Name = "tbCreateBy";
            this.tbCreateBy.ReadOnly = true;
            this.tbCreateBy.Size = new System.Drawing.Size(114, 20);
            this.tbCreateBy.TabIndex = 10;
            this.tbCreateBy.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "制单员：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "出库日期：";
            // 
            // tbInstockDate
            // 
            this.tbInstockDate.Location = new System.Drawing.Point(105, 53);
            this.tbInstockDate.Name = "tbInstockDate";
            this.tbInstockDate.Size = new System.Drawing.Size(114, 20);
            this.tbInstockDate.TabIndex = 1;
            this.tbInstockDate.TabStop = false;
            this.tbInstockDate.Text = "2013年11月15日";
            this.tbInstockDate.Value = new System.DateTime(2013, 11, 15, 16, 52, 36, 151);
            // 
            // tbTaskNo
            // 
            this.tbTaskNo.Location = new System.Drawing.Point(105, 27);
            this.tbTaskNo.Name = "tbTaskNo";
            this.tbTaskNo.ReadOnly = true;
            this.tbTaskNo.Size = new System.Drawing.Size(114, 20);
            this.tbTaskNo.TabIndex = 10;
            this.tbTaskNo.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "出库单号：";
            // 
            // tbWarehouse
            // 
            this.tbWarehouse.DisplayMember = null;
            this.tbWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbWarehouse.Location = new System.Drawing.Point(322, 27);
            this.tbWarehouse.Name = "tbWarehouse";
            this.tbWarehouse.PickerView = null;
            this.tbWarehouse.SelectedEntity = null;
            this.tbWarehouse.Size = new System.Drawing.Size(114, 20);
            this.tbWarehouse.TabIndex = 0;
            this.tbWarehouse.TabStop = false;
            this.tbWarehouse.ThemeName = "ControlDefault";
            // 
            // tbTakeBy
            // 
            this.tbTakeBy.DisplayMember = null;
            this.tbTakeBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbTakeBy.Location = new System.Drawing.Point(104, 131);
            this.tbTakeBy.Name = "tbTakeBy";
            this.tbTakeBy.PickerView = null;
            this.tbTakeBy.SelectedEntity = null;
            this.tbTakeBy.Size = new System.Drawing.Size(115, 20);
            this.tbTakeBy.TabIndex = 3;
            this.tbTakeBy.TabStop = false;
            this.tbTakeBy.ThemeName = "ControlDefault";
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(812, 30);
            this.radCommandBar1.TabIndex = 8;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btSave,
            this.commandBarSeparator1,
            this.btPick,
            this.commandBarSeparator2,
            this.btPrint});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "打开";
            this.btSave.AccessibleName = "打开";
            this.btSave.DisplayName = "commandBarButton1";
            this.btSave.DrawText = true;
            this.btSave.Image = global::mzerp.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // btPick
            // 
            this.btPick.AccessibleDescription = "出库拣货";
            this.btPick.AccessibleName = "出库拣货";
            this.btPick.DisplayName = "commandBarButton1";
            this.btPick.DrawText = true;
            this.btPick.Image = global::mzerp.UI.Properties.Resources.OutstockPick16;
            this.btPick.Name = "btPick";
            this.btPick.Text = "出库拣货";
            this.btPick.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPick.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.AccessibleDescription = "commandBarSeparator2";
            this.commandBarSeparator2.AccessibleName = "commandBarSeparator2";
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // btPrint
            // 
            this.btPrint.AccessibleDescription = "commandBarButton1";
            this.btPrint.AccessibleName = "commandBarButton1";
            this.btPrint.DisplayName = "commandBarButton1";
            this.btPrint.DrawText = true;
            this.btPrint.Image = global::mzerp.UI.Properties.Resources.Print16;
            this.btPrint.Name = "btPrint";
            this.btPrint.Text = "打印";
            this.btPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPrint.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dgItems
            // 
            this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItems.Location = new System.Drawing.Point(0, 241);
            // 
            // dgItems
            // 
            this.dgItems.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dgItems.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Material.Code";
            gridViewTextBoxColumn1.HeaderText = "物料";
            gridViewTextBoxColumn1.Name = "clMaterialCode";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Material.Name";
            gridViewTextBoxColumn2.HeaderText = "名称";
            gridViewTextBoxColumn2.Name = "clName";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "Material.Spec";
            gridViewTextBoxColumn3.HeaderText = "规格";
            gridViewTextBoxColumn3.Name = "clSpec";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.FieldName = "Material.MeasureUnit";
            gridViewTextBoxColumn4.HeaderText = "单位";
            gridViewTextBoxColumn4.Name = "clMeasureUnit";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewDecimalColumn1.DecimalPlaces = 6;
            gridViewDecimalColumn1.FieldName = "Quantity";
            gridViewDecimalColumn1.HeaderText = "数量";
            gridViewDecimalColumn1.Name = "clQuantity";
            gridViewDecimalColumn1.ReadOnly = true;
            gridViewDecimalColumn1.Width = 70;
            gridViewDecimalColumn2.FieldName = "TotalPrice";
            gridViewDecimalColumn2.HeaderText = "出库成本";
            gridViewDecimalColumn2.Name = "clTotalPrice";
            gridViewDecimalColumn2.ReadOnly = true;
            gridViewDecimalColumn2.Width = 100;
            gridViewDecimalColumn3.DecimalPlaces = 4;
            gridViewDecimalColumn3.Expression = "IIF(clQuantity=0,0 ,clTotalPrice / clQuantity ) ";
            gridViewDecimalColumn3.HeaderText = "出库单价";
            gridViewDecimalColumn3.Name = "clUnitPrice";
            gridViewDecimalColumn3.ReadOnly = true;
            gridViewDecimalColumn3.Width = 60;
            gridViewTextBoxColumn5.FieldName = "Memo";
            gridViewTextBoxColumn5.HeaderText = "备注";
            gridViewTextBoxColumn5.Name = "clMemo";
            gridViewTextBoxColumn5.Width = 100;
            this.dgItems.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewTextBoxColumn5});
            this.dgItems.Name = "dgItems";
            this.dgItems.ShowGroupPanel = false;
            this.dgItems.Size = new System.Drawing.Size(812, 303);
            this.dgItems.TabIndex = 9;
            this.dgItems.Text = "radGridView1";
            // 
            // OtherOutstockView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "OtherOutstockView";
            this.Size = new System.Drawing.Size(812, 544);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCostCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOutstockType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRefTaskNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBizPartner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInstockDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTakeBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Controls.EntityPicker tbCostCenter;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadDropDownList tbOutstockType;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadTextBox tbRefTaskNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Controls.EntityPicker tbBizPartner;
        private Telerik.WinControls.UI.RadTextBox tbMemo;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadTextBox tbCreateTime;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox tbCreateBy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadDateTimePicker tbInstockDate;
        private Telerik.WinControls.UI.RadTextBox tbTaskNo;
        private System.Windows.Forms.Label label1;
        private Controls.EntityPicker tbWarehouse;
        private Controls.EntityPicker tbTakeBy;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton btPick;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarButton btPrint;
        private Telerik.WinControls.UI.RadGridView dgItems;
    }
}
