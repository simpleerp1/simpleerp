﻿namespace mzerp.UI
{
    partial class MakeReturnPickForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpVendor = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.qpMeasureUnit = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.qpSpec = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.qpName = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.qpMaterial = new mzerp.UI.Controls.EntityPicker();
            this.label3 = new System.Windows.Forms.Label();
            this.qpWarehouse = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new Telerik.WinControls.UI.RadButton();
            this.btAdd = new Telerik.WinControls.UI.RadButton();
            this.dgItems = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMeasureUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpVendor);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.qpMeasureUnit);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.qpSpec);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.qpName);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.qpMaterial);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.qpWarehouse);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "退料查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(680, 88);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "退料查询条件";
            // 
            // qpVendor
            // 
            this.qpVendor.Location = new System.Drawing.Point(249, 25);
            this.qpVendor.Name = "qpVendor";
            this.qpVendor.ReadOnly = true;
            this.qpVendor.Size = new System.Drawing.Size(343, 20);
            this.qpVendor.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(182, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "成本中心：";
            // 
            // qpMeasureUnit
            // 
            this.qpMeasureUnit.Location = new System.Drawing.Point(540, 51);
            this.qpMeasureUnit.Name = "qpMeasureUnit";
            this.qpMeasureUnit.ReadOnly = true;
            this.qpMeasureUnit.Size = new System.Drawing.Size(52, 20);
            this.qpMeasureUnit.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(498, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "单位：";
            // 
            // qpSpec
            // 
            this.qpSpec.Location = new System.Drawing.Point(398, 51);
            this.qpSpec.Name = "qpSpec";
            this.qpSpec.ReadOnly = true;
            this.qpSpec.Size = new System.Drawing.Size(100, 20);
            this.qpSpec.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(354, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "规格：";
            // 
            // qpName
            // 
            this.qpName.Location = new System.Drawing.Point(249, 51);
            this.qpName.Name = "qpName";
            this.qpName.ReadOnly = true;
            this.qpName.Size = new System.Drawing.Size(100, 20);
            this.qpName.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "名称：";
            // 
            // qpMaterial
            // 
            this.qpMaterial.DisplayMember = null;
            this.qpMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qpMaterial.Location = new System.Drawing.Point(76, 51);
            this.qpMaterial.Name = "qpMaterial";
            this.qpMaterial.PickerView = null;
            this.qpMaterial.SelectedEntity = null;
            this.qpMaterial.Size = new System.Drawing.Size(100, 20);
            this.qpMaterial.TabIndex = 29;
            this.qpMaterial.TabStop = false;
            this.qpMaterial.ThemeName = "ControlDefault";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料：";
            // 
            // qpWarehouse
            // 
            this.qpWarehouse.Location = new System.Drawing.Point(76, 25);
            this.qpWarehouse.Name = "qpWarehouse";
            this.qpWarehouse.ReadOnly = true;
            this.qpWarehouse.Size = new System.Drawing.Size(100, 20);
            this.qpWarehouse.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "仓库：";
            // 
            // btSearch
            // 
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search;
            this.btSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btSearch.Location = new System.Drawing.Point(601, 28);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(67, 42);
            this.btSearch.TabIndex = 19;
            this.btSearch.Text = "查询";
            this.btSearch.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Controls.Add(this.btAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 329);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(680, 52);
            this.panel1.TabIndex = 2;
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Location = new System.Drawing.Point(558, 16);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(110, 24);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "关闭";
            // 
            // btAdd
            // 
            this.btAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btAdd.Location = new System.Drawing.Point(449, 16);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(103, 24);
            this.btAdd.TabIndex = 0;
            this.btAdd.Text = "添加至退料清单";
            // 
            // dgItems
            // 
            this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItems.Location = new System.Drawing.Point(0, 88);
            // 
            // dgItems
            // 
            this.dgItems.MasterTemplate.AllowAddNewRow = false;
            this.dgItems.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn1.FieldName = "TaskNo";
            gridViewTextBoxColumn1.HeaderText = "领料单号";
            gridViewTextBoxColumn1.Name = "clTaskNo";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.FieldName = "OutstockDate";
            gridViewTextBoxColumn2.FormatString = "{0:yyyy-MM-dd}";
            gridViewTextBoxColumn2.HeaderText = "领料日期";
            gridViewTextBoxColumn2.Name = "clOutstockDate";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "AvaliableQuantity";
            gridViewTextBoxColumn3.HeaderText = "可退数量";
            gridViewTextBoxColumn3.Name = "clAvaliableQuantity";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn3.Width = 100;
            gridViewDecimalColumn1.DecimalPlaces = 4;
            gridViewDecimalColumn1.FieldName = "OutQuantity";
            gridViewDecimalColumn1.HeaderText = "退回数量";
            gridViewDecimalColumn1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn1.Name = "clOutQuantity";
            gridViewDecimalColumn1.Width = 100;
            this.dgItems.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDecimalColumn1});
            this.dgItems.Name = "dgItems";
            this.dgItems.ShowGroupPanel = false;
            this.dgItems.Size = new System.Drawing.Size(680, 241);
            this.dgItems.TabIndex = 4;
            this.dgItems.Text = "radGridView1";
            // 
            // MakeReturnPickForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 381);
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MakeReturnPickForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "退料选择";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMeasureUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton btClose;
        private Telerik.WinControls.UI.RadButton btAdd;
        private Telerik.WinControls.UI.RadButton btSearch;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBox qpWarehouse;
        private Telerik.WinControls.UI.RadTextBox qpMeasureUnit;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadTextBox qpSpec;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadTextBox qpName;
        private System.Windows.Forms.Label label4;
        private Controls.EntityPicker qpMaterial;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadTextBox qpVendor;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadGridView dgItems;
    }
}
