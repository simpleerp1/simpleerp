﻿using dps.Common;
using mzerp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class MakeReturnPickForm : Telerik.WinControls.UI.RadForm
    {
        private mzerp.Entities.MaterialInstockTask _task;
        public mzerp.Entities.MaterialInstockTask Task
        {
            get { return _task; }
            set
            {
                _task = value;
                this.qpWarehouse.Text = _task.Warehouse.Name;
                this.qpVendor.Text = _task.CostCenter.Name;
            }
        }

        public MakeReturnPickForm()
        {
            InitializeComponent();
            this.qpMaterial.PickerView = new MaterialPickerView();
            this.qpMaterial.DisplayMember = "Code";
            this.qpMaterial.SelectedChanged += OnMaterialChanged;

            this.btSearch.Click += (s, e) => { this.Search(); };
            this.btAdd.Click += (s, e) => { this.AddToList(); };
            this.btClose.Click += (s, e) => { this.Close(); };
        }

        private void OnMaterialChanged(object sender, EventArgs e)
        {
            if (qpMaterial.SelectedEntity == null)
            {
                this.qpName.Text = null;
                this.qpSpec.Text = null;
                this.qpMeasureUnit.Text = null;
            }
            else
            {
                var m = (Material)qpMaterial.SelectedEntity;
                this.qpName.Text = m.Name;
                this.qpSpec.Text = m.Spec;
                this.qpMeasureUnit.Text = m.MeasureUnit;
            }
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[3];
            args[0] = this._task.CostCenterID;
            args[1] = this._task.WarehouseID;
            if (this.qpMaterial.SelectedEntityID.HasValue)
                args[2] = this.qpMaterial.SelectedEntityID.Value;
            return args;
        }

        private void Search()
        {
            if (this.qpMaterial.SelectedEntity == null)
            {
                RadMessageBox.Show(this, "请先选择退回物料", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var args = GetQueryArgs();
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var dt = dps.Common.SysService.Invoke("mzerp", "MaterialOutstockService", "QueryForReturn", args) as dps.Common.Data.DataTable;
                    if (dt == null)
                        throw new System.Exception("查询结果不正确");

                    dt.Columns.Add("OutQuantity", typeof(Decimal));
                    //检查存在的已拣货量
                    if (_task.Items.Count > 0)
                    {
                        Guid outstockItemID;
                        Guid materialID = this.qpMaterial.SelectedEntityID.Value;
                        //string lotNo;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            outstockItemID = (Guid)dt.Rows[i]["ID"];
                            for (int j = 0; j < _task.Items.Count; j++)
                            {
                                if (_task.Items[j].MaterialID == materialID)
                                {
                                    var item = _task.Items[j];
                                    for (int k = 0; k < item.ReturnedItems.Count; k++)
                                    {
                                        if (item.ReturnedItems[k].OutstockItemID == outstockItemID)
                                        {
                                            dt.Rows[i]["OutQuantity"] = item.ReturnedItems[k].Quantity;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dgItems.DataSource = dt;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void AddToList()
        {
            var dt = this.dgItems.DataSource as dps.Common.Data.DataTable;
            if (dt == null)
                return;

            object outQuantityValue;
            decimal availableQuantity = 0m;
            decimal outQuantity = 0m;
            Guid materialID = this.qpMaterial.SelectedEntityID.Value;
            Guid outstockItemID;
            //string lotNo;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                outQuantityValue = dt.Rows[i]["OutQuantity"];
                if (outQuantityValue != DBNull.Value)
                {
                    availableQuantity = (decimal)dt.Rows[i]["AvaliableQuantity"];
                    outQuantity = (decimal)outQuantityValue;
                    outstockItemID = (Guid)dt.Rows[i]["ID"];
                    if (outQuantity > 0)
                    {
                        MaterialInstockItem item = null;
                        //先查找有没有相同物料的入库项存在
                        for (int j = 0; j < _task.Items.Count; j++)
                        {
                            if (_task.Items[j].MaterialID == materialID)
                            {
                                item = _task.Items[j];
                                break;
                            }
                        }
                        if (item == null)
                        {
                            item = new MaterialInstockItem();
                            item.MaterialID = materialID;
                            item.Quantity = 0;
                            //item.ReturnedQuantity = 0.0m;
                            //item.TotalPrice = 0.0m;
                            _task.Items.Add(item);
                        }

                        //再查找对应的ReturnedItem并计算差异量
                        decimal diff = 0m;
                        bool founded = false;
                        for (int k = 0; k < item.ReturnedItems.Count; k++)
                        {
                            //Todo:注意：个别计价需要判断批号是否相同
                            if (item.ReturnedItems[k].OutstockItemID == outstockItemID)
                            {
                                decimal oldvalue = item.ReturnedItems[k].Quantity;
                                item.ReturnedItems[k].Quantity = outQuantity;
                                diff = outQuantity - oldvalue;
                                founded = true;
                                break;
                            }
                        }
                        if (founded)
                            item.Quantity += diff;
                        else
                        {
                            var returnedItem = new MaterialInstockReturnedItem();
                            returnedItem.OutstockItemID = outstockItemID;
                            returnedItem.Quantity = outQuantity; 
                            item.ReturnedItems.Add(returnedItem);
                            item.Quantity += outQuantity;
                        }
                    }
                }
            }
        }
    }
}
