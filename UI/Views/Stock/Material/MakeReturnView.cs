﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;
using Telerik.WinControls;
using mzerp.Reports;

namespace mzerp.UI
{
    public partial class MakeReturnView : UserControl
    {
        private mzerp.Entities.MaterialInstockTask _entity;
        public mzerp.Entities.MaterialInstockTask Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public MakeReturnView()
        {
            InitializeComponent();

            this.tbWarehouse.PickerView = new WarehousePickerView();
            this.tbWarehouse.DisplayMember = "Name";
            this.tbCostCenter.PickerView = new CostCenterPickerView();
            this.tbCostCenter.DisplayMember = "Name";
            this.tbTakeBy.PickerView = new EmploeePickerView();
            this.tbTakeBy.DisplayMember = "Name";

            this.btSave.Click += (s, e) => { this.Save(); };
            this.btPick.Click += (s, e) => { this.Pick(); };
            this.btPrint.Click += (s, e) => { this.Print(); };
            //this.dgItems.DefaultValuesNeeded += OnNewItemDefaultValuesNeeded;
            //this.dgItems.CellValueChanged += OnCellValueChanged;
            //Todo:处理仓库选择变更时，必须移除所有明细项重新开始
        }

        private void OnCellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            //if (e.Column.Name == "clMaterial")
            //{
            //    Material obj = (Material)e.Value;
            //    e.Row.Cells["clName"].Value = obj.Name;
            //    e.Row.Cells["clSpec"].Value = obj.Spec;
            //    e.Row.Cells["clMeasureUnit"].Value = obj.MeasureUnit;
            //}
            //else if (e.Column.Name == "clQuantity" || e.Column.Name == "clTotalPrice")
            //{
            //    CalcQuantity(e.Row);
            //}
        }

        //重新计算总的万粒数
        //private void CalcQuantity(GridViewRowInfo row)
        //{
        //    int boxes = row.Cells["clBoxes"].Value == null ? 0 : (int)row.Cells["clBoxes"].Value;
        //    decimal boxQuantity = row.Cells["clQuantityOfBox"].Value == null ? 0m : (decimal)row.Cells["clQuantityOfBox"].Value;
        //    decimal change = row.Cells["clChange"].Value == null ? 0m : (decimal)row.Cells["clChange"].Value;
        //    row.Cells["clQuantity"].Value = boxes * boxQuantity + change;
        //}

        public void Create()
        {
            var task = new mzerp.Entities.MaterialInstockTask();
            task.InstockDate = task.CreateTime = DateTime.Now;
            task.CreateBy = SystemService.CurrentEmploee;
            task.InstockType = Enums.MaterialInstockType.MakeReturn;
            this.Entity = task;
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbTaskNo.Text = _entity.TaskNo;
            this.tbOutstockDate.Value = _entity.InstockDate;
            if (_entity.Instance["WarehouseID"].HasValue)
                this.tbWarehouse.SelectedEntity = _entity.Warehouse;
            if (_entity.CostCenterID.HasValue)
                this.tbCostCenter.SelectedEntity = _entity.CostCenter;
            if (_entity.PurchaseByID.HasValue)
                this.tbTakeBy.SelectedEntity = _entity.PurchaseBy;
            this.tbRefTaskNo.Text = _entity.RefTaskNo;
            this.tbCreateBy.Text = _entity.CreateBy.Base.Name;
            this.tbCreateTime.Text = _entity.CreateTime.ToString();
            this.tbMemo.Text = _entity.Memo;
            this.dgItems.DataSource = _entity.Items;

            this.CheckState();
        }

        private void FlushData()
        {
            _entity.Warehouse = (Warehouse)this.tbWarehouse.SelectedEntity;
            _entity.InstockDate = this.tbOutstockDate.Value;
            _entity.CostCenter = (CostCenter)this.tbCostCenter.SelectedEntity;
            _entity.PurchaseBy = (sys.Entities.Emploee)this.tbTakeBy.SelectedEntity;
            _entity.RefTaskNo = this.tbRefTaskNo.Text;
            _entity.Memo = this.tbMemo.Text;

            this.dgItems.EndEdit();
        }

        private void CheckState()
        {
            //处理当前用户的权限
            this.btNew.Enabled = Permissions.PurchaseInstock_Edit.IsOwn;

            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.btSave.Enabled = Permissions.MakeReturn_Edit.IsOwn;
                this.btPick.Enabled = false;

                this.tbWarehouse.Enabled = false;
                this.tbCostCenter.Enabled = false;
                this.tbOutstockDate.Enabled = false;

                this.dgItems.AllowAddNewRow = false;
                this.dgItems.AllowDeleteRow = false;
            }
            else
            {
                this.btSave.Enabled = Permissions.MakeReturn_Edit.IsOwn;
                this.btPick.Enabled = true;

                this.tbWarehouse.Enabled = true;
                this.tbCostCenter.Enabled = true;
                this.tbOutstockDate.Enabled = true;

                this.dgItems.AllowAddNewRow = false;
                this.dgItems.AllowDeleteRow = true;
            }
        }

        private void Pick()
        {
            if (this.tbCostCenter.SelectedEntity != null)
                _entity.CostCenter = (CostCenter)this.tbCostCenter.SelectedEntity;
            if (this.tbWarehouse.SelectedEntity != null)
                _entity.Warehouse = (Warehouse)this.tbWarehouse.SelectedEntity;
            if (!_entity.Instance["WarehouseID"].HasValue)
            {
                RadMessageBox.Show(this, "请先选择仓库", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (!_entity.CostCenterID.HasValue)
            {
                RadMessageBox.Show(this, "请先选择成本中心", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var dlg = new MakeReturnPickForm();
            dlg.Task = _entity;
            dlg.ShowDialog();

            //Todo:
            foreach (var row in dgItems.Rows)
            {
                row.InvalidateRow();
            }
            dlg.Dispose();
        }

        private void Save()
        {
            try
            {
                this.FlushData();
                if (!_entity.Instance["WarehouseID"].HasValue)
                    throw new System.Exception("必须指定入库仓库");
                if (_entity.Items.Count == 0)
                    throw new System.Exception("出库单至少需要一项产品");

                if (RadMessageBox.Show(this, "保存后将不允许删除及更改出库信息，确认保存吗?", "确认信息",
                    MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                    return;

                var r = (dps.Common.Data.Entity)dps.Common.SysService.Invoke("mzerp", "MaterialInstockService", "SaveNew", _entity.Instance);
                this.Entity = new MaterialInstockTask(r);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private const int PageRows = 12;
        private void Print()
        {
            var view = new ReportViewerForm();
            view.Width = 900;
            view.Height = 700;

            var report = new MakeReturnReport();
            report.ReportParameters["Company"].Value = Global.Company;
            report.TaskDataSource.DataSource = new List<MaterialInstockTask> { _entity };

            var items = new List<MaterialInstockItem>();
            items.AddRange(_entity.Items);
            int rowsToAdd = PageRows - (items.Count % PageRows);
            for (int i = 0; i < rowsToAdd; i++)
            { items.Add(null); }
            report.ItemsDataSource.DataSource = items;

            var reportSource = new Telerik.Reporting.InstanceReportSource();
            reportSource.ReportDocument = report;

            view.ReportViewer.ReportSource = reportSource;

            view.ShowDialog();
        }
    }
}
