﻿namespace mzerp.UI
{
    partial class PurchaseInstockView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            mzerp.UI.Controls.EntityColumn entityColumn1 = new mzerp.UI.Controls.EntityColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btPrint = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbRefTaskNo = new Telerik.WinControls.UI.RadTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbVendor = new mzerp.UI.Controls.EntityPicker();
            this.tbMemo = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCreateTime = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCreateBy = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInstockDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbTaskNo = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbWarehouse = new mzerp.UI.Controls.EntityPicker();
            this.tbPurchaseBy = new mzerp.UI.Controls.EntityPicker();
            this.dgItems = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRefTaskNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInstockDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPurchaseBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(683, 30);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btSave,
            this.commandBarSeparator1,
            this.btPrint});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "打开";
            this.btSave.AccessibleName = "打开";
            this.btSave.DisplayName = "commandBarButton1";
            this.btSave.DrawText = true;
            this.btSave.Image = global::mzerp.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // btPrint
            // 
            this.btPrint.AccessibleDescription = "commandBarButton1";
            this.btPrint.AccessibleName = "commandBarButton1";
            this.btPrint.DisplayName = "commandBarButton1";
            this.btPrint.DrawText = true;
            this.btPrint.Image = global::mzerp.UI.Properties.Resources.Print16;
            this.btPrint.Name = "btPrint";
            this.btPrint.Text = "打印";
            this.btPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPrint.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbRefTaskNo);
            this.radGroupBox1.Controls.Add(this.label9);
            this.radGroupBox1.Controls.Add(this.label8);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.label7);
            this.radGroupBox1.Controls.Add(this.tbVendor);
            this.radGroupBox1.Controls.Add(this.tbMemo);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.tbCreateTime);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.tbCreateBy);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.tbInstockDate);
            this.radGroupBox1.Controls.Add(this.tbTaskNo);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.tbWarehouse);
            this.radGroupBox1.Controls.Add(this.tbPurchaseBy);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "采购入库单";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(683, 177);
            this.radGroupBox1.TabIndex = 5;
            this.radGroupBox1.Text = "采购入库单";
            // 
            // tbRefTaskNo
            // 
            this.tbRefTaskNo.Location = new System.Drawing.Point(322, 82);
            this.tbRefTaskNo.Name = "tbRefTaskNo";
            this.tbRefTaskNo.Size = new System.Drawing.Size(114, 20);
            this.tbRefTaskNo.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(270, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "仓库：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(244, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "采购单号：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "采购员：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "供应商：";
            // 
            // tbVendor
            // 
            this.tbVendor.DisplayMember = null;
            this.tbVendor.Location = new System.Drawing.Point(104, 54);
            this.tbVendor.Name = "tbVendor";
            this.tbVendor.PickerView = null;
            this.tbVendor.SelectedEntity = null;
            this.tbVendor.Size = new System.Drawing.Size(332, 20);
            this.tbVendor.TabIndex = 2;
            this.tbVendor.TabStop = false;
            // 
            // tbMemo
            // 
            this.tbMemo.Location = new System.Drawing.Point(104, 137);
            this.tbMemo.Name = "tbMemo";
            this.tbMemo.Size = new System.Drawing.Size(549, 20);
            this.tbMemo.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "备注：";
            // 
            // tbCreateTime
            // 
            this.tbCreateTime.Location = new System.Drawing.Point(322, 110);
            this.tbCreateTime.Name = "tbCreateTime";
            this.tbCreateTime.ReadOnly = true;
            this.tbCreateTime.Size = new System.Drawing.Size(114, 20);
            this.tbCreateTime.TabIndex = 10;
            this.tbCreateTime.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(244, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "制单日期：";
            // 
            // tbCreateBy
            // 
            this.tbCreateBy.Location = new System.Drawing.Point(105, 110);
            this.tbCreateBy.Name = "tbCreateBy";
            this.tbCreateBy.ReadOnly = true;
            this.tbCreateBy.Size = new System.Drawing.Size(114, 20);
            this.tbCreateBy.TabIndex = 10;
            this.tbCreateBy.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "制单员：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(461, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "入库日期：";
            // 
            // tbInstockDate
            // 
            this.tbInstockDate.Location = new System.Drawing.Point(539, 27);
            this.tbInstockDate.Name = "tbInstockDate";
            this.tbInstockDate.Size = new System.Drawing.Size(114, 20);
            this.tbInstockDate.TabIndex = 1;
            this.tbInstockDate.TabStop = false;
            this.tbInstockDate.Text = "2013年11月15日";
            this.tbInstockDate.Value = new System.DateTime(2013, 11, 15, 16, 52, 36, 151);
            // 
            // tbTaskNo
            // 
            this.tbTaskNo.Location = new System.Drawing.Point(105, 27);
            this.tbTaskNo.Name = "tbTaskNo";
            this.tbTaskNo.ReadOnly = true;
            this.tbTaskNo.Size = new System.Drawing.Size(114, 20);
            this.tbTaskNo.TabIndex = 10;
            this.tbTaskNo.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "入库单号：";
            // 
            // tbWarehouse
            // 
            this.tbWarehouse.DisplayMember = null;
            this.tbWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbWarehouse.Location = new System.Drawing.Point(322, 27);
            this.tbWarehouse.Name = "tbWarehouse";
            this.tbWarehouse.PickerView = null;
            this.tbWarehouse.SelectedEntity = null;
            this.tbWarehouse.Size = new System.Drawing.Size(114, 20);
            this.tbWarehouse.TabIndex = 0;
            this.tbWarehouse.TabStop = false;
            this.tbWarehouse.ThemeName = "ControlDefault";
            // 
            // tbPurchaseBy
            // 
            this.tbPurchaseBy.DisplayMember = null;
            this.tbPurchaseBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbPurchaseBy.Location = new System.Drawing.Point(104, 82);
            this.tbPurchaseBy.Name = "tbPurchaseBy";
            this.tbPurchaseBy.PickerView = null;
            this.tbPurchaseBy.SelectedEntity = null;
            this.tbPurchaseBy.Size = new System.Drawing.Size(115, 20);
            this.tbPurchaseBy.TabIndex = 3;
            this.tbPurchaseBy.TabStop = false;
            this.tbPurchaseBy.ThemeName = "ControlDefault";
            // 
            // dgItems
            // 
            this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItems.EnterKeyMode = Telerik.WinControls.UI.RadGridViewEnterKeyMode.EnterMovesToNextRow;
            this.dgItems.Location = new System.Drawing.Point(0, 207);
            // 
            // dgItems
            // 
            this.dgItems.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dgItems.MasterTemplate.AutoGenerateColumns = false;
            entityColumn1.DisplayMember = null;
            entityColumn1.FieldName = "Material";
            entityColumn1.HeaderText = "物料";
            entityColumn1.Name = "clMaterial";
            entityColumn1.PickerView = null;
            entityColumn1.Width = 100;
            gridViewTextBoxColumn1.FieldName = "Material.Name";
            gridViewTextBoxColumn1.HeaderText = "名称";
            gridViewTextBoxColumn1.Name = "clName";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.FieldName = "Material.Spec";
            gridViewTextBoxColumn2.HeaderText = "规格";
            gridViewTextBoxColumn2.Name = "clSpec";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "Material.MeasureUnit";
            gridViewTextBoxColumn3.HeaderText = "单位";
            gridViewTextBoxColumn3.Name = "clMeasureUnit";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewDecimalColumn1.DecimalPlaces = 6;
            gridViewDecimalColumn1.FieldName = "Quantity";
            gridViewDecimalColumn1.HeaderText = "数量";
            gridViewDecimalColumn1.Name = "clQuantity";
            gridViewDecimalColumn1.Width = 60;
            gridViewDecimalColumn2.FieldName = "TotalPrice";
            gridViewDecimalColumn2.HeaderText = "金额（不含进项税）";
            gridViewDecimalColumn2.Name = "clTotalPrice";
            gridViewDecimalColumn2.Width = 115;
            gridViewDecimalColumn3.DecimalPlaces = 4;
            gridViewDecimalColumn3.Expression = "IIF(clQuantity=0,0 ,  clTotalPrice / clQuantity )";
            gridViewDecimalColumn3.HeaderText = "单价";
            gridViewDecimalColumn3.Name = "clUnitPrice";
            gridViewDecimalColumn3.ReadOnly = true;
            gridViewDecimalColumn3.Width = 60;
            gridViewTextBoxColumn4.FieldName = "Memo";
            gridViewTextBoxColumn4.HeaderText = "备注";
            gridViewTextBoxColumn4.Name = "clMemo";
            gridViewTextBoxColumn4.Width = 100;
            this.dgItems.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            entityColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewTextBoxColumn4});
            this.dgItems.Name = "dgItems";
            this.dgItems.ShowGroupPanel = false;
            this.dgItems.Size = new System.Drawing.Size(683, 243);
            this.dgItems.TabIndex = 6;
            this.dgItems.Text = "radGridView1";
            // 
            // PurchaseInstockView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "PurchaseInstockView";
            this.Size = new System.Drawing.Size(683, 450);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRefTaskNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInstockDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPurchaseBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private mzerp.UI.Controls.EntityPicker tbWarehouse;
        private mzerp.UI.Controls.EntityPicker tbPurchaseBy;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton btPrint;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.Label label7;
        private Controls.EntityPicker tbVendor;
        private Telerik.WinControls.UI.RadTextBox tbMemo;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadTextBox tbCreateTime;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox tbCreateBy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadDateTimePicker tbInstockDate;
        private Telerik.WinControls.UI.RadTextBox tbTaskNo;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadGridView dgItems;
        private Telerik.WinControls.UI.RadTextBox tbRefTaskNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
    }
}
