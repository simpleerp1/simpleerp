﻿using dps.Common;
using mzerp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class MakeOutstockPickForm : Telerik.WinControls.UI.RadForm
    {
        private mzerp.Entities.MaterialOutstockTask _task;
        public mzerp.Entities.MaterialOutstockTask Task
        {
            get { return _task; }
            set
            {
                _task = value;
                this.qpWarehouse.Text = _task.Warehouse.Name;
            }
        }

        public MakeOutstockPickForm()
        {
            InitializeComponent();

            this.qpMaterial.PickerView = new MaterialPickerView();
            this.qpMaterial.DisplayMember = "Code";

            this.btSearch.Click += (s, e) => { this.Search(); };
            this.btAdd.Click += (s, e) => { this.AddToList(); };
            this.btClose.Click += (s, e) => { this.Close(); };
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[2];
            args[0] = this._task.WarehouseID;
            if (this.qpMaterial.SelectedEntityID.HasValue)
                args[1] = this.qpMaterial.SelectedEntityID.Value;
            return args;
        }

        private void Search()
        {
            var args = GetQueryArgs();
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var dt = dps.Common.SysService.Invoke("mzerp", "MaterialStockQueryService", "QueryForOutstock", args) as dps.Common.Data.DataTable;
                    if (dt == null)
                        throw new System.Exception("查询结果不正确");

                    dt.Columns.Add("OutQuantity", typeof(Decimal));
                    //检查存在的已拣货量
                    if (_task.Items.Count > 0)
                    {
                        Guid materialID, locationID;
                        //string lotNo;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            materialID = (Guid)dt.Rows[i]["MaterialID"];
                            locationID = (Guid)dt.Rows[i]["LocationID"];
                            for (int j = 0; j < _task.Items.Count; j++)
                            {
                                if (_task.Items[j].MaterialID == materialID)
                                {
                                    var item = _task.Items[j];
                                    for (int k = 0; k < item.Locations.Count; k++)
                                    {
                                        if (item.Locations[k].LocationID == locationID)
                                        {
                                            dt.Rows[i]["OutQuantity"] = item.Locations[k].Quantity;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dgItems.DataSource = dt;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void AddToList()
        {
            var dt = this.dgItems.DataSource as dps.Common.Data.DataTable;
            if (dt == null)
                return;

            object outQuantityValue;
            decimal availableQuantity = 0m;
            decimal outQuantity = 0m;
            Guid materialID;
            Guid locationID;
            //string lotNo;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                outQuantityValue = dt.Rows[i]["OutQuantity"];
                if (outQuantityValue != DBNull.Value)
                {
                    availableQuantity = (decimal)dt.Rows[i]["Quantity"];
                    outQuantity = (decimal)outQuantityValue;
                    locationID = (Guid)dt.Rows[i]["LocationID"];
                    if (outQuantity > 0)
                    {
                        materialID = (Guid)dt.Rows[i]["MaterialID"];
                        MaterialOutstockItem item = null;
                        //先查找有没有相同物料的出库项存在
                        for (int j = 0; j < _task.Items.Count; j++)
                        {
                            if (_task.Items[j].MaterialID == materialID)
                            {
                                item = _task.Items[j];
                                break;
                            }
                        }
                        if (item == null)
                        {
                            item = new MaterialOutstockItem();
                            item.MaterialID = materialID;
                            item.Quantity = 0;
                            //item.ReturnedQuantity = 0.0m;
                            //item.TotalPrice = 0.0m;
                            _task.Items.Add(item);
                        }

                        //再查找对应的location并计算差异量
                        decimal diff = 0m;
                        bool founded = false;
                        for (int k = 0; k < item.Locations.Count; k++)
                        {
                            //Todo:注意：个别计价需要判断批号是否相同
                            if (item.Locations[k].LocationID == locationID)
                            {
                                decimal oldvalue = item.Locations[k].Quantity;
                                item.Locations[k].Quantity = outQuantity;
                                diff = outQuantity - oldvalue;
                                founded = true;
                                break;
                            }
                        }
                        if (founded)
                            item.Quantity += diff;
                        else
                        {
                            var location = new MaterialOutstockLocation();
                            location.LocationID = locationID;
                            location.Quantity = outQuantity; //Todo:个别计价需要设置批号及价格
                            item.Locations.Add(location);
                            item.Quantity += outQuantity;
                        }
                    }
                }
            }
        }
    }
}
