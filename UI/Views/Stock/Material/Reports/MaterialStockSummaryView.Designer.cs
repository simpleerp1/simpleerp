﻿namespace mzerp.UI
{
    partial class MaterialStockSummaryView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.btExport = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpMonth = new Telerik.WinControls.UI.RadSpinEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.qpYear = new Telerik.WinControls.UI.RadSpinEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.dgList = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(657, 30);
            this.radCommandBar1.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSearch,
            this.btExport});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btExport
            // 
            this.btExport.AccessibleDescription = "导出";
            this.btExport.AccessibleName = "导出";
            this.btExport.DisplayName = "commandBarButton1";
            this.btExport.DrawText = true;
            this.btExport.Image = global::mzerp.UI.Properties.Resources.ExportExcel16;
            this.btExport.Name = "btExport";
            this.btExport.Text = "导出";
            this.btExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btExport.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpMonth);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.qpYear);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(657, 52);
            this.radGroupBox1.TabIndex = 6;
            this.radGroupBox1.Text = "查询条件";
            // 
            // qpMonth
            // 
            this.qpMonth.Location = new System.Drawing.Point(168, 21);
            this.qpMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.qpMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.qpMonth.Name = "qpMonth";
            this.qpMonth.Size = new System.Drawing.Size(35, 20);
            this.qpMonth.TabIndex = 3;
            this.qpMonth.TabStop = false;
            this.qpMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "月：";
            // 
            // qpYear
            // 
            this.qpYear.Location = new System.Drawing.Point(68, 21);
            this.qpYear.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.qpYear.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qpYear.Name = "qpYear";
            this.qpYear.Size = new System.Drawing.Size(55, 20);
            this.qpYear.TabIndex = 2;
            this.qpYear.TabStop = false;
            this.qpYear.Value = new decimal(new int[] {
            2014,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "年：";
            // 
            // dgList
            // 
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 82);
            // 
            // dgList
            // 
            gridViewTextBoxColumn1.FieldName = "Warehouse";
            gridViewTextBoxColumn1.HeaderText = "仓库";
            gridViewTextBoxColumn1.IsPinned = true;
            gridViewTextBoxColumn1.Name = "clWarehouse";
            gridViewTextBoxColumn1.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Code";
            gridViewTextBoxColumn2.HeaderText = "物料编号";
            gridViewTextBoxColumn2.IsPinned = true;
            gridViewTextBoxColumn2.Name = "clCode";
            gridViewTextBoxColumn2.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn2.Width = 56;
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "名称";
            gridViewTextBoxColumn3.IsPinned = true;
            gridViewTextBoxColumn3.Name = "clName";
            gridViewTextBoxColumn3.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "Spec";
            gridViewTextBoxColumn4.HeaderText = "规格";
            gridViewTextBoxColumn4.IsPinned = true;
            gridViewTextBoxColumn4.Name = "clSpec";
            gridViewTextBoxColumn4.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "MeasureUnit";
            gridViewTextBoxColumn5.HeaderText = "单位";
            gridViewTextBoxColumn5.IsPinned = true;
            gridViewTextBoxColumn5.Name = "clMeasureUnit";
            gridViewTextBoxColumn5.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn5.Width = 32;
            gridViewTextBoxColumn6.FieldName = "BeginningQuantity";
            gridViewTextBoxColumn6.HeaderText = "期初数量";
            gridViewTextBoxColumn6.Name = "clBeginningQuantity";
            gridViewTextBoxColumn6.Width = 56;
            gridViewTextBoxColumn7.FieldName = "BeginningValue";
            gridViewTextBoxColumn7.FormatString = "{0:N2}";
            gridViewTextBoxColumn7.HeaderText = "期初余额";
            gridViewTextBoxColumn7.Name = "clBeginningValue";
            gridViewTextBoxColumn7.Width = 56;
            gridViewTextBoxColumn8.FieldName = "PurchaseInQuantity";
            gridViewTextBoxColumn8.HeaderText = "采购入库数量";
            gridViewTextBoxColumn8.Name = "clPurchaseInQuantity";
            gridViewTextBoxColumn8.Width = 79;
            gridViewTextBoxColumn9.FieldName = "PurchaseInValue";
            gridViewTextBoxColumn9.FormatString = "{0:N2}";
            gridViewTextBoxColumn9.HeaderText = "采购入库金额";
            gridViewTextBoxColumn9.Name = "clPurchaseInValue";
            gridViewTextBoxColumn9.Width = 79;
            gridViewTextBoxColumn10.FieldName = "MakeReturnQuantity";
            gridViewTextBoxColumn10.HeaderText = "退料入库数量";
            gridViewTextBoxColumn10.Name = "clMakeReturnQuantity";
            gridViewTextBoxColumn10.Width = 79;
            gridViewTextBoxColumn11.FieldName = "MakeReturnValue";
            gridViewTextBoxColumn11.FormatString = "{0:N2}";
            gridViewTextBoxColumn11.HeaderText = "退料入库金额";
            gridViewTextBoxColumn11.Name = "clMakeReturnValue";
            gridViewTextBoxColumn11.Width = 79;
            gridViewTextBoxColumn12.FieldName = "OtherInQuantity";
            gridViewTextBoxColumn12.HeaderText = "其他入库数量";
            gridViewTextBoxColumn12.Name = "clOtherInQuantity";
            gridViewTextBoxColumn12.Width = 79;
            gridViewTextBoxColumn13.FieldName = "OtherInValue";
            gridViewTextBoxColumn13.FormatString = "{0:N2}";
            gridViewTextBoxColumn13.HeaderText = "其他入库金额";
            gridViewTextBoxColumn13.Name = "clOtherInValue";
            gridViewTextBoxColumn13.Width = 79;
            gridViewTextBoxColumn14.FieldName = "MakeOutQuantity";
            gridViewTextBoxColumn14.HeaderText = "领料出库数量";
            gridViewTextBoxColumn14.Name = "clMakeOutQuantity";
            gridViewTextBoxColumn14.Width = 79;
            gridViewTextBoxColumn15.FieldName = "MakeOutValue";
            gridViewTextBoxColumn15.FormatString = "{0:N2}";
            gridViewTextBoxColumn15.HeaderText = "领料出库金额";
            gridViewTextBoxColumn15.Name = "clMakeOutValue";
            gridViewTextBoxColumn15.Width = 79;
            gridViewTextBoxColumn16.FieldName = "PurchaseReturnQuantity";
            gridViewTextBoxColumn16.HeaderText = "采购退货数量";
            gridViewTextBoxColumn16.Name = "clPurchaseReturnQuantity";
            gridViewTextBoxColumn16.Width = 79;
            gridViewTextBoxColumn17.FieldName = "PurchaseReturnValue";
            gridViewTextBoxColumn17.FormatString = "{0:N2}";
            gridViewTextBoxColumn17.HeaderText = "采购退货金额";
            gridViewTextBoxColumn17.Name = "clPurchaseReturnValue";
            gridViewTextBoxColumn17.Width = 79;
            gridViewTextBoxColumn18.FieldName = "OtherOutQuantity";
            gridViewTextBoxColumn18.HeaderText = "其他出库数量";
            gridViewTextBoxColumn18.Name = "clOtherOutQuantity";
            gridViewTextBoxColumn18.Width = 79;
            gridViewTextBoxColumn19.FieldName = "OtherOutValue";
            gridViewTextBoxColumn19.FormatString = "{0:N2}";
            gridViewTextBoxColumn19.HeaderText = "其他出库金额";
            gridViewTextBoxColumn19.Name = "clOtherOutValue";
            gridViewTextBoxColumn19.Width = 79;
            gridViewTextBoxColumn20.FieldName = "EndingQuantity";
            gridViewTextBoxColumn20.HeaderText = "期末数量";
            gridViewTextBoxColumn20.Name = "clEndingQuantity";
            gridViewTextBoxColumn20.Width = 56;
            gridViewTextBoxColumn21.FieldName = "EndingValue";
            gridViewTextBoxColumn21.FormatString = "{0:N2}";
            gridViewTextBoxColumn21.HeaderText = "期末余额";
            gridViewTextBoxColumn21.Name = "clEndingValue";
            gridViewTextBoxColumn21.Width = 56;
            this.dgList.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21});
            this.dgList.Name = "dgList";
            this.dgList.ReadOnly = true;
            this.dgList.Size = new System.Drawing.Size(657, 434);
            this.dgList.TabIndex = 7;
            this.dgList.Text = "radGridView1";
            // 
            // MaterialStockSummaryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "MaterialStockSummaryView";
            this.Size = new System.Drawing.Size(657, 516);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Telerik.WinControls.UI.CommandBarButton btExport;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadSpinEditor qpMonth;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadSpinEditor qpYear;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadGridView dgList;
    }
}
