﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class MaterialStockSummaryView : UserControl
    {
        private ColumnGroupsViewDefinition cgv; 

        public MaterialStockSummaryView()
        {
            InitializeComponent();

            this.qpYear.Value = DateTime.Now.Year;
            this.qpMonth.Value = DateTime.Now.Month;

            this.InitColumnGroups();

            this.btSearch.Click += (s, e) => { Search(); };
        }

        private void InitColumnGroups()
        {
            this.cgv = new ColumnGroupsViewDefinition();
            var cg = new GridViewColumnGroup("物料");
            cg.IsPinned = true;
            this.cgv.ColumnGroups.Add(cg);
            var cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[0]);
            cgr.Columns.Add(this.dgList.Columns[1]);
            cgr.Columns.Add(this.dgList.Columns[2]);
            cgr.Columns.Add(this.dgList.Columns[3]);
            cgr.Columns.Add(this.dgList.Columns[4]);

            cg = new GridViewColumnGroup("期初");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[5]);
            cgr.Columns.Add(this.dgList.Columns[6]);

            cg = new GridViewColumnGroup("本期入库");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[7]);
            cgr.Columns.Add(this.dgList.Columns[8]);
            cgr.Columns.Add(this.dgList.Columns[9]);
            cgr.Columns.Add(this.dgList.Columns[10]);
            cgr.Columns.Add(this.dgList.Columns[11]);
            cgr.Columns.Add(this.dgList.Columns[12]);

            cg = new GridViewColumnGroup("本期出库");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[13]);
            cgr.Columns.Add(this.dgList.Columns[14]);
            cgr.Columns.Add(this.dgList.Columns[15]);
            cgr.Columns.Add(this.dgList.Columns[16]);
            cgr.Columns.Add(this.dgList.Columns[17]);
            cgr.Columns.Add(this.dgList.Columns[18]);

            cg = new GridViewColumnGroup("期末");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[19]);
            cgr.Columns.Add(this.dgList.Columns[20]);

            this.dgList.ViewDefinition = cgv;

            //初始化分组汇总
            GridViewSummaryRowItem sumRow = new GridViewSummaryRowItem();
            sumRow.Add(new GridViewSummaryItem("clBeginningValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clPurchaseInValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clMakeReturnValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clOtherInValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clMakeOutValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clPurchaseReturnValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clOtherOutValue", "{0:N2}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clEndingValue", "{0:N2}", GridAggregateFunction.Sum));
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item1);
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item2);
            this.dgList.MasterTemplate.SummaryRowsBottom.Add(sumRow);
            this.dgList.MasterTemplate.ShowTotals = true;
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var result = dps.Common.SysService.Invoke("mzerp", "MaterialStockSummaryService", "Query",
                                        (int)qpYear.Value, (int)qpMonth.Value,null,null) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dgList.DataSource = result;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }
    }
}
