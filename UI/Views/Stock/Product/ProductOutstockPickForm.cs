﻿using dps.Common;
using mzerp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class ProductOutstockPickForm : Telerik.WinControls.UI.RadForm
    {
        private object[] _queryArgs;

        private mzerp.Entities.ProductOutstockTask _task;
        public mzerp.Entities.ProductOutstockTask Task
        {
            get { return _task; }
            set { _task = value; }
        }

        public ProductOutstockPickForm()
        {
            InitializeComponent();
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.btAdd.Click += (s, e) => { this.AddToList(); };
            this.btClose.Click += (s, e) => { this.Close(); };
            this.dgItems.CellValueChanged += OnCellValueChanged;
        }

        private void OnCellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            decimal boxQuantity = (decimal)e.Row.Cells["clBoxQuantity"].Value;
            int boxes = e.Row.Cells["clOutBoxes"].Value == DBNull.Value ? 0 : (int)e.Row.Cells["clOutBoxes"].Value;
            decimal change = e.Row.Cells["clOutChange"].Value == DBNull.Value ? 0m : (decimal)e.Row.Cells["clOutChange"].Value;
            e.Row.Cells["clOutQuantity"].Value = boxes * boxQuantity + change;
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[6];
            args[0] = qpMaterial.Text;
            args[1] = qpSpec.Text;
            args[2] = qpBatchNo.Text;
            args[3] = qpSpecValue1.Text;
            args[4] = qpSpecValue2.Text;
            args[5] = qpLevel.SelectedEntityID;
            return args;
        }

        private void Search()
        {
            var args = GetQueryArgs();
            if (this._queryArgs == null)
            {
                if (args == null || args.Length == 0)
                    this._queryArgs = new object[2];
                else
                    this._queryArgs = new object[args.Length + 2];
            }
            if (this._queryArgs.Length > 2)
            {
                for (int i = 2; i < this._queryArgs.Length; i++)
                {
                    this._queryArgs[i] = args[i - 2];
                }
            }

            this._queryArgs[0] = 0;
            this._queryArgs[1] = -2;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var dt = dps.Common.SysService.Invoke("mzerp", "ProductStockQueryService", "Query", _queryArgs) as dps.Common.Data.DataTable;
                    if (dt == null)
                        throw new System.Exception("查询结果不正确");
                    dt.Columns.Add("OutBoxes", typeof(int));
                    dt.Columns.Add("OutChange", typeof(decimal));
                    dt.Columns.Add("OutQuantity", typeof(Decimal));
                    //检查存在的已拣货量
                    if (_task.Items.Count > 0)
                    {
                        Guid materialID, levelID, specID, specValue1ID, specValue2ID;
                        string batchNo;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            materialID = (Guid)dt.Rows[i]["MaterialID"];
                            specID = (Guid)dt.Rows[i]["SpecID"];
                            batchNo = (string)dt.Rows[i]["BatchNo"];
                            specValue1ID = (Guid)dt.Rows[i]["SpecValue1ID"];
                            specValue2ID = (Guid)dt.Rows[i]["SpecValue2ID"];
                            levelID = (Guid)dt.Rows[i]["LevelID"];
                            for (int j = 0; j < _task.Items.Count; j++)
                            {
                                if (_task.Items[j].MaterialID == materialID
                                && _task.Items[j].SpecID == specID
                                && _task.Items[j].BatchNo == batchNo
                                && _task.Items[j].SpecValue1ID == specValue1ID
                                && _task.Items[j].SpecValue2ID == specValue2ID
                                && _task.Items[j].LevelID == levelID)
                                {
                                    dt.Rows[i]["OutBoxes"] = _task.Items[j].Boxes;
                                    dt.Rows[i]["OutChange"] = _task.Items[j].Change;
                                    dt.Rows[i]["OutQuantity"] = _task.Items[j].Quantity;
                                }
                            }
                        }
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dgItems.DataSource = dt;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void AddToList()
        {
            var dt = this.dgItems.DataSource as dps.Common.Data.DataTable;
            if (dt == null)
                return;

            object outQuantityValue = null;
            decimal availableQuantity = 0m;
            int outBoxes = 0;
            decimal outQuantity = 0m;
            decimal outChange = 0m;
            decimal boxQuantity = 0m;
            Guid materialID, levelID, specID, specValue1ID, specValue2ID;
            string batchNo;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                outQuantityValue = dt.Rows[i]["OutQuantity"];
                if (outQuantityValue != DBNull.Value)
                {
                    availableQuantity = (decimal)dt.Rows[i]["Quantity"];
                    outQuantity = (decimal)outQuantityValue;
                    if (outQuantity > 0)
                    {
                        materialID = (Guid)dt.Rows[i]["MaterialID"];
                        specID = (Guid)dt.Rows[i]["SpecID"];
                        batchNo = (string)dt.Rows[i]["BatchNo"];
                        specValue1ID = (Guid)dt.Rows[i]["SpecValue1ID"];
                        specValue2ID = (Guid)dt.Rows[i]["SpecValue2ID"];
                        levelID = (Guid)dt.Rows[i]["LevelID"];
                        outBoxes = (int)dt.Rows[i]["OutBoxes"];
                        boxQuantity = (decimal)dt.Rows[i]["BoxQuantity"];
                        outChange = dt.Rows[i]["OutChange"] == DBNull.Value ? 0m : (decimal)dt.Rows[i]["OutChange"];
                        ProductOutstockItem item = null;
                        //查找有没有相同项存在
                        for (int j = 0; j < _task.Items.Count; j++)
                        {
                            if (_task.Items[j].MaterialID == materialID
                                && _task.Items[j].SpecID == specID
                                && _task.Items[j].BatchNo == batchNo
                                && _task.Items[j].SpecValue1ID == specValue1ID
                                && _task.Items[j].SpecValue2ID == specValue2ID
                                && _task.Items[j].LevelID == levelID)
                            {
                                item = _task.Items[j];
                                break;
                            }
                        }
                        if (item == null)
                        {
                            item = new ProductOutstockItem();
                            item.MaterialID = materialID;
                            item.SpecID = specID;
                            item.BatchNo = batchNo;
                            item.SpecValue1ID = specValue1ID;
                            item.SpecValue2ID = specValue2ID;
                            item.LevelID = levelID;
                            item.Boxes = outBoxes;
                            item.Change = outChange;
                            item.QuantityOfBox = boxQuantity;
                            item.Quantity = outQuantity;
                            _task.Items.Add(item);
                        }
                        else
                        {
                            item.Boxes = outBoxes;
                            item.Change = outChange;
                            item.QuantityOfBox = boxQuantity;
                            item.Quantity = outQuantity;
                        }
                    }
                }
            }
        }
    }
}
