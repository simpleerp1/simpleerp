﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace mzerp.UI
{
    public partial class ProductStockQuery : UserControl
    {
        public ProductStockQuery()
        {
            InitializeComponent();

            this.qpLevel.PickerView = new ProductLevelPickerView();
            this.qpLevel.DisplayMember = "Name";

            this.dgList.GridView.ShowGroupPanel = true;
            this.dgList.QueryMethod = "mzerp.ProductStockQueryService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.dgList.AllowPage = false;
            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btExport.Click += (s, e) => { ExportService.ExportGridViewToExcel(this.dgList.GridView, "当前库存表"); };

            //初始化分组汇总
            GridViewSummaryRowItem item1 = new GridViewSummaryRowItem();
            item1.Add(new GridViewSummaryItem("clQuantity", "合计:{0}", GridAggregateFunction.Sum));
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item1);
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item2);
            this.dgList.GridView.MasterTemplate.SummaryRowsBottom.Add(item1);
            this.dgList.GridView.MasterTemplate.ShowTotals = true;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[6];
            args[0] = qpMaterial.Text;
            args[1] = qpSpec.Text;
            args[2] = qpBatchNo.Text;
            args[3] = qpSpecValue1.Text;
            args[4] = qpSpecValue2.Text;
            args[5] = qpLevel.SelectedEntityID;
            return args;
        }

    }
}
