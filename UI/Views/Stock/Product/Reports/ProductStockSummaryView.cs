﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class ProductStockSummaryView : UserControl
    {

        private ColumnGroupsViewDefinition cgv; 

        public ProductStockSummaryView()
        {
            InitializeComponent();

            this.qpYear.Value = DateTime.Now.Year;
            this.qpMonth.Value = DateTime.Now.Month;

            this.InitColumnGroups();

            this.btSearch.Click += (s, e) => { Search(); };
        }

        private void InitColumnGroups()
        {
            this.cgv = new ColumnGroupsViewDefinition();
            var cg = new GridViewColumnGroup("规格");
            cg.IsPinned = true;
            this.cgv.ColumnGroups.Add(cg);
            var cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[0]);

            cg = new GridViewColumnGroup("期初");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[1]);

            cg = new GridViewColumnGroup("本期入库");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[2]);
            cgr.Columns.Add(this.dgList.Columns[3]);
            cgr.Columns.Add(this.dgList.Columns[4]);

            cg = new GridViewColumnGroup("本期出库");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[5]);
            cgr.Columns.Add(this.dgList.Columns[6]);
            cgr.Columns.Add(this.dgList.Columns[7]);

            cg = new GridViewColumnGroup("期末");
            this.cgv.ColumnGroups.Add(cg);
            cgr = new GridViewColumnGroupRow();
            cg.Rows.Add(cgr);
            cgr.Columns.Add(this.dgList.Columns[8]);

            this.dgList.ViewDefinition = cgv;

            //初始化分组汇总
            GridViewSummaryRowItem sumRow = new GridViewSummaryRowItem();
            sumRow.Add(new GridViewSummaryItem("clBeginningQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clMakeInQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clMakeReturnQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clOtherInQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clSaleOutQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clSaleReturnQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clOtherOutQuantity", "{0:N4}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clEndingQuantity", "{0:N4}", GridAggregateFunction.Sum));
            this.dgList.MasterTemplate.SummaryRowsBottom.Add(sumRow);
            this.dgList.MasterTemplate.ShowTotals = true;
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var result = dps.Common.SysService.Invoke("mzerp", "ProductStockSummaryService", "Query",
                                        (int)qpYear.Value, (int)qpMonth.Value, null) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dgList.DataSource = result;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }
    }
}
