﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.IO;
using Telerik.WinControls.UI.Export;
using Telerik.Charting;

namespace mzerp.UI
{
    public partial class MakeInSummary : UserControl
    {


        public MakeInSummary()
        {
            InitializeComponent();

            this.btSearch.Click += (s, e) => { this.Search(); };
            this.radChartView1.ShowSmartLabels = true;
            this.radChartView1.LabelFormatting += OnPieLabelFormatting;
        }

        private void OnPieLabelFormatting(object sender, ChartViewLabelFormattingEventArgs e)
        {
            PieDataPoint p = e.LabelElement.DataPoint as PieDataPoint;
            if (p != null)
            {
                p.Label = string.Format("[{0}] {1:N1}%", p.Name, p.Percent);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.qpYear.Value = DateTime.Now.Year;
            this.qpMonth.Value = DateTime.Now.Month;

            this.Search();
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var result = dps.Common.SysService.Invoke("mzerp", "ProductStockQueryService", "GetMakeInReport",
                                        (int)this.qpYear.Value, (int)this.qpMonth.Value) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");

                    Dictionary<string, PieDataPoint> dic = new Dictionary<string, PieDataPoint>();
                    PieSeries pieSeries = new PieSeries();
                    pieSeries.ShowLabels = true;
                    //pieSeries.LabelFormat = "{0:P2}";
                    //pieSeries.LabelMode = PieLabelModes.Horizontal;
                    pieSeries.DrawLinesToLabels = true;
                    pieSeries.SyncLinesToLabelsColor = true;

                    string spec = null;
                    PieDataPoint point = null;
                    for (int i = 0; i < result.Rows.Count; i++)
                    {
                        spec = result.Rows[i]["Spec"].ToString();
                        if (!dic.TryGetValue(spec, out point))
                        {
                            point = new PieDataPoint(0d, spec);
                            point.Label = spec; //string.Format("{0} {1:P2}", spec, point.Percent);
                            pieSeries.DataPoints.Add(point);
                            dic.Add(spec, point);
                        }
                        point.Value += (double)((decimal)result.Rows[i]["InStock"]);
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        //显示汇总报表
                        var report = new Reports.Stock.ProductMakeInSummaryReport();
                        report.ReportParameters["Year"].Value = (int)this.qpYear.Value;
                        report.ReportParameters["Month"].Value = (int)this.qpMonth.Value;
                        report.MainDataSource.DataSource = result;
                        var reportSource = new Telerik.Reporting.InstanceReportSource();
                        reportSource.ReportDocument = report;
                        this.rvSummary.ReportSource = reportSource;
                        this.rvSummary.RefreshReport();
                        //显示比例图
                        this.radChartView1.Series.Clear();
                        this.radChartView1.Series.Add(pieSeries);
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

    }
}
