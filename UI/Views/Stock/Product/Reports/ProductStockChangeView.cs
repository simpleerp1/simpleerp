﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.Charting;

namespace mzerp.UI
{
    public partial class ProductStockChangeView : UserControl
    {
        public ProductStockChangeView()
        {
            InitializeComponent();

            this.qpYear1.Value = this.qpYear2.Value = (int)DateTime.Now.Year;
            this.btSearch.Click += (s, e) => { Search(); };
            this.Load += (s, e) => { Search(); };
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    int startYear = (int)qpYear1.Value;
                    int endYear = (int)qpYear2.Value;

                    var result = dps.Common.SysService.Invoke("mzerp", "ProductStockSummaryService", "GetChangeQuery",
                                        startYear, endYear) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");


                    //CategoricalAxis horizontalAxis = new CategoricalAxis();
                    //LinearAxis verticalAxis = new LinearAxis();
                    //verticalAxis.AxisType = AxisType.Second;

                    Dictionary<string, Dictionary<string, CategoricalDataPoint>> dic = new Dictionary<string, Dictionary<string, CategoricalDataPoint>>();
                    List<LineSeries> ls = new List<LineSeries>();

                    for (int i = 0; i < result.Rows.Count; i++)
                    {
                        var p = GetPoint(startYear, endYear, dic, ls, result.Rows[i]);
                        p.Value = (double)((decimal)result.Rows[i]["EndingQuantity"]);
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.radChartView1.Series.Clear();
                        for (int i = 0; i < ls.Count; i++)
                        {
                            this.radChartView1.Series.Add(ls[i]);
                        }
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private CategoricalDataPoint GetPoint(int startYear, int endYear, Dictionary<string, Dictionary<string, CategoricalDataPoint>> ds, List<LineSeries> ls, DataRow row)
        {
            var specName = (string)row["SpecName"];
            Dictionary<string, CategoricalDataPoint> ss = null;
            if (!ds.TryGetValue(specName, out ss))
            {
                ss = new Dictionary<string, CategoricalDataPoint>();
                ds.Add(specName, ss);
                var s = new LineSeries();
                s.LegendTitle = specName;
                ls.Add(s);

                string key = null;
                for (int i = startYear; i <= endYear; i++)
                {
                    for (int j = 1; j < 13; j++)
                    {
                        key = string.Format("{0}年{1}月", i, j);
                        var p = new CategoricalDataPoint(0, key);
                        s.DataPoints.Add(p);
                        ss.Add(key, p);
                    }
                }
            }

            return ss[string.Format("{0}年{1}月", row["Year"], row["Month"])];
        }

    }


}
