﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.IO;
using Telerik.WinControls.UI.Export;

namespace mzerp.UI
{
    public partial class CustomerStockSummary : UserControl
    {
        public CustomerStockSummary()
        {
            InitializeComponent();
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.btExport.Click += (s, e) => { ExportService.ExportGridViewToExcel(this.gvList, "客户出入库汇总表"); };

            //初始化分组汇总
            GridViewSummaryRowItem sumRow = new GridViewSummaryRowItem();
            sumRow.Add(new GridViewSummaryItem("clOutStock", "合计:{0}", GridAggregateFunction.Sum));
            sumRow.Add(new GridViewSummaryItem("clInStock", "合计:{0}", GridAggregateFunction.Sum));
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item1);
            //this.gvList.MasterTemplate.SummaryRowGroupHeaders.Add(item2);
            this.gvList.MasterTemplate.SummaryRowsBottom.Add(sumRow);
            this.gvList.MasterTemplate.ShowTotals = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.qpStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            this.qpEndDate.Value = this.qpStartDate.Value.AddMonths(1).AddDays(-1);

            this.Search();
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var result = dps.Common.SysService.Invoke("mzerp", "ProductStockQueryService", "GetCustomerReport",
                                        this.qpStartDate.Value, this.qpEndDate.Value) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.gvList.DataSource = result;
                        //显示汇总报表
                        var report = new Reports.Stock.ProductCustomerSummaryReport();
                        report.MainDataSource.DataSource = result;
                        var reportSource = new Telerik.Reporting.InstanceReportSource();
                        reportSource.ReportDocument = report;
                        this.reportViewer1.ReportSource = reportSource;
                        this.reportViewer1.RefreshReport();
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

    }
}
