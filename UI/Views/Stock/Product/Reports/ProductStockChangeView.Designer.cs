﻿namespace mzerp.UI
{
    partial class ProductStockChangeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.btExport = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpYear2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.qpYear1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(750, 30);
            this.radCommandBar1.TabIndex = 7;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSearch,
            this.btExport});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btExport
            // 
            this.btExport.AccessibleDescription = "导出";
            this.btExport.AccessibleName = "导出";
            this.btExport.DisplayName = "commandBarButton1";
            this.btExport.DrawText = true;
            this.btExport.Image = global::mzerp.UI.Properties.Resources.ExportExcel16;
            this.btExport.Name = "btExport";
            this.btExport.Text = "导出";
            this.btExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btExport.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpYear2);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.qpYear1);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(750, 52);
            this.radGroupBox1.TabIndex = 8;
            this.radGroupBox1.Text = "查询条件";
            // 
            // qpYear2
            // 
            this.qpYear2.Location = new System.Drawing.Point(234, 21);
            this.qpYear2.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.qpYear2.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qpYear2.Name = "qpYear2";
            this.qpYear2.Size = new System.Drawing.Size(55, 20);
            this.qpYear2.TabIndex = 3;
            this.qpYear2.TabStop = false;
            this.qpYear2.Value = new decimal(new int[] {
            2014,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "结束年份：";
            // 
            // qpYear1
            // 
            this.qpYear1.Location = new System.Drawing.Point(105, 21);
            this.qpYear1.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.qpYear1.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qpYear1.Name = "qpYear1";
            this.qpYear1.Size = new System.Drawing.Size(55, 20);
            this.qpYear1.TabIndex = 2;
            this.qpYear1.TabStop = false;
            this.qpYear1.Value = new decimal(new int[] {
            2014,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "开始年份：";
            // 
            // radChartView1
            // 
            this.radChartView1.AreaDesign = cartesianArea1;
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radChartView1.Location = new System.Drawing.Point(0, 82);
            this.radChartView1.Name = "radChartView1";
            this.radChartView1.ShowGrid = false;
            this.radChartView1.ShowLegend = true;
            this.radChartView1.Size = new System.Drawing.Size(750, 468);
            this.radChartView1.TabIndex = 9;
            this.radChartView1.Text = "radChartView1";
            // 
            // ProductStockChangeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radChartView1);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ProductStockChangeView";
            this.Size = new System.Drawing.Size(750, 550);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Telerik.WinControls.UI.CommandBarButton btExport;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadSpinEditor qpYear1;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadSpinEditor qpYear2;
        private Telerik.WinControls.UI.RadChartView radChartView1;
    }
}
