﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class ProductOutstockList : UserControl
    {
        public ProductOutstockList()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.ProductOutstockService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += (s, e) => { this.dgList.LoadData(); };
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[3];
            if (qcDate.Checked)
            {
                args[0] = qpStartDate.Value;
                args[1] = qpEndDate.Value;
            }
            if (qcEmploee.Checked)
                args[2] = qpEmploee.Text;

            return args;
        }

        #region Event handlers
        private void OnCreate()
        {
            var entity = new ProductOutstockTask();
            entity.OutstockDate = entity.CreateTime = DateTime.Now;
            entity.CreateBy = SystemService.CurrentEmploee;

            var view = new ProductOutstockView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "成品出库");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new ProductOutstockTask(dps.Common.Data.Entity.Retrieve(ProductOutstockTask.EntityModelID, id));

            var view = new ProductOutstockView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "成品出库");
        }
        #endregion
    }
}
