﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mzerp.Entities;
using Telerik.WinControls.UI;

namespace mzerp.UI
{
    public partial class ProductInstockList : UserControl
    {
        public ProductInstockList()
        {
            InitializeComponent();

            this.dgList.QueryMethod = "mzerp.ProductInstockService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            //处理当前用的权限
            this.btNew.Enabled = Permissions.ProductInstock_Create.IsOwn;
            //this.btOpen.Enabled = Permissions.ProductInstock_Edit.IsOwn;
            this.btDelete.Enabled = Permissions.ProductInstock_Delete.IsOwn;

            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[3];
            if (qcDate.Checked)
            {
                args[0] = qpStartDate.Value;
                args[1] = qpEndDate.Value;
            }
            if (qcEmploee.Checked)
                args[2] = qpEmploee.Text;

            return args;
        }

        #region Event handlers
        private void OnCreate()
        {
            var entity = new ProductInstockTask();
            entity.InstockDate = entity.CreateTime = DateTime.Now;
            entity.CreateBy = SystemService.CurrentEmploee;

            var view = new ProductInstockView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "成品入库");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new ProductInstockTask(dps.Common.Data.Entity.Retrieve(ProductInstockTask.EntityModelID, id));

            var view = new ProductInstockView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "成品入库");
        }
        #endregion
    }
}
