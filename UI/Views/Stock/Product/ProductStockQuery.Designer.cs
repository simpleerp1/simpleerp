﻿namespace mzerp.UI
{
    partial class ProductStockQuery
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.btExport = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpSpecValue2 = new Telerik.WinControls.UI.RadTextBox();
            this.qpSpecValue1 = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.qpBatchNo = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.qpSpec = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.qpMaterial = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.qpLevel = new mzerp.UI.Controls.EntityPicker();
            this.dgList = new mzerp.UI.Controls.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpBatchNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSearch,
            this.btExport});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btExport
            // 
            this.btExport.AccessibleDescription = "导出";
            this.btExport.AccessibleName = "导出";
            this.btExport.DisplayName = "commandBarButton1";
            this.btExport.DrawText = true;
            this.btExport.Image = global::mzerp.UI.Properties.Resources.ExportExcel16;
            this.btExport.Name = "btExport";
            this.btExport.Text = "导出";
            this.btExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btExport.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(687, 30);
            this.radCommandBar1.TabIndex = 3;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpSpecValue2);
            this.radGroupBox1.Controls.Add(this.qpSpecValue1);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.qpBatchNo);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.qpSpec);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.qpMaterial);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.qpLevel);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "库存查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(687, 94);
            this.radGroupBox1.TabIndex = 4;
            this.radGroupBox1.Text = "库存查询条件";
            // 
            // qpSpecValue2
            // 
            this.qpSpecValue2.Location = new System.Drawing.Point(246, 55);
            this.qpSpecValue2.Name = "qpSpecValue2";
            this.qpSpecValue2.Size = new System.Drawing.Size(100, 20);
            this.qpSpecValue2.TabIndex = 3;
            // 
            // qpSpecValue1
            // 
            this.qpSpecValue1.Location = new System.Drawing.Point(79, 55);
            this.qpSpecValue1.Name = "qpSpecValue1";
            this.qpSpecValue1.Size = new System.Drawing.Size(100, 20);
            this.qpSpecValue1.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(361, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "级别：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(181, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "分规值：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "规值：";
            // 
            // qpBatchNo
            // 
            this.qpBatchNo.Location = new System.Drawing.Point(413, 26);
            this.qpBatchNo.Name = "qpBatchNo";
            this.qpBatchNo.Size = new System.Drawing.Size(100, 20);
            this.qpBatchNo.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "批号：";
            // 
            // qpSpec
            // 
            this.qpSpec.Location = new System.Drawing.Point(246, 26);
            this.qpSpec.Name = "qpSpec";
            this.qpSpec.Size = new System.Drawing.Size(100, 20);
            this.qpSpec.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "规格：";
            // 
            // qpMaterial
            // 
            this.qpMaterial.Location = new System.Drawing.Point(79, 26);
            this.qpMaterial.Name = "qpMaterial";
            this.qpMaterial.Size = new System.Drawing.Size(100, 20);
            this.qpMaterial.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "材料：";
            // 
            // qpLevel
            // 
            this.qpLevel.DisplayMember = null;
            this.qpLevel.Location = new System.Drawing.Point(413, 55);
            this.qpLevel.Name = "qpLevel";
            this.qpLevel.PickerView = null;
            this.qpLevel.SelectedEntity = null;
            this.qpLevel.Size = new System.Drawing.Size(100, 22);
            this.qpLevel.TabIndex = 1;
            this.qpLevel.TabStop = false;
            // 
            // dgList
            // 
            this.dgList.AllowPage = true;
            gridViewTextBoxColumn1.FieldName = "Material";
            gridViewTextBoxColumn1.HeaderText = "材料";
            gridViewTextBoxColumn1.Name = "clMaterial";
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Spec";
            gridViewTextBoxColumn2.HeaderText = "规格";
            gridViewTextBoxColumn2.Name = "clSpec";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "BatchNo";
            gridViewTextBoxColumn3.HeaderText = "批号";
            gridViewTextBoxColumn3.Name = "clBatchNo";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "SpecValue1";
            gridViewTextBoxColumn4.HeaderText = "规值";
            gridViewTextBoxColumn4.Name = "clSpecValue1";
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "SpecValue2";
            gridViewTextBoxColumn5.HeaderText = "分规值";
            gridViewTextBoxColumn5.Name = "clSpecValue2";
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.FieldName = "Level";
            gridViewTextBoxColumn6.HeaderText = "级别";
            gridViewTextBoxColumn6.Name = "clLevel";
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn7.FieldName = "Quantity";
            gridViewTextBoxColumn7.HeaderText = "万粒数";
            gridViewTextBoxColumn7.Name = "clQuantity";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 80;
            gridViewTextBoxColumn8.FieldName = "ReturnedQuantity";
            gridViewTextBoxColumn8.HeaderText = "累计退货万粒数";
            gridViewTextBoxColumn8.Name = "clReturnedQuantity";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 90;
            this.dgList.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 124);
            this.dgList.Name = "dgList";
            this.dgList.PageSize = 20;
            this.dgList.Size = new System.Drawing.Size(687, 338);
            this.dgList.TabIndex = 5;
            // 
            // ProductStockQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ProductStockQuery";
            this.Size = new System.Drawing.Size(687, 462);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpBatchNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox qpSpec;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBox qpMaterial;
        private System.Windows.Forms.Label label1;
        private Controls.DataGrid dgList;
        private Telerik.WinControls.UI.RadTextBox qpBatchNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadTextBox qpSpecValue2;
        private Telerik.WinControls.UI.RadTextBox qpSpecValue1;
        private mzerp.UI.Controls.EntityPicker qpLevel;
        private Telerik.WinControls.UI.CommandBarButton btExport;

    }
}
