﻿namespace mzerp.UI
{
    partial class ProductOutstockView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            mzerp.UI.Controls.EntityColumn entityColumn1 = new mzerp.UI.Controls.EntityColumn();
            mzerp.UI.Controls.EntityColumn entityColumn2 = new mzerp.UI.Controls.EntityColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            mzerp.UI.Controls.EntityColumn entityColumn3 = new mzerp.UI.Controls.EntityColumn();
            mzerp.UI.Controls.EntityColumn entityColumn4 = new mzerp.UI.Controls.EntityColumn();
            mzerp.UI.Controls.EntityColumn entityColumn5 = new mzerp.UI.Controls.EntityColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.btDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btPick = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btPrint = new Telerik.WinControls.UI.CommandBarButton();
            this.tbCustomer = new mzerp.UI.Controls.EntityPicker();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbTaskType = new mzerp.UI.Controls.EnumPicker();
            this.label6 = new System.Windows.Forms.Label();
            this.tbMemo = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCreateTime = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCreateBy = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInstockDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbTaskNo = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgItems = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInstockDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(765, 30);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btSave,
            this.btDelete,
            this.commandBarSeparator1,
            this.btPick,
            this.commandBarSeparator2,
            this.btPrint});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "打开";
            this.btSave.AccessibleName = "打开";
            this.btSave.DisplayName = "commandBarButton1";
            this.btSave.DrawText = true;
            this.btSave.Image = global::mzerp.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btDelete
            // 
            this.btDelete.AccessibleDescription = "删除";
            this.btDelete.AccessibleName = "删除";
            this.btDelete.DisplayName = "commandBarButton2";
            this.btDelete.DrawText = true;
            this.btDelete.Image = global::mzerp.UI.Properties.Resources.Delete16;
            this.btDelete.Name = "btDelete";
            this.btDelete.Text = "删除";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // btPick
            // 
            this.btPick.AccessibleDescription = "出库拣货";
            this.btPick.AccessibleName = "出库拣货";
            this.btPick.DisplayName = "commandBarButton1";
            this.btPick.DrawText = true;
            this.btPick.Image = global::mzerp.UI.Properties.Resources.OutstockPick16;
            this.btPick.Name = "btPick";
            this.btPick.Text = "出库拣货";
            this.btPick.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPick.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.AccessibleDescription = "commandBarSeparator2";
            this.commandBarSeparator2.AccessibleName = "commandBarSeparator2";
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // btPrint
            // 
            this.btPrint.AccessibleDescription = "commandBarButton1";
            this.btPrint.AccessibleName = "commandBarButton1";
            this.btPrint.DisplayName = "commandBarButton1";
            this.btPrint.DrawText = true;
            this.btPrint.Image = global::mzerp.UI.Properties.Resources.Print16;
            this.btPrint.Name = "btPrint";
            this.btPrint.Text = "打印";
            this.btPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPrint.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbCustomer
            // 
            this.tbCustomer.DisplayMember = null;
            this.tbCustomer.Location = new System.Drawing.Point(319, 56);
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.PickerView = null;
            this.tbCustomer.SelectedEntity = null;
            this.tbCustomer.Size = new System.Drawing.Size(332, 20);
            this.tbCustomer.TabIndex = 27;
            this.tbCustomer.TabStop = false;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.label7);
            this.radGroupBox1.Controls.Add(this.tbTaskType);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.tbMemo);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.tbCreateTime);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.tbCreateBy);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.tbInstockDate);
            this.radGroupBox1.Controls.Add(this.tbTaskNo);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.tbCustomer);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "产品出库单";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(765, 154);
            this.radGroupBox1.TabIndex = 5;
            this.radGroupBox1.Text = "产品出库单";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(267, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "客户：";
            // 
            // tbTaskType
            // 
            this.tbTaskType.AutoCompleteDisplayMember = "LocalizedName.Value";
            this.tbTaskType.AutoCompleteValueMember = "Value";
            this.tbTaskType.DisplayMember = "LocalizedName.Value";
            this.tbTaskType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.tbTaskType.EnumModelID = "mzerp.ProductOutstockType";
            this.tbTaskType.Location = new System.Drawing.Point(102, 56);
            this.tbTaskType.Name = "tbTaskType";
            this.tbTaskType.Size = new System.Drawing.Size(114, 20);
            this.tbTaskType.TabIndex = 25;
            this.tbTaskType.Text = "enumPicker1";
            this.tbTaskType.ValueMember = "Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "出库类型：";
            // 
            // tbMemo
            // 
            this.tbMemo.Location = new System.Drawing.Point(102, 114);
            this.tbMemo.Name = "tbMemo";
            this.tbMemo.Size = new System.Drawing.Size(549, 20);
            this.tbMemo.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "备注：";
            // 
            // tbCreateTime
            // 
            this.tbCreateTime.Location = new System.Drawing.Point(319, 85);
            this.tbCreateTime.Name = "tbCreateTime";
            this.tbCreateTime.ReadOnly = true;
            this.tbCreateTime.Size = new System.Drawing.Size(114, 20);
            this.tbCreateTime.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(241, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "制单日期：";
            // 
            // tbCreateBy
            // 
            this.tbCreateBy.Location = new System.Drawing.Point(102, 85);
            this.tbCreateBy.Name = "tbCreateBy";
            this.tbCreateBy.ReadOnly = true;
            this.tbCreateBy.Size = new System.Drawing.Size(114, 20);
            this.tbCreateBy.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "制单员：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "出库日期：";
            // 
            // tbInstockDate
            // 
            this.tbInstockDate.Location = new System.Drawing.Point(319, 27);
            this.tbInstockDate.Name = "tbInstockDate";
            this.tbInstockDate.Size = new System.Drawing.Size(114, 20);
            this.tbInstockDate.TabIndex = 17;
            this.tbInstockDate.TabStop = false;
            this.tbInstockDate.Text = "2013年11月15日";
            this.tbInstockDate.Value = new System.DateTime(2013, 11, 15, 16, 52, 36, 151);
            // 
            // tbTaskNo
            // 
            this.tbTaskNo.Location = new System.Drawing.Point(102, 27);
            this.tbTaskNo.Name = "tbTaskNo";
            this.tbTaskNo.ReadOnly = true;
            this.tbTaskNo.Size = new System.Drawing.Size(114, 20);
            this.tbTaskNo.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "出库单号：";
            // 
            // dgItems
            // 
            this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItems.Location = new System.Drawing.Point(0, 184);
            // 
            // dgItems
            // 
            this.dgItems.MasterTemplate.AllowAddNewRow = false;
            this.dgItems.MasterTemplate.AutoGenerateColumns = false;
            entityColumn1.DisplayMember = null;
            entityColumn1.FieldName = "Material";
            entityColumn1.HeaderText = "材料";
            entityColumn1.Name = "clMaterial";
            entityColumn1.PickerView = null;
            entityColumn1.ReadOnly = true;
            entityColumn1.Width = 80;
            entityColumn2.DisplayMember = null;
            entityColumn2.FieldName = "Spec";
            entityColumn2.HeaderText = "规格";
            entityColumn2.Name = "clSpec";
            entityColumn2.PickerView = null;
            entityColumn2.Width = 60;
            gridViewTextBoxColumn1.FieldName = "BatchNo";
            gridViewTextBoxColumn1.HeaderText = "批号";
            gridViewTextBoxColumn1.Name = "clBatchNo";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 80;
            entityColumn3.DisplayMember = null;
            entityColumn3.FieldName = "SpecValue1";
            entityColumn3.HeaderText = "规值";
            entityColumn3.Name = "clSpecValue1";
            entityColumn3.PickerView = null;
            entityColumn3.Width = 60;
            entityColumn4.DisplayMember = null;
            entityColumn4.FieldName = "SpecValue2";
            entityColumn4.HeaderText = "分规值";
            entityColumn4.Name = "clSpecValue2";
            entityColumn4.PickerView = null;
            entityColumn4.Width = 60;
            entityColumn5.DisplayMember = null;
            entityColumn5.FieldName = "Level";
            entityColumn5.HeaderText = "级别";
            entityColumn5.Name = "clLevel";
            entityColumn5.PickerView = null;
            entityColumn5.ReadOnly = true;
            entityColumn5.Width = 90;
            gridViewDecimalColumn1.DecimalPlaces = 0;
            gridViewDecimalColumn1.FieldName = "Boxes";
            gridViewDecimalColumn1.HeaderText = "箱数";
            gridViewDecimalColumn1.Name = "clBoxes";
            gridViewDecimalColumn1.Width = 60;
            gridViewDecimalColumn2.DecimalPlaces = 4;
            gridViewDecimalColumn2.FieldName = "Change";
            gridViewDecimalColumn2.HeaderText = "零头数";
            gridViewDecimalColumn2.Name = "clChange";
            gridViewDecimalColumn2.Width = 60;
            gridViewDecimalColumn3.DecimalPlaces = 4;
            gridViewDecimalColumn3.FieldName = "Quantity";
            gridViewDecimalColumn3.HeaderText = "万粒数";
            gridViewDecimalColumn3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            gridViewDecimalColumn3.Name = "clQuantity";
            gridViewDecimalColumn3.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Memo";
            gridViewTextBoxColumn2.HeaderText = "备注";
            gridViewTextBoxColumn2.Name = "clMemo";
            gridViewTextBoxColumn2.Width = 100;
            this.dgItems.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            entityColumn1,
            entityColumn2,
            gridViewTextBoxColumn1,
            entityColumn3,
            entityColumn4,
            entityColumn5,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewTextBoxColumn2});
            this.dgItems.Name = "dgItems";
            this.dgItems.ShowGroupPanel = false;
            this.dgItems.Size = new System.Drawing.Size(765, 298);
            this.dgItems.TabIndex = 6;
            this.dgItems.Text = "radGridView1";
            // 
            // ProductOutstockView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ProductOutstockView";
            this.Size = new System.Drawing.Size(765, 482);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbInstockDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaskNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.CommandBarButton btDelete;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton btPrint;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Controls.EnumPicker tbTaskType;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadTextBox tbMemo;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadTextBox tbCreateTime;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox tbCreateBy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadDateTimePicker tbInstockDate;
        private Telerik.WinControls.UI.RadTextBox tbTaskNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadGridView dgItems;
        private mzerp.UI.Controls.EntityPicker tbCustomer;
        private Telerik.WinControls.UI.CommandBarButton btPick;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
    }
}
