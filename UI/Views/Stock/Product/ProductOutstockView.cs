﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using mzerp.UI.Controls;
using mzerp.Entities;
using mzerp.Reports;

namespace mzerp.UI
{
    public partial class ProductOutstockView : UserControl
    {

        private mzerp.Entities.ProductOutstockTask _entity;
        public mzerp.Entities.ProductOutstockTask Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public ProductOutstockView()
        {
            InitializeComponent();

            this.tbCustomer.PickerView = new BizPartnerPickerView(true,false);
            this.tbCustomer.DisplayMember = "Name";
            var clLevel = (EntityColumn)this.dgItems.Columns["clLevel"];
            //clLevel.PickerViewType = typeof(ProductLevelPickerView);
            clLevel.DisplayMember = "Name";
            var clMaterial = (EntityColumn)this.dgItems.Columns["clMaterial"];
            //clMaterial.PickerViewType = typeof(ProductMaterialPickerView);
            clMaterial.DisplayMember = "Name";
            var clSpec = (EntityColumn)this.dgItems.Columns["clSpec"];
            clSpec.DisplayMember = "Name";
            var clSpecValue1 = (EntityColumn)this.dgItems.Columns["clSpecValue1"];
            clSpecValue1.DisplayMember = "Name";
            var clSpecValue2 = (EntityColumn)this.dgItems.Columns["clSpecValue2"];
            clSpecValue2.DisplayMember = "Name";

            this.btSave.Click += (s, e) => { this.Save(); };
            this.btDelete.Click += (s, e) => { this.Delete(); };
            this.btPick.Click += (s, e) => { this.Pick(); };
            this.btPrint.Click += (s, e) => { this.Print(); };
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbTaskNo.Text = _entity.TaskNo;
            this.tbInstockDate.Value = _entity.OutstockDate;
            this.tbTaskType.SelectedValue = (int)_entity.TaskType;
            this.tbCustomer.SelectedEntity = _entity.Customer;
            this.tbCreateBy.Text = _entity.CreateBy.Base.Name;
            this.tbCreateTime.Text = _entity.CreateTime.ToString();
            this.tbMemo.Text = _entity.Memo;
            this.dgItems.DataSource = _entity.Items;

            this.CheckState();
        }

        private void FlushData()
        {
            _entity.OutstockDate = this.tbInstockDate.Value;
            _entity.Memo = this.tbMemo.Text;
            _entity.Customer = (BizPartner)this.tbCustomer.SelectedEntity;
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                _entity.TaskType = (Enums.ProductOutstockType)((int)this.tbTaskType.SelectedValue);
            }

            this.dgItems.EndEdit();
        }

        private void CheckState()
        {
            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.btPick.Enabled = false;
                this.tbTaskType.Enabled = false;
                this.dgItems.AllowDeleteRow = false;
            }
            else
            {
                this.btPick.Enabled = true;
                this.tbTaskType.Enabled = true;
                this.dgItems.AllowDeleteRow = true;
            }

            //除备注外所有列只读
            foreach (var cl in this.dgItems.Columns)
            {
                if (cl.Name != "clMemo")
                    cl.ReadOnly = true;
            }
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                if (_entity.Items.Count == 0)
                    throw new System.Exception("出库单至少需要一项产品");

                dps.Common.SysService.Invoke("mzerp", "ProductOutstockService", "Save", _entity.Instance);

                _entity.Instance.AcceptChanges();
                this.tbTaskNo.Text = _entity.TaskNo;
                this.CheckState();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private const int PageRows = 12;
        private void Print()
        {
            var view = new ReportViewerForm();
            view.Width = 900;
            view.Height = 700;

            var report = new ProductOutstockReport();
            report.ReportParameters["Company"].Value = Global.Company;
            report.TaskDataSource.DataSource = new List<ProductOutstockTask> { _entity };

            var items = new List<ProductOutstockItem>();
            items.AddRange(_entity.Items);
            //int rowsToAdd  = PageRows - (items.Count % PageRows);
            //int rowsToAdd = 2;
            //for (int i = 0; i < rowsToAdd; i++)
            //{ //items.Add(null);
            //    report.ItemsTable.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.55)));
            //}
            report.ItemsDataSource.DataSource = items;

            var reportSource = new Telerik.Reporting.InstanceReportSource();
            reportSource.ReportDocument = report;

            view.ReportViewer.ReportSource = reportSource;

            view.ShowDialog();

        }

        private void Pick()
        {
            var dlg = new ProductOutstockPickForm();
            dlg.Task = _entity;
            dlg.ShowDialog();

            //Todo:
            foreach (var row in dgItems.Rows)
            {
                row.InvalidateRow();
            }
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "ProductOutstockService", "Delete", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "删除出现异常：\r\n" + ex.Message, "删除错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
