﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mzerp.UI.Controls;
using Telerik.WinControls;
using mzerp.Entities;
using mzerp.Reports;
using Telerik.WinControls.UI;

namespace mzerp.UI
{
    public partial class ProductInstockView : UserControl
    {

        private mzerp.Entities.ProductInstockTask _entity;
        public mzerp.Entities.ProductInstockTask Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public ProductInstockView()
        {
            InitializeComponent();
            this.tbCustomer.PickerView = new BizPartnerPickerView(true,false);
            this.tbCustomer.DisplayMember = "Name";
            var clLevel = (EntityColumn)this.dgItems.Columns["clLevel"];
            clLevel.PickerView = new ProductLevelPickerView();
            clLevel.DisplayMember = "Name";
            var clMaterial = (EntityColumn)this.dgItems.Columns["clMaterial"];
            clMaterial.PickerView = new ProductMaterialPickerView();
            clMaterial.DisplayMember = "Name";
            var clSpec = (EntityColumn)this.dgItems.Columns["clSpec"];
            clSpec.PickerView = new ProductSpecPickerView();
            clSpec.DisplayMember = "Name";
            var clSpecValue1 = (EntityColumn)this.dgItems.Columns["clSpecValue1"];
            clSpecValue1.PickerView = new ProductSpecValue1PickerView();
            clSpecValue1.DisplayMember = "Name";
            var clSpecValue2 = (EntityColumn)this.dgItems.Columns["clSpecValue2"];
            clSpecValue2.PickerView = new ProductSpecValue2PickerView();
            clSpecValue2.DisplayMember = "Name";

            this.btSave.Click += (s, e) => { this.Save(); };
            this.btDelete.Click += (s, e) => { this.Delete(); };
            this.btPrint.Click += (s, e) => { this.Print(); };
            this.tbTaskType.SelectedIndexChanged += (s, e) => { tbCustomer.Enabled = tbTaskType.SelectedIndex == 1; };

            this.dgItems.DefaultValuesNeeded += OnNewItemDefaultValuesNeeded;
            this.dgItems.CellValueChanged += OnCellValueChanged;
        }

        private void OnCellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (e.Column.Name == "clSpec")
            {
                ProductSpec spec = (ProductSpec)e.Value;
                e.Row.Cells["clQuantityOfBox"].Value = spec.BoxQuantity;
                CalcQuantity(e.Row);
            }
            else if (e.Column.Name == "clBoxes" || e.Column.Name =="clChange")
            {
                CalcQuantity(e.Row);
            }
        }

        //重新计算总的万粒数
        private void CalcQuantity(GridViewRowInfo row)
        {
            int boxes = row.Cells["clBoxes"].Value == null ? 0 : (int)row.Cells["clBoxes"].Value;
            decimal boxQuantity = row.Cells["clQuantityOfBox"].Value == null ? 0m : (decimal)row.Cells["clQuantityOfBox"].Value;
            decimal change = row.Cells["clChange"].Value == null ? 0m : (decimal)row.Cells["clChange"].Value;
            row.Cells["clQuantity"].Value = boxes * boxQuantity + change;
        }

        private void OnNewItemDefaultValuesNeeded(object sender, Telerik.WinControls.UI.GridViewRowEventArgs e)
        {
            //在这里设置新行的默认值
            //e.Row.Cells["clBoxes"].Value = 0;
            //e.Row.Cells["clQuantityOfBox"].Value = 0m;
            //e.Row.Cells["clChange"].Value = 0m;
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbTaskNo.Text = _entity.TaskNo;
            this.tbInstockDate.Value = _entity.InstockDate;
            this.tbTaskType.SelectedValue = (int)_entity.TaskType;
            this.tbCreateBy.Text = _entity.CreateBy.Base.Name;
            this.tbCreateTime.Text = _entity.CreateTime.ToString();
            this.tbMemo.Text = _entity.Memo;
            this.dgItems.DataSource = _entity.Items;

            this.CheckState();
        }

        private void FlushData()
        {
            _entity.InstockDate = this.tbInstockDate.Value;
            _entity.Customer = (BizPartner)this.tbCustomer.SelectedEntity;
            _entity.Memo = this.tbMemo.Text;
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                _entity.TaskType = (Enums.ProductInstockType)((int)this.tbTaskType.SelectedValue);
            }

            this.dgItems.EndEdit();
        }

        private void CheckState()
        {
            //处理当前用的权限
            this.btNew.Enabled = Permissions.ProductInstock_Create.IsOwn;

            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.btSave.Enabled = Permissions.ProductInstock_Edit.IsOwn;
                this.btDelete.Enabled = Permissions.ProductInstock_Delete.IsOwn;

                this.tbTaskType.Enabled = false;
                this.dgItems.AllowAddNewRow = false;
                this.dgItems.AllowDeleteRow = false;
                this.dgItems.Columns["clMaterial"].ReadOnly = true;
                this.dgItems.Columns["clSpec"].ReadOnly = true;
                this.dgItems.Columns["clBatchNo"].ReadOnly = true;
                this.dgItems.Columns["clSpecValue1"].ReadOnly = true;
                this.dgItems.Columns["clSpecValue2"].ReadOnly = true;
                this.dgItems.Columns["clLevel"].ReadOnly = true;
                this.dgItems.Columns["clBoxes"].ReadOnly = true;
                this.dgItems.Columns["clChange"].ReadOnly = true;
            }
            else
            {
                this.btSave.Enabled = Permissions.ProductInstock_Create.IsOwn;
                this.btDelete.Enabled = false;

                this.tbTaskType.Enabled = true;
                this.dgItems.AllowAddNewRow = true;
                this.dgItems.AllowDeleteRow = true;
                this.dgItems.Columns["clMaterial"].ReadOnly = false;
                this.dgItems.Columns["clSpec"].ReadOnly = false;
                this.dgItems.Columns["clBatchNo"].ReadOnly = false;
                this.dgItems.Columns["clSpecValue1"].ReadOnly = false;
                this.dgItems.Columns["clSpecValue2"].ReadOnly = false;
                this.dgItems.Columns["clLevel"].ReadOnly = false;
                this.dgItems.Columns["clBoxes"].ReadOnly = false;
                this.dgItems.Columns["clChange"].ReadOnly = false;
            }
        }

        private void Save()
        {
            try
            {
                this.FlushData();

                if (_entity.Items.Count == 0)
                    throw new System.Exception("入库单至少需要一项产品");

                dps.Common.SysService.Invoke("mzerp", "ProductInstockService", "Save", _entity.Instance);

                _entity.Instance.AcceptChanges();
                this.tbTaskNo.Text = _entity.TaskNo;
                this.CheckState();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private const int PageRows = 12;
        private void Print()
        {
            var view = new ReportViewerForm();
            view.Width = 900;
            view.Height = 700;

            var report = new ProductInstockReport();
            report.ReportParameters["Company"].Value = Global.Company;
            report.TaskDataSource.DataSource = new List<ProductInstockTask> { _entity };

            var items = new List<ProductInstockTaskItem>();
            items.AddRange(_entity.Items);
            int rowsToAdd = PageRows - (items.Count % PageRows);
            for (int i = 0; i < rowsToAdd; i++)
            { items.Add(null); }
            report.ItemsDataSource.DataSource = items;

            var reportSource = new Telerik.Reporting.InstanceReportSource();
            reportSource.ReportDocument = report;

            view.ReportViewer.ReportSource = reportSource;

            view.ShowDialog();
        }

        private void Delete()
        {
            if (RadMessageBox.Show(this, "确认删除吗？", "确认信息", MessageBoxButtons.OKCancel, RadMessageIcon.Exclamation) != DialogResult.OK)
                return;

            try
            {
                dps.Common.SysService.Invoke("mzerp", "ProductInstockService", "Delete", _entity.Instance.ID);
                RadMessageBox.Show(this, "删除成功，将关闭当前视图", "操作成功", MessageBoxButtons.OK, RadMessageIcon.Info);
                MainForm.Instance.CloseCurrentView();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "删除出现异常：\r\n" + ex.Message, "删除错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
