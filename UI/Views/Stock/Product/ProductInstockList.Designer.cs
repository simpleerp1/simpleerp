﻿namespace mzerp.UI
{
    partial class ProductInstockList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            mzerp.UI.Controls.EnumColumn enumColumn1 = new mzerp.UI.Controls.EnumColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpEmploee = new Telerik.WinControls.UI.RadTextBox();
            this.qcEmploee = new Telerik.WinControls.UI.RadCheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.qpEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.qpStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.qcDate = new Telerik.WinControls.UI.RadCheckBox();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btOpen = new Telerik.WinControls.UI.CommandBarButton();
            this.btDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.sp1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.dgList = new mzerp.UI.Controls.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpEmploee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qcEmploee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qcDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpEmploee);
            this.radGroupBox1.Controls.Add(this.qcEmploee);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.qpEndDate);
            this.radGroupBox1.Controls.Add(this.qpStartDate);
            this.radGroupBox1.Controls.Add(this.qcDate);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(578, 51);
            this.radGroupBox1.TabIndex = 4;
            this.radGroupBox1.Text = "查询条件";
            // 
            // qpEmploee
            // 
            this.qpEmploee.Location = new System.Drawing.Point(448, 20);
            this.qpEmploee.Name = "qpEmploee";
            this.qpEmploee.Size = new System.Drawing.Size(113, 20);
            this.qpEmploee.TabIndex = 4;
            // 
            // qcEmploee
            // 
            this.qcEmploee.Location = new System.Drawing.Point(374, 21);
            this.qcEmploee.Name = "qcEmploee";
            this.qcEmploee.Size = new System.Drawing.Size(68, 18);
            this.qcEmploee.TabIndex = 1;
            this.qcEmploee.Text = "创建人：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(218, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "--";
            // 
            // qpEndDate
            // 
            this.qpEndDate.Location = new System.Drawing.Point(239, 20);
            this.qpEndDate.Name = "qpEndDate";
            this.qpEndDate.Size = new System.Drawing.Size(113, 20);
            this.qpEndDate.TabIndex = 2;
            this.qpEndDate.TabStop = false;
            this.qpEndDate.Text = "2013年11月15日";
            this.qpEndDate.Value = new System.DateTime(2013, 11, 15, 16, 40, 4, 479);
            // 
            // qpStartDate
            // 
            this.qpStartDate.Location = new System.Drawing.Point(99, 20);
            this.qpStartDate.Name = "qpStartDate";
            this.qpStartDate.Size = new System.Drawing.Size(113, 20);
            this.qpStartDate.TabIndex = 1;
            this.qpStartDate.TabStop = false;
            this.qpStartDate.Text = "2013年11月15日";
            this.qpStartDate.Value = new System.DateTime(2013, 11, 15, 16, 40, 4, 479);
            // 
            // qcDate
            // 
            this.qcDate.Location = new System.Drawing.Point(16, 22);
            this.qcDate.Name = "qcDate";
            this.qcDate.Size = new System.Drawing.Size(79, 18);
            this.qcDate.TabIndex = 0;
            this.qcDate.Text = "日期范围：";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btOpen,
            this.btDelete,
            this.sp1,
            this.btSearch});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Add16;
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btOpen
            // 
            this.btOpen.AccessibleDescription = "打开";
            this.btOpen.AccessibleName = "打开";
            this.btOpen.DisplayName = "commandBarButton1";
            this.btOpen.DrawText = true;
            this.btOpen.Image = global::mzerp.UI.Properties.Resources.Notepad16;
            this.btOpen.Name = "btOpen";
            this.btOpen.Text = "编辑";
            this.btOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btOpen.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btDelete
            // 
            this.btDelete.AccessibleDescription = "删除";
            this.btDelete.AccessibleName = "删除";
            this.btDelete.DisplayName = "commandBarButton2";
            this.btDelete.DrawText = true;
            this.btDelete.Image = global::mzerp.UI.Properties.Resources.Delete16;
            this.btDelete.Name = "btDelete";
            this.btDelete.Text = "删除";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sp1
            // 
            this.sp1.AccessibleDescription = "commandBarSeparator1";
            this.sp1.AccessibleName = "commandBarSeparator1";
            this.sp1.DisplayName = "commandBarSeparator1";
            this.sp1.Name = "sp1";
            this.sp1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.sp1.VisibleInOverflowMenu = false;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(578, 30);
            this.radCommandBar1.TabIndex = 2;
            // 
            // dgList
            // 
            gridViewTextBoxColumn1.FieldName = "TaskNo";
            gridViewTextBoxColumn1.HeaderText = "入库单号";
            gridViewTextBoxColumn1.Name = "clTaskNo";
            gridViewTextBoxColumn1.Width = 85;
            gridViewDateTimeColumn1.CustomFormat = "";
            gridViewDateTimeColumn1.FieldName = "InstockDate";
            gridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn1.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn1.HeaderText = "入库日期";
            gridViewDateTimeColumn1.Name = "clInstockDate";
            gridViewDateTimeColumn1.Width = 80;
            enumColumn1.EnumModelID = "mzerp.ProductInstockType";
            enumColumn1.FieldName = "TaskType";
            enumColumn1.HeaderText = "入库类型";
            enumColumn1.Name = "clTaskType";
            enumColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "CustomerName";
            gridViewTextBoxColumn2.HeaderText = "客户";
            gridViewTextBoxColumn2.Name = "clCustomer";
            gridViewTextBoxColumn2.Width = 180;
            gridViewTextBoxColumn3.FieldName = "CreateBy";
            gridViewTextBoxColumn3.HeaderText = "制单员";
            gridViewTextBoxColumn3.Name = "clCreateBy";
            gridViewTextBoxColumn3.Width = 80;
            gridViewDateTimeColumn2.FieldName = "CreateTime";
            gridViewDateTimeColumn2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn2.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn2.HeaderText = "制单日期";
            gridViewDateTimeColumn2.Name = "clCreateTime";
            gridViewDateTimeColumn2.Width = 80;
            gridViewTextBoxColumn4.FieldName = "Memo";
            gridViewTextBoxColumn4.HeaderText = "备注";
            gridViewTextBoxColumn4.Name = "clMemo";
            gridViewTextBoxColumn4.Width = 120;
            this.dgList.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewDateTimeColumn1,
            enumColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn4});
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Location = new System.Drawing.Point(0, 81);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(578, 363);
            this.dgList.TabIndex = 3;
            // 
            // ProductInstockList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ProductInstockList";
            this.Size = new System.Drawing.Size(578, 444);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpEmploee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qcEmploee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qcDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.DataGrid dgList;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btOpen;
        private Telerik.WinControls.UI.CommandBarButton btDelete;
        private Telerik.WinControls.UI.CommandBarSeparator sp1;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.RadTextBox qpEmploee;
        private Telerik.WinControls.UI.RadCheckBox qcEmploee;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadDateTimePicker qpEndDate;
        private Telerik.WinControls.UI.RadDateTimePicker qpStartDate;
        private Telerik.WinControls.UI.RadCheckBox qcDate;
    }
}
