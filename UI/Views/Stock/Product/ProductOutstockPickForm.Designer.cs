﻿namespace mzerp.UI
{
    partial class ProductOutstockPickForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MakeOutstockPickForm));
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.qpSpecValue2 = new Telerik.WinControls.UI.RadTextBox();
            this.qpSpecValue1 = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.qpBatchNo = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.qpSpec = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.qpMaterial = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.qpLevel = new mzerp.UI.Controls.EntityPicker();
            this.dgItems = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new Telerik.WinControls.UI.RadButton();
            this.btAdd = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpBatchNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Controls.Add(this.qpSpecValue2);
            this.radGroupBox1.Controls.Add(this.qpSpecValue1);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.qpBatchNo);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.qpSpec);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.qpMaterial);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.qpLevel);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "库存查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(782, 89);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "库存查询条件";
            // 
            // btSearch
            // 
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search;
            this.btSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btSearch.Location = new System.Drawing.Point(536, 25);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(93, 51);
            this.btSearch.TabIndex = 19;
            this.btSearch.Text = "查询";
            this.btSearch.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // qpSpecValue2
            // 
            this.qpSpecValue2.Location = new System.Drawing.Point(252, 54);
            this.qpSpecValue2.Name = "qpSpecValue2";
            this.qpSpecValue2.Size = new System.Drawing.Size(100, 20);
            this.qpSpecValue2.TabIndex = 13;
            // 
            // qpSpecValue1
            // 
            this.qpSpecValue1.Location = new System.Drawing.Point(85, 54);
            this.qpSpecValue1.Name = "qpSpecValue1";
            this.qpSpecValue1.Size = new System.Drawing.Size(100, 20);
            this.qpSpecValue1.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(367, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "级别：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "分规值：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "规值：";
            // 
            // qpBatchNo
            // 
            this.qpBatchNo.Location = new System.Drawing.Point(419, 25);
            this.qpBatchNo.Name = "qpBatchNo";
            this.qpBatchNo.Size = new System.Drawing.Size(100, 20);
            this.qpBatchNo.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(367, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "批号：";
            // 
            // qpSpec
            // 
            this.qpSpec.Location = new System.Drawing.Point(252, 25);
            this.qpSpec.Name = "qpSpec";
            this.qpSpec.Size = new System.Drawing.Size(100, 20);
            this.qpSpec.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(200, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "规格：";
            // 
            // qpMaterial
            // 
            this.qpMaterial.Location = new System.Drawing.Point(85, 25);
            this.qpMaterial.Name = "qpMaterial";
            this.qpMaterial.Size = new System.Drawing.Size(100, 20);
            this.qpMaterial.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "材料：";
            // 
            // qpLevel
            // 
            this.qpLevel.DisplayMember = null;
            this.qpLevel.Location = new System.Drawing.Point(419, 54);
            this.qpLevel.Name = "qpLevel";
            this.qpLevel.PickerView = null;
            this.qpLevel.SelectedEntity = null;
            this.qpLevel.Size = new System.Drawing.Size(100, 22);
            this.qpLevel.TabIndex = 9;
            this.qpLevel.TabStop = false;
            // 
            // dgItems
            // 
            this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItems.Location = new System.Drawing.Point(0, 89);
            // 
            // dgItems
            // 
            this.dgItems.MasterTemplate.AllowAddNewRow = false;
            this.dgItems.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn1.FieldName = "Material";
            gridViewTextBoxColumn1.HeaderText = "材料";
            gridViewTextBoxColumn1.Name = "clMaterial";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Spec";
            gridViewTextBoxColumn2.HeaderText = "规格";
            gridViewTextBoxColumn2.Name = "clSpec";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "BatchNo";
            gridViewTextBoxColumn3.HeaderText = "批号";
            gridViewTextBoxColumn3.Name = "clBatchNo";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "SpecValue1";
            gridViewTextBoxColumn4.HeaderText = "规值";
            gridViewTextBoxColumn4.Name = "clSpecValue1";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "SpecValue2";
            gridViewTextBoxColumn5.HeaderText = "分规值";
            gridViewTextBoxColumn5.Name = "clSpecValue2";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.FieldName = "Level";
            gridViewTextBoxColumn6.HeaderText = "级别";
            gridViewTextBoxColumn6.Name = "clLevel";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn7.FieldName = "Quantity";
            gridViewTextBoxColumn7.HeaderText = "库存万粒数";
            gridViewTextBoxColumn7.Name = "clQuantity";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 80;
            gridViewDecimalColumn1.DecimalPlaces = 0;
            gridViewDecimalColumn1.FieldName = "OutBoxes";
            gridViewDecimalColumn1.HeaderText = "箱数";
            gridViewDecimalColumn1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn1.Name = "clOutBoxes";
            gridViewDecimalColumn2.FieldName = "BoxQuantity";
            gridViewDecimalColumn2.HeaderText = "每箱万粒数";
            gridViewDecimalColumn2.IsVisible = false;
            gridViewDecimalColumn2.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn2.Name = "clBoxQuantity";
            gridViewDecimalColumn3.DecimalPlaces = 4;
            gridViewDecimalColumn3.FieldName = "OutChange";
            gridViewDecimalColumn3.HeaderText = "零头数";
            gridViewDecimalColumn3.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn3.Name = "clOutChange";
            gridViewDecimalColumn4.DecimalPlaces = 4;
            gridViewDecimalColumn4.FieldName = "OutQuantity";
            gridViewDecimalColumn4.HeaderText = "出库万粒数";
            gridViewDecimalColumn4.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn4.Name = "clOutQuantity";
            gridViewDecimalColumn4.ReadOnly = true;
            gridViewDecimalColumn4.Width = 80;
            this.dgItems.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewDecimalColumn4});
            this.dgItems.Name = "dgItems";
            this.dgItems.ShowGroupPanel = false;
            this.dgItems.Size = new System.Drawing.Size(782, 317);
            this.dgItems.TabIndex = 1;
            this.dgItems.Text = "radGridView1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Controls.Add(this.btAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 406);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 52);
            this.panel1.TabIndex = 2;
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Location = new System.Drawing.Point(660, 16);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(110, 24);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "关闭";
            // 
            // btAdd
            // 
            this.btAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btAdd.Location = new System.Drawing.Point(551, 16);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(103, 24);
            this.btAdd.TabIndex = 0;
            this.btAdd.Text = "添加至出库清单";
            // 
            // ProductOutstockPickForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 458);
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductOutstockPickForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "出库拣货";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpecValue1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpBatchNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGridView dgItems;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton btClose;
        private Telerik.WinControls.UI.RadButton btAdd;
        private Telerik.WinControls.UI.RadTextBox qpSpecValue2;
        private Telerik.WinControls.UI.RadTextBox qpSpecValue1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox qpBatchNo;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadTextBox qpSpec;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBox qpMaterial;
        private System.Windows.Forms.Label label1;
        private Controls.EntityPicker qpLevel;
        private Telerik.WinControls.UI.RadButton btSearch;
    }
}
