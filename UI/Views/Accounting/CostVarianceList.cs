﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class CostVarianceList : UserControl
    {
        public CostVarianceList()
        {
            InitializeComponent();

            this.qpWarehouse.PickerView = new WarehousePickerView();
            this.qpWarehouse.DisplayMember = "Name";
            this.dgList.QueryMethod = "mzerp.CostVarianceService.Query";
            this.dgList.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += OnLoad;
            this.btSearch.Click += (s, e) => { this.dgList.LoadData(); };
            this.btNew.Click += (s, e) => { this.OnCreate(); };
            this.btOpen.Click += (s, e) => { this.OnOpen(); };
            this.dgList.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
        }

        private void OnLoad(object sender, EventArgs e)
        {
            //处理当前用的权限
            //this.btNew.Enabled = Permissions.ProductInstock_Create.IsOwn;
            //this.btOpen.Enabled = Permissions.ProductInstock_Edit.IsOwn;
            //this.btDelete.Enabled = Permissions.ProductInstock_Delete.IsOwn;

            this.dgList.LoadData();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[4];
            if (qcDate.Checked)
            {
                args[0] = qpStartDate.Value;
                args[1] = qpEndDate.Value;
            }

            if (qcWharehouse.Checked)
                args[2] = qpWarehouse.SelectedEntityID;
            if (qcUnProcessed.Checked && qcHasProcessed.Checked)
                args[3] = (byte)2;
            else
            {
                if (qcUnProcessed.Checked)
                    args[3] = (byte)0;
                else
                    args[3] = (byte)1;
            }

            return args;
        }

        #region Event handlers
        private void OnCreate()
        {
            //var view = new MakeOutstockView();
            //view.Create();
            //MainForm.Instance.ShowView(view, "成本差异");
        }

        private void OnOpen()
        {
            var id = this.dgList.SelectedEntityID;
            if (id == Guid.Empty)
                return;

            var entity = new CostVarianceTask(dps.Common.Data.Entity.Retrieve(CostVarianceTask.EntityModelID, id));

            var view = new CostVarianceView();
            view.Entity = entity;
            MainForm.Instance.ShowView(view, "成本差异");
        }
        #endregion
    }
}
