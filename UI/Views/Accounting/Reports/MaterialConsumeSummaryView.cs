﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.Charting;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class MaterialConsumeSummaryView : UserControl
    {

        public MaterialConsumeSummaryView()
        {
            InitializeComponent();

            this.btSearch.Click += (s, e) => { this.Search(); };
            this.radChartView1.ShowSmartLabels = true;
            this.radChartView1.LabelFormatting += OnPieLabelFormatting;
        }

        private void OnPieLabelFormatting(object sender, ChartViewLabelFormattingEventArgs e)
        {
            PieDataPoint p = e.LabelElement.DataPoint as PieDataPoint;
            if (p != null)
            {
                p.Label = string.Format("{0} {1:N1}%", p.Name, p.Percent);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.qpYear.Value = DateTime.Now.Year;
            this.qpMonth.Value = DateTime.Now.Month;

            this.Search();
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    int year = (int)this.qpYear.Value;
                    int month = (int)this.qpMonth.Value;
                    DateTime startDate = new DateTime(year, month, 1);
                    DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

                    var result = dps.Common.SysService.Invoke("mzerp", "MaterialStockQueryService", "GetMaterialConsumeSummary",
                                       startDate, endDate) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");

                    PieSeries pieSeries = new PieSeries();
                    pieSeries.ShowLabels = true;
                    //pieSeries.LabelFormat = "{0:P2}";
                    //pieSeries.LabelMode = PieLabelModes.Horizontal;
                    pieSeries.DrawLinesToLabels = true;
                    pieSeries.SyncLinesToLabelsColor = true;

                    string costCenter = null;
                    PieDataPoint point = null;
                    for (int i = 0; i < result.Rows.Count; i++)
                    {
                        costCenter = (string)result.Rows[i]["CostCenter"];
                        point = new PieDataPoint(0d, costCenter);
                        point.Label = costCenter; //string.Format("{0} {1:P2}", spec, point.Percent);
                        point.Value = (double)((decimal)result.Rows[i]["MakeOut"] + (decimal)result.Rows[i]["OtherOut"] - (decimal)result.Rows[i]["MakeReturn"] - (decimal)result.Rows[i]["OtherIn"]);
                        pieSeries.DataPoints.Add(point);
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        //显示汇总报表
                        this.gvList.DataSource = result;
                        //显示比例图
                        this.radChartView1.Series.Clear();
                        this.radChartView1.Series.Add(pieSeries);
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

    }
}
