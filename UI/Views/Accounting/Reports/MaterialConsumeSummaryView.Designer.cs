﻿namespace mzerp.UI
{
    partial class MaterialConsumeSummaryView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSearch = new Telerik.WinControls.UI.CommandBarButton();
            this.btExport = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpMonth = new Telerik.WinControls.UI.RadSpinEditor();
            this.label3 = new System.Windows.Forms.Label();
            this.qpYear = new Telerik.WinControls.UI.RadSpinEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.gvList = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvList.MasterTemplate)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(771, 30);
            this.radCommandBar1.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSearch,
            this.btExport});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btSearch
            // 
            this.btSearch.AccessibleDescription = "commandBarButton1";
            this.btSearch.AccessibleName = "commandBarButton1";
            this.btSearch.DisplayName = "commandBarButton1";
            this.btSearch.DrawText = true;
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search16;
            this.btSearch.Name = "btSearch";
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btExport
            // 
            this.btExport.AccessibleDescription = "导出";
            this.btExport.AccessibleName = "导出";
            this.btExport.DisplayName = "commandBarButton1";
            this.btExport.DrawText = true;
            this.btExport.Image = global::mzerp.UI.Properties.Resources.ExportExcel16;
            this.btExport.Name = "btExport";
            this.btExport.Text = "导出";
            this.btExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btExport.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpMonth);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.qpYear);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(771, 52);
            this.radGroupBox1.TabIndex = 6;
            this.radGroupBox1.Text = "查询条件";
            // 
            // qpMonth
            // 
            this.qpMonth.Location = new System.Drawing.Point(146, 21);
            this.qpMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.qpMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.qpMonth.Name = "qpMonth";
            this.qpMonth.Size = new System.Drawing.Size(35, 20);
            this.qpMonth.TabIndex = 6;
            this.qpMonth.TabStop = false;
            this.qpMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "月：";
            // 
            // qpYear
            // 
            this.qpYear.Location = new System.Drawing.Point(46, 21);
            this.qpYear.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.qpYear.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qpYear.Name = "qpYear";
            this.qpYear.Size = new System.Drawing.Size(55, 20);
            this.qpYear.TabIndex = 5;
            this.qpYear.TabStop = false;
            this.qpYear.Value = new decimal(new int[] {
            2014,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "年：";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 82);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(771, 448);
            this.radPageView1.TabIndex = 8;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.gvList);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(64F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(750, 400);
            this.radPageViewPage1.Text = "汇总报表";
            // 
            // gvList
            // 
            this.gvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvList.Location = new System.Drawing.Point(0, 0);
            // 
            // gvList
            // 
            gridViewTextBoxColumn1.FieldName = "CostCenter";
            gridViewTextBoxColumn1.HeaderText = "成本中心";
            gridViewTextBoxColumn1.Name = "clCostCenter";
            gridViewTextBoxColumn1.Width = 180;
            gridViewTextBoxColumn2.FieldName = "MakeOut";
            gridViewTextBoxColumn2.HeaderText = "领料出库";
            gridViewTextBoxColumn2.Name = "clMakeOut";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "MakeReturn";
            gridViewTextBoxColumn3.HeaderText = "退料入库";
            gridViewTextBoxColumn3.Name = "clMakeReturn";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "OtherOut";
            gridViewTextBoxColumn4.HeaderText = "其他出库";
            gridViewTextBoxColumn4.Name = "clOtherOut";
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "OtherIn";
            gridViewTextBoxColumn5.HeaderText = "其他入库";
            gridViewTextBoxColumn5.Name = "clOtherIn";
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.Expression = "clMakeOut -clMakeReturn +clOtherOut -clOtherIn ";
            gridViewTextBoxColumn6.HeaderText = "累计消耗";
            gridViewTextBoxColumn6.Name = "clTotal";
            gridViewTextBoxColumn6.Width = 100;
            this.gvList.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.gvList.Name = "gvList";
            this.gvList.ReadOnly = true;
            this.gvList.ShowGroupPanel = false;
            this.gvList.Size = new System.Drawing.Size(750, 400);
            this.gvList.TabIndex = 6;
            this.gvList.Text = "radGridView1";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.radChartView1);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(123F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(750, 400);
            this.radPageViewPage2.Text = "各成本中心消耗比例";
            // 
            // radChartView1
            // 
            this.radChartView1.AreaType = Telerik.WinControls.UI.ChartAreaType.Pie;
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radChartView1.Location = new System.Drawing.Point(0, 0);
            this.radChartView1.Name = "radChartView1";
            this.radChartView1.ShowGrid = false;
            this.radChartView1.Size = new System.Drawing.Size(750, 400);
            this.radChartView1.TabIndex = 0;
            this.radChartView1.Text = "radChartView1";
            // 
            // MaterialConsumeSummaryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "MaterialConsumeSummaryView";
            this.Size = new System.Drawing.Size(771, 530);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvList.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvList)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btSearch;
        private Telerik.WinControls.UI.CommandBarButton btExport;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadGridView gvList;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadSpinEditor qpMonth;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadSpinEditor qpYear;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadChartView radChartView1;
    }
}
