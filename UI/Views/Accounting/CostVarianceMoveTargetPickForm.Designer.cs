﻿namespace mzerp.UI
{
    partial class CostVarianceMoveTargetPickForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            mzerp.UI.Controls.EnumColumn enumColumn1 = new mzerp.UI.Controls.EnumColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.qpMaterialCode = new Telerik.WinControls.UI.RadTextBox();
            this.qpLeft = new Telerik.WinControls.UI.RadTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.qpStocks = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.qpMeasureUnit = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.qpSpec = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.qpName = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.qpWarehouse = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btClose = new Telerik.WinControls.UI.RadButton();
            this.btAdd = new Telerik.WinControls.UI.RadButton();
            this.dgItems = new Telerik.WinControls.UI.RadGridView();
            this.qcAllLeft = new Telerik.WinControls.UI.RadCheckBox();
            this.qpCostCenter = new mzerp.UI.Controls.EntityPicker();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpStocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMeasureUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qcAllLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpCostCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.qpMaterialCode);
            this.radGroupBox1.Controls.Add(this.qpLeft);
            this.radGroupBox1.Controls.Add(this.label7);
            this.radGroupBox1.Controls.Add(this.qpStocks);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.qpMeasureUnit);
            this.radGroupBox1.Controls.Add(this.label6);
            this.radGroupBox1.Controls.Add(this.qpSpec);
            this.radGroupBox1.Controls.Add(this.label5);
            this.radGroupBox1.Controls.Add(this.qpName);
            this.radGroupBox1.Controls.Add(this.label4);
            this.radGroupBox1.Controls.Add(this.qpWarehouse);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(672, 86);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "查询条件";
            // 
            // qpMaterialCode
            // 
            this.qpMaterialCode.Location = new System.Drawing.Point(61, 25);
            this.qpMaterialCode.Name = "qpMaterialCode";
            this.qpMaterialCode.ReadOnly = true;
            this.qpMaterialCode.Size = new System.Drawing.Size(100, 20);
            this.qpMaterialCode.TabIndex = 30;
            // 
            // qpLeft
            // 
            this.qpLeft.Location = new System.Drawing.Point(376, 51);
            this.qpLeft.Name = "qpLeft";
            this.qpLeft.ReadOnly = true;
            this.qpLeft.Size = new System.Drawing.Size(100, 20);
            this.qpLeft.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(328, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "待摊：";
            // 
            // qpStocks
            // 
            this.qpStocks.Location = new System.Drawing.Point(222, 51);
            this.qpStocks.Name = "qpStocks";
            this.qpStocks.ReadOnly = true;
            this.qpStocks.Size = new System.Drawing.Size(100, 20);
            this.qpStocks.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(180, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "库存：";
            // 
            // qpMeasureUnit
            // 
            this.qpMeasureUnit.Location = new System.Drawing.Point(525, 25);
            this.qpMeasureUnit.Name = "qpMeasureUnit";
            this.qpMeasureUnit.ReadOnly = true;
            this.qpMeasureUnit.Size = new System.Drawing.Size(52, 20);
            this.qpMeasureUnit.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(484, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "单位：";
            // 
            // qpSpec
            // 
            this.qpSpec.Location = new System.Drawing.Point(376, 25);
            this.qpSpec.Name = "qpSpec";
            this.qpSpec.ReadOnly = true;
            this.qpSpec.Size = new System.Drawing.Size(100, 20);
            this.qpSpec.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(328, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "规格：";
            // 
            // qpName
            // 
            this.qpName.Location = new System.Drawing.Point(222, 25);
            this.qpName.Name = "qpName";
            this.qpName.ReadOnly = true;
            this.qpName.Size = new System.Drawing.Size(100, 20);
            this.qpName.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(180, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "名称：";
            // 
            // qpWarehouse
            // 
            this.qpWarehouse.Location = new System.Drawing.Point(61, 51);
            this.qpWarehouse.Name = "qpWarehouse";
            this.qpWarehouse.ReadOnly = true;
            this.qpWarehouse.Size = new System.Drawing.Size(100, 20);
            this.qpWarehouse.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "仓库：";
            // 
            // btSearch
            // 
            this.btSearch.Image = global::mzerp.UI.Properties.Resources.Search;
            this.btSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btSearch.Location = new System.Drawing.Point(582, 25);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(81, 46);
            this.btSearch.TabIndex = 19;
            this.btSearch.Text = "查询";
            this.btSearch.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "物料：";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.qpCostCenter);
            this.panel1.Controls.Add(this.qcAllLeft);
            this.panel1.Controls.Add(this.btClose);
            this.panel1.Controls.Add(this.btAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 411);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(672, 52);
            this.panel1.TabIndex = 2;
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Location = new System.Drawing.Point(550, 16);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(110, 24);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "关闭";
            // 
            // btAdd
            // 
            this.btAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btAdd.Location = new System.Drawing.Point(441, 16);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(103, 24);
            this.btAdd.TabIndex = 0;
            this.btAdd.Text = "分摊成本差异项";
            // 
            // dgItems
            // 
            this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgItems.Location = new System.Drawing.Point(0, 86);
            // 
            // dgItems
            // 
            this.dgItems.MasterTemplate.AllowAddNewRow = false;
            this.dgItems.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn1.FieldName = "TaskNo";
            gridViewTextBoxColumn1.HeaderText = "出库单号";
            gridViewTextBoxColumn1.Name = "clTaskNo";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.FieldName = "InstockDate";
            gridViewTextBoxColumn2.FormatString = "{0:yyyy-MM-dd}";
            gridViewTextBoxColumn2.HeaderText = "出库日期";
            gridViewTextBoxColumn2.Name = "clInstockDate";
            gridViewTextBoxColumn2.Width = 100;
            enumColumn1.EnumModelID = "mzerp.MaterialOutstockType";
            enumColumn1.HeaderText = "出库类型";
            enumColumn1.Name = "column2";
            enumColumn1.Width = 80;
            gridViewTextBoxColumn3.HeaderText = "成本中心";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.FieldName = "AvaliableQuantity";
            gridViewTextBoxColumn4.HeaderText = "出库数量";
            gridViewTextBoxColumn4.Name = "clAvaliableQuantity";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn4.Width = 100;
            gridViewDecimalColumn1.DecimalPlaces = 4;
            gridViewDecimalColumn1.FieldName = "OutQuantity";
            gridViewDecimalColumn1.HeaderText = "分摊数量";
            gridViewDecimalColumn1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn1.Name = "clOutQuantity";
            gridViewDecimalColumn1.Width = 100;
            gridViewDecimalColumn2.HeaderText = "分摊差异额";
            gridViewDecimalColumn2.Name = "column1";
            gridViewDecimalColumn2.Width = 80;
            this.dgItems.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            enumColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2});
            this.dgItems.Name = "dgItems";
            this.dgItems.ShowGroupPanel = false;
            this.dgItems.Size = new System.Drawing.Size(672, 325);
            this.dgItems.TabIndex = 3;
            this.dgItems.Text = "radGridView1";
            // 
            // qcAllLeft
            // 
            this.qcAllLeft.Location = new System.Drawing.Point(16, 16);
            this.qcAllLeft.Name = "qcAllLeft";
            this.qcAllLeft.Size = new System.Drawing.Size(186, 18);
            this.qcAllLeft.TabIndex = 2;
            this.qcAllLeft.Text = "不足部分分摊至指定成本中心：";
            // 
            // qpCostCenter
            // 
            this.qpCostCenter.DisplayMember = null;
            this.qpCostCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qpCostCenter.Location = new System.Drawing.Point(208, 16);
            this.qpCostCenter.Name = "qpCostCenter";
            this.qpCostCenter.PickerView = null;
            this.qpCostCenter.SelectedEntity = null;
            this.qpCostCenter.Size = new System.Drawing.Size(142, 20);
            this.qpCostCenter.TabIndex = 23;
            this.qpCostCenter.TabStop = false;
            this.qpCostCenter.ThemeName = "ControlDefault";
            // 
            // CostVarianceMoveTargetPickForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 463);
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CostVarianceMoveTargetPickForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "成本差异移动对象选择";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qpMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpStocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpMeasureUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qcAllLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpCostCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton btClose;
        private Telerik.WinControls.UI.RadButton btAdd;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton btSearch;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBox qpWarehouse;
        private Telerik.WinControls.UI.RadGridView dgItems;
        private Telerik.WinControls.UI.RadTextBox qpMeasureUnit;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadTextBox qpSpec;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadTextBox qpName;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox qpLeft;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadTextBox qpStocks;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadTextBox qpMaterialCode;
        private Telerik.WinControls.UI.RadCheckBox qcAllLeft;
        private Controls.EntityPicker qpCostCenter;
    }
}
