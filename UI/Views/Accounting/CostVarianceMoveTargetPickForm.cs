﻿using dps.Common;
using mzerp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class CostVarianceMoveTargetPickForm : Telerik.WinControls.UI.RadForm
    {

        private decimal _moveToStockQuantity = 0m; //摊到存货的数量
        private decimal _leftQuantity = 0m;//除摊到存货外的剩余待摊数量

        private mzerp.Entities.CostVarianceItem _cvItem;
        public mzerp.Entities.CostVarianceItem CVItem
        {
            get { return _cvItem; }
            set
            {
                _cvItem = value;
                this.qpWarehouse.Text = _cvItem.CostVarianceTask.Warehouse.Name;
                this.qpMaterialCode.Text = _cvItem.Material.Code;
                this.qpName.Text = _cvItem.Material.Name;
                this.qpSpec.Text = _cvItem.Material.Spec;
                this.qpMeasureUnit.Text = _cvItem.Material.MeasureUnit;
            }
        }

        public CostVarianceMoveTargetPickForm()
        {
            InitializeComponent();

            this.btSearch.Click += (s, e) => { this.Search(); };
            this.btAdd.Click += (s, e) => { this.AddToList(); };
            this.btClose.Click += (s, e) => { this.Close(); };
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[3];
            args[0] = 50;
            args[1] = _cvItem.MaterialID;
            args[2] = _cvItem.CostVarianceTask.WarehouseID;
            return args;
        }

        private void Search()
        {
            var args = GetQueryArgs();
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    //先查询当前库存数
                    var currentStock = (decimal)SysService.Invoke("mzerp", "MaterialStockQueryService", "GetMaterialCurrentStock",
                        _cvItem.MaterialID, _cvItem.CostVarianceTask.WarehouseID);

                    dps.Common.Data.DataTable dt = null;
                    if (currentStock < _cvItem.Quantity) //当前库存不足
                    {
                        //再查询出库清单
                        dt = SysService.Invoke("mzerp", "MaterialOutstockService", "QueryForCVMove", args) as dps.Common.Data.DataTable;
                        if (dt == null)
                            throw new System.Exception("查询结果不正确");
                        dt.Columns.Add("OutQuantity", typeof(Decimal));
                        _leftQuantity = _cvItem.Quantity - currentStock;
                        _moveToStockQuantity = currentStock;
                    }
                    else
                        _moveToStockQuantity = _cvItem.Quantity;

                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.qpStocks.Text = currentStock.ToString();
                        this.qpLeft.Text = _leftQuantity.ToString();
                        this.dgItems.DataSource = dt;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void AddToList()
        {
            if (_moveToStockQuantity > 0)
            {
                var move = new CostVarianceMove();
                move.MoveTarget = mzerp.Enums.CostVarianceMoveTarget.Stock;
                move.TargetID = _cvItem.CostVarianceTask.WarehouseID;
                move.Quantity = _moveToStockQuantity;
                move.Value = (_cvItem.Variance / _cvItem.Quantity) * _moveToStockQuantity;
                _cvItem.MoveTargets.Add(move);
            }

            if (_leftQuantity > 0)
            {
                throw new NotImplementedException();
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
