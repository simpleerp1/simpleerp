﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using mzerp.Entities;

namespace mzerp.UI
{
    public partial class CostVarianceView : UserControl
    {
        private mzerp.Entities.CostVarianceTask _entity;
        public mzerp.Entities.CostVarianceTask Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        public CostVarianceView()
        {
            InitializeComponent();

            this.tbWarehouse.PickerView = new WarehousePickerView();
            this.tbWarehouse.DisplayMember = "Name";

            //this.btSave.Click += (s, e) => { this.Save(); };
            this.btPick.Click += (s, e) => { this.Pick(); };
            this.btProcess.Click += (s, e) => { this.Process(); };
            //this.btPrint.Click += (s, e) => { this.Print(); };
            //this.dgItems.DefaultValuesNeeded += OnNewItemDefaultValuesNeeded;
            //this.dgItems.CellValueChanged += OnCellValueChanged;
        }

        public void Create()
        {
            //var task = new mzerp.Entities.MaterialOutstockTask();
            //task.OutstockDate = task.CreateTime = DateTime.Now;
            //task.CreateBy = SystemService.CurrentEmploee;
            //task.OutstockType = Enums.MaterialOutstockType.Make;
            //this.Entity = task;
            throw new NotImplementedException();
        }

        private void BindData()
        {
            if (_entity == null)
                return;

            this.tbTaskNo.Text = _entity.TaskNo;
            this.tbCreateDate.Value = _entity.CreateTime;
            if (_entity.Instance["WarehouseID"].HasValue)
                this.tbWarehouse.SelectedEntity = _entity.Warehouse;
            this.tbCreateBy.Text = _entity.CreateBy.Base.Name;
            this.tbCreateTime.Text = _entity.CreateTime.ToString();
            //this.tbMemo.Text = _entity.Memo;
            this.tbHasProcessed.Checked = _entity.ProcessByID.HasValue;
            this.dgItems.DataSource = _entity.Items;

            this.CheckState();
        }

        private void FlushData()
        {
            //_entity.Warehouse = (Warehouse)this.tbWarehouse.SelectedEntity;
            //_entity.OutstockDate = this.tbOutstockDate.Value;
            //_entity.CostCenter = (CostCenter)this.tbCostCenter.SelectedEntity;
            //_entity.TakeBy = (sys.Entities.Emploee)this.tbTakeBy.SelectedEntity;
            //_entity.RefTaskNo = this.tbRefTaskNo.Text;
            //_entity.Memo = this.tbMemo.Text;

            //this.dgItems.EndEdit();
        }

        private void CheckState()
        {
            //处理当前用户的权限
            this.btNew.Enabled = Permissions.PurchaseInstock_Edit.IsOwn;

            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.btSave.Enabled = false; //Permissions.PurchaseInstock_Edit.IsOwn;
                this.tbWarehouse.Enabled = false;

                this.dgItems.AllowAddNewRow = false;
                this.dgItems.AllowDeleteRow = false;
            }
            else
            {
                //this.btSave.Enabled = Permissions.PurchaseInstock_Edit.IsOwn;

                //this.tbWarehouse.Enabled = false;
                //this.tbCostCenter.Enabled = false;
                //this.tbOutstockDate.Enabled = false;

                //this.dgItems.AllowAddNewRow = false;
                //this.dgItems.AllowDeleteRow = false;
                throw new NotImplementedException();
            }
        }

        private void Pick()
        {
            if (this.dgItems.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请先选择差异物料项", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var dlg = new CostVarianceMoveTargetPickForm();
            dlg.CVItem = (CostVarianceItem)this.dgItems.SelectedRows[0].DataBoundItem;
            dlg.ShowDialog();

            //foreach (var row in dgItems.Rows)
            //{
            //    row.InvalidateRow();
            //}
        }

        private void Process()
        {
            try
            {
                if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                    throw new System.Exception("请先保存单据后再处理成本差异");

                if (RadMessageBox.Show(this, "处理成本差异后将不允许删除及更改信息，确认处理吗?", "确认信息",
                    MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                    return;

                dps.Common.SysService.Invoke("mzerp", "CostVarianceService", "ProcessVariance", _entity.Instance);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "处理出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Save()
        {
            //try
            //{
            //    this.FlushData();
            //    if (!_entity.Instance["WarehouseID"].HasValue)
            //        throw new System.Exception("必须指定入库仓库");
            //    if (_entity.Items.Count == 0)
            //        throw new System.Exception("出库单至少需要一项产品");

            //    if (RadMessageBox.Show(this, "保存后将不允许删除及更改出库信息，确认保存吗?", "确认信息",
            //        MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
            //        return;

            //    var r = (dps.Common.Data.Entity)dps.Common.SysService.Invoke("mzerp", "MaterialOutstockService", "SaveNew", _entity.Instance);
            //    this.Entity = new MaterialOutstockTask(r);
            //}
            //catch (Exception ex)
            //{
            //    RadMessageBox.Show(this, "保存出现异常：\r\n" + ex.Message, "保存错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            //}
        }

        private const int PageRows = 12;
        private void Print()
        {
            //var view = new ReportViewerForm();
            //view.Width = 900;
            //view.Height = 700;

            //var report = new ProductInstockReport();
            //report.ReportParameters["Company"].Value = Global.Company;
            //report.TaskDataSource.DataSource = new List<ProductInstockTask> { _entity };

            //var items = new List<ProductInstockTaskItem>();
            //items.AddRange(_entity.Items);
            //int rowsToAdd = PageRows - (items.Count % PageRows);
            //for (int i = 0; i < rowsToAdd; i++)
            //{ items.Add(null); }
            //report.ItemsDataSource.DataSource = items;

            //var reportSource = new Telerik.Reporting.InstanceReportSource();
            //reportSource.ReportDocument = report;

            //view.ReportViewer.ReportSource = reportSource;

            //view.ShowDialog();
        }
    }
}
