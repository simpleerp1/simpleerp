﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    interface IOrgUnitView
    {

        void BindData(OrgUnitInfo ouInfo);

        void FlushData();

    }
}
