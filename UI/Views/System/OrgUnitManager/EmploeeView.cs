﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using dps.Common;

namespace mzerp.UI
{
    public partial class EmploeeView : UserControl, IOrgUnitView
    {

        private OrgUnitInfo _orgUnitInfo;

        public EmploeeView()
        {
            InitializeComponent();

            this.btChangeAccount.Click += ChangeAccount;
            this.btChangePassword.Click += ChangePassword;
        }

        private void ChangeAccount(object sender, EventArgs e)
        {
            if (_orgUnitInfo.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                RadMessageBox.Show(this, "请先保存当前组织结构后再更改员工帐号", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var empInfo = (EmploeeInfo)_orgUnitInfo.Base;
            if (string.IsNullOrEmpty(tbAccount.Text)) //新帐号为空表示删除员工的帐号
            {
                if (!string.IsNullOrEmpty(empInfo.Account))
                {
                    try
                    {
                        SysService.Invoke(SysGloble.SysString, SysGloble.Service_ManagementService, "RemoveUser", empInfo.Instance);
                        empInfo.Account = null;
                    }
                    catch (Exception ex)
                    {
                        RadMessageBox.Show(this, ex.Message, "删除帐号错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(empInfo.Account)) //新建帐号
                {
                    try
                    {
                        SysService.Invoke(SysGloble.SysString, SysGloble.Service_ManagementService, "AddUser",
                            empInfo.Instance, SysGloble.SysString, this.tbAccount.Text, "111111");
                        empInfo.Account = string.Format("sys\\{0}", this.tbAccount.Text);
                    }
                    catch (Exception ex)
                    {
                        RadMessageBox.Show(this, ex.Message, "新建帐号错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
                else //修改旧帐号
                {
                    try
                    {
                        SysService.Invoke(SysGloble.SysString, SysGloble.Service_ManagementService, "ChangeUserAccount",
                            empInfo.Instance, empInfo.Account, string.Format("sys\\{0}", this.tbAccount.Text));
                        empInfo.Account = string.Format("sys\\{0}", this.tbAccount.Text);
                    }
                    catch (Exception ex)
                    {
                        RadMessageBox.Show(this, ex.Message, "修改帐号错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        private void ChangePassword(object sender, EventArgs e)
        {
            if (_orgUnitInfo.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                RadMessageBox.Show(this, "请先保存当前组织结构后再更改密码", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            var empInfo = (EmploeeInfo)_orgUnitInfo.Base;
            if (string.IsNullOrEmpty(empInfo.Account))
            {
                RadMessageBox.Show(this, "请先添加员工帐号后再更改密码", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(this.tbPassword.Text))
            {
                RadMessageBox.Show(this, "密码不能为空", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            try
            {
                SysService.Invoke(SysGloble.SysString, SysGloble.Service_ManagementService, "ChangeUserPassword",
                    empInfo.Account, null, this.tbPassword.Text);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, ex.Message, "修改密码错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void ClearBinding()
        {
            _orgUnitInfo = null;
            this.tbName.Text = string.Empty;
        }

        void IOrgUnitView.BindData(OrgUnitInfo ouInfo)
        {
            _orgUnitInfo = ouInfo;

            if (ouInfo == null)
            {
                ClearBinding();
                return;
            }

            var empInfo = (EmploeeInfo)ouInfo.Base;
            this.tbName.Text = ouInfo.Name;
            if (string.IsNullOrEmpty(empInfo.Account))
                this.tbAccount.Text = null;
            else
                this.tbAccount.Text = empInfo.Account.Split('\\')[1];
        }

        void IOrgUnitView.FlushData()
        {
            if (_orgUnitInfo == null)
                return;

            _orgUnitInfo.Name = this.tbName.Text;
            //var bizUnitInfo = (BusinessUnitInfo)_orgUnitInfo.Base;
        }


    }
}
