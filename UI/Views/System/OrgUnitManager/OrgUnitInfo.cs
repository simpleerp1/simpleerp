﻿using dps.Common.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    class OrgUnitInfo : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "sys.OrgUnit";

        #region ====Properties====
        public string Name
        {
            get { return (string)Instance["Name"].Value; }
            set { Instance["Name"].Value = value; }
        }

        private IList<OrgUnitInfo> _items;
        public IList<OrgUnitInfo> Items
        {
            get
            {
                if (_items == null)
                    _items = new dps.Data.Mapper.EntityList<OrgUnitInfo>(Instance["SubItems"].EntityListValue);
                return _items;
            }
        }

        public string BaseType
        { get { return (string)Instance["BaseType"].StringValue; } }

        public Guid BaseID
        { get { return (Guid)Instance["BaseID"].GuidValue; } }

        private dps.Data.Mapper.EntityBase _base;
        public dps.Data.Mapper.EntityBase Base
        {
            get
            {
                if (_base == null)
                {
                    Entity value = Instance["Base"].EntityValue;
                    if (value == null)
                        return null;

                    switch (value.ModelID)
                    {
                        case "sys.BusinessUnit":
                            _base = new BusinessUnitInfo(value);
                            break;
                        case "sys.WorkGroup":
                            _base = new WorkGroupInfo(value);
                            break;
                        case "sys.Emploee":
                            _base = new EmploeeInfo(value);
                            break;
                        default:
                            throw new System.Exception("Invalid Entity Model ID");
                    }
                }
                return _base;
            }
            set
            {
                _base = value;
                if (value == null)
                    Instance["Base"].EntityValue = null;
                else
                    Instance["Base"].EntityValue = _base.Instance;
            }
        }

        private OrgUnitInfo _parent;
        public OrgUnitInfo Parent
        {
            get
            {
                if (_parent == null)
                {
                    Entity value = Instance["Parent"].EntityValue;
                    if (value == null)
                        return null;
                    _parent = new OrgUnitInfo(value);
                }

                return _parent;
            }
            set
            {
                _parent = value;
                if (value == null)
                    Instance["Parent"].EntityValue = null;
                else
                    Instance["Parent"].EntityValue = _parent.Instance;
            }
        }

        public Guid? ManagerID
        {
            get
            {
                if (Instance["ManagerID"].HasValue)
                    return Instance["ManagerID"].GuidValue;
                else
                    return null;
            }
        }

        private OrgUnitInfo _manager;
        public OrgUnitInfo Manager
        {
            get
            {
                if (_manager == null)
                {
                    Entity value = Instance["Manager"].EntityValue;
                    if (value == null)
                        return null;
                    _manager = new OrgUnitInfo(value);
                }
                return _manager;
            }
            set
            {
                _manager = value;
                if (value == null)
                    Instance["Manager"].EntityValue = null;
                else
                    Instance["Manager"].EntityValue = _manager.Instance;
            }
        }

        public Image Image
        {
            get
            {
                if (Instance == null)
                    return null;

                switch (Instance["BaseType"].StringValue.ToLower())
                {
                    case "sys.businessunit":
                        return Properties.Resources.Home16;
                    case "sys.workgroup":
                        return Properties.Resources.Group16;
                    case "sys.emploee":
                        return Properties.Resources.User16;
                    default:
                        return null;
                }
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }
        #endregion

        #region ====Ctor====
        public OrgUnitInfo() : base() { }

        public OrgUnitInfo(Entity instance) : base(instance) { }

        public OrgUnitInfo(BusinessUnitInfo businessUnit)
            : base(new Entity("sys.OrgUnit"))
        { this.Base = businessUnit; }

        public OrgUnitInfo(WorkGroupInfo workgroup)
            : base(new Entity("sys.OrgUnit"))
        { this.Base = workgroup; }

        public OrgUnitInfo(EmploeeInfo emploee)
            : base(new Entity("sys.OrgUnit"))
        { this.Base = emploee; }
        #endregion

        #region ====Methods====
        internal void InitManager(OrgUnitInfo manager)
        {
            _manager = manager;
        }
        #endregion

    }
}
