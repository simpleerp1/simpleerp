﻿namespace mzerp.UI
{
    partial class OrgUnitManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btNewGroup = new Telerik.WinControls.UI.RadMenuItem();
            this.btNewEmploee = new Telerik.WinControls.UI.RadMenuItem();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.tvOrgUnits = new Telerik.WinControls.UI.RadTreeView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.pv = new Telerik.WinControls.UI.RadPageView();
            this.pvInfo = new Telerik.WinControls.UI.RadPageViewPage();
            this.pvPermission = new Telerik.WinControls.UI.RadPageViewPage();
            this.tvPermissions = new Telerik.WinControls.UI.RadTreeView();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btNew = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.btDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvOrgUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pv)).BeginInit();
            this.pv.SuspendLayout();
            this.pvPermission.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvPermissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // btNewGroup
            // 
            this.btNewGroup.AccessibleDescription = "部门";
            this.btNewGroup.AccessibleName = "部门";
            this.btNewGroup.Image = global::mzerp.UI.Properties.Resources.Group16;
            this.btNewGroup.Name = "btNewGroup";
            this.btNewGroup.Text = "部门";
            this.btNewGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btNewEmploee
            // 
            this.btNewEmploee.AccessibleDescription = "员工";
            this.btNewEmploee.AccessibleName = "员工";
            this.btNewEmploee.Image = global::mzerp.UI.Properties.Resources.User16;
            this.btNewEmploee.Name = "btNewEmploee";
            this.btNewEmploee.Text = "员工";
            this.btNewEmploee.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 55);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(660, 462);
            this.radSplitContainer1.SplitterWidth = 4;
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.tvOrgUnits);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(200, 462);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2378049F, 0F);
            this.splitPanel1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Absolute;
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-156, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // tvOrgUnits
            // 
            this.tvOrgUnits.AllowDragDrop = true;
            this.tvOrgUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvOrgUnits.Location = new System.Drawing.Point(0, 0);
            this.tvOrgUnits.Name = "tvOrgUnits";
            this.tvOrgUnits.ShowLines = true;
            this.tvOrgUnits.Size = new System.Drawing.Size(200, 462);
            this.tvOrgUnits.SpacingBetweenNodes = -1;
            this.tvOrgUnits.TabIndex = 0;
            this.tvOrgUnits.Text = "radTreeView1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.pv);
            this.splitPanel2.Location = new System.Drawing.Point(204, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(456, 462);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2378049F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(156, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // pv
            // 
            this.pv.Controls.Add(this.pvInfo);
            this.pv.Controls.Add(this.pvPermission);
            this.pv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pv.Location = new System.Drawing.Point(0, 0);
            this.pv.Name = "pv";
            this.pv.SelectedPage = this.pvInfo;
            this.pv.Size = new System.Drawing.Size(456, 462);
            this.pv.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pv.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pvInfo
            // 
            this.pvInfo.Location = new System.Drawing.Point(10, 37);
            this.pvInfo.Name = "pvInfo";
            this.pvInfo.Size = new System.Drawing.Size(435, 414);
            this.pvInfo.Text = "组织单元属性";
            // 
            // pvPermission
            // 
            this.pvPermission.Controls.Add(this.tvPermissions);
            this.pvPermission.Location = new System.Drawing.Point(10, 37);
            this.pvPermission.Name = "pvPermission";
            this.pvPermission.Size = new System.Drawing.Size(435, 439);
            this.pvPermission.Text = "权限设置";
            // 
            // tvPermissions
            // 
            this.tvPermissions.CheckBoxes = true;
            this.tvPermissions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvPermissions.Location = new System.Drawing.Point(0, 0);
            this.tvPermissions.Name = "tvPermissions";
            this.tvPermissions.ShowLines = true;
            this.tvPermissions.Size = new System.Drawing.Size(435, 439);
            this.tvPermissions.SpacingBetweenNodes = -1;
            this.tvPermissions.TabIndex = 0;
            this.tvPermissions.Text = "radTreeView1";
            this.tvPermissions.TriStateMode = true;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btNew,
            this.btSave,
            this.btDelete});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btNew
            // 
            this.btNew.AccessibleDescription = "新建";
            this.btNew.AccessibleName = "新建";
            this.btNew.DisplayName = "commandBarDropDownButton1";
            this.btNew.DrawText = true;
            this.btNew.Image = global::mzerp.UI.Properties.Resources.Notepad16;
            this.btNew.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btNewGroup,
            this.btNewEmploee});
            this.btNew.Name = "btNew";
            this.btNew.Text = "新建";
            this.btNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton1";
            this.btSave.DrawText = true;
            this.btSave.Image = global::mzerp.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btDelete
            // 
            this.btDelete.AccessibleDescription = "删除";
            this.btDelete.AccessibleName = "删除";
            this.btDelete.DisplayName = "commandBarButton2";
            this.btDelete.DrawText = true;
            this.btDelete.Image = global::mzerp.UI.Properties.Resources.Delete16;
            this.btDelete.Name = "btDelete";
            this.btDelete.Text = "删除";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(660, 55);
            this.radCommandBar1.TabIndex = 0;
            // 
            // OrgUnitManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "OrgUnitManager";
            this.Size = new System.Drawing.Size(660, 517);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvOrgUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pv)).EndInit();
            this.pv.ResumeLayout(false);
            this.pvPermission.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvPermissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMenuItem btNewGroup;
        private Telerik.WinControls.UI.RadMenuItem btNewEmploee;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadTreeView tvOrgUnits;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarDropDownButton btNew;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.CommandBarButton btDelete;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.RadPageView pv;
        private Telerik.WinControls.UI.RadPageViewPage pvInfo;
        private Telerik.WinControls.UI.RadPageViewPage pvPermission;
        private Telerik.WinControls.UI.RadTreeView tvPermissions;
    }
}
