﻿using dps.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    class EmploeeInfo : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "sys.Emploee";

        #region ====Properties====
        public string Name
        {
            get { return Instance["Base"]["Name"].StringValue; }
            set { Instance["Base"]["Name"].StringValue = value; }
        }

        public string Account
        {
            get { return Instance["Account"].StringValue; }
            set { Instance["Account"].StringValue = value; }
        }

        private BusinessUnitInfo _businessUnit;
        public BusinessUnitInfo BusinessUnit
        {
            get
            {
                if (_businessUnit == null)
                {
                    Entity value = Instance["BusinessUnit"].EntityValue;
                    if (value == null)
                        return null;
                    _businessUnit = new BusinessUnitInfo(value);
                }

                return _businessUnit;
            }
            set
            {
                _businessUnit = value;
                if (value == null)
                    Instance["BusinessUnit"].EntityValue = null;
                else
                    Instance["BusinessUnit"].EntityValue = _businessUnit.Instance;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }
        #endregion

        #region ====Ctor====
        public EmploeeInfo() : base() { }

        public EmploeeInfo(Entity instance) : base(instance) { }
        #endregion

    }
}
