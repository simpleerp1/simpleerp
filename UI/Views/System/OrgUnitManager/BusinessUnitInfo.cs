﻿using dps.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    class BusinessUnitInfo : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "sys.BusinessUnit";

        #region ====Properties====
        public string Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public string ShortName
        {
            get { return Instance["ShortName"].StringValue; }
            set { Instance["ShortName"].StringValue = value; }
        }

        public bool IsInnerCompany
        {
            get { return Instance["IsInnerCompany"].BooleanValue; }
            set { Instance["IsInnerCompany"].BooleanValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }
        #endregion

        #region ====Ctor====
        public BusinessUnitInfo() : base() { }

        public BusinessUnitInfo(Entity instance) : base(instance) { }
        #endregion

    }
}
