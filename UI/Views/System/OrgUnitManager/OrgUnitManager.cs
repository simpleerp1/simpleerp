﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using dps.Common;
using dps.Common.Models;
using Telerik.WinControls;
using dps.Common.Data;

namespace mzerp.UI
{
    public partial class OrgUnitManager : UserControl
    {

        private dps.Data.Mapper.EntityList<OrgUnitInfo> source;
        private List<PermissionNode> _list = new List<PermissionNode>();
        private OrgUnitInfo _currentOU;
        private IOrgUnitView _currentView;
        private bool _inBindingMode; //判断是否因选择组织单元节点而引发权限树进入绑定模式，此时引发的权限树勾选与否不处理

        #region Views
        private BusinessUnitView _bizUnitView;
        protected BusinessUnitView BusinessUnitView
        {
            get
            {
                if (_bizUnitView == null)
                {
                    _bizUnitView = new BusinessUnitView();
                    _bizUnitView.Dock = DockStyle.Fill;
                }
                return _bizUnitView;
            }
        }

        private WorkgroupView _workgroupView;
        protected WorkgroupView WorkgroupView
        {
            get
            {
                if (_workgroupView == null)
                {
                    _workgroupView = new WorkgroupView();
                    _workgroupView.Dock = DockStyle.Fill;
                }
                return _workgroupView;
            }
        }

        private EmploeeView _empView;
        protected EmploeeView EmploeeView
        {
            get
            {
                if (_empView == null)
                {
                    _empView = new EmploeeView();
                    _empView.Dock = DockStyle.Fill;
                }
                return _empView;
            }
        }
        #endregion

        #region Ctor & Load
        public OrgUnitManager()
        {
            InitializeComponent();

            this.Load += OnLoad;
            this.tvOrgUnits.SelectedNodeChanged += OnOrgUnitsSelectedNodeChanged;
            this.tvOrgUnits.DragOverNode += OnOrgUnitsDragOverNode;
            this.tvOrgUnits.DragEnding += OnOrgUnitsDragEnding;

            this.tvPermissions.NodeCheckedChanged += tvPermissions_NodeCheckedChanged;
            this.tvPermissions.NodeFormatting += tvPermissions_NodeFormatting;

            this.btNewGroup.Click += CreateGroup;
            this.btNewEmploee.Click += CreateEmploee;
            this.btSave.Click += Save;
            this.btDelete.Click += Delete;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            LoadOrgUnitTreeAsync(this.tvOrgUnits, (s) => { this.source = s; });
            this.LoadPermissionTreeAsync();
        }
        #endregion

        #region Load OrgUnits
        internal static void LoadOrgUnitTreeAsync(RadTreeView tvOrgUnits, Action<dps.Data.Mapper.EntityList<OrgUnitInfo>> callback)
        {
            System.Threading.ThreadPool.QueueUserWorkItem((s) =>
            {
                dps.Common.Data.Query q = new dps.Common.Data.Query("sys.OrgUnit");
                q.Where(q.T["ParentID"] == null);
                dps.Common.Data.EntityList list = q.ToTreeList(q.T["SubItems"]);
                var orgunits = new dps.Data.Mapper.EntityList<OrgUnitInfo>(list);
                if (callback != null)
                    callback(orgunits);

                //处理Manager属性实例，防止从数据库重复加载
                //Todo:***考虑查询的ToList,ToTreeList方法内判断当前模型有没有引用自身的情况，然后做相应的自动处理
                Dictionary<Guid, OrgUnitInfo> temp = new Dictionary<Guid, OrgUnitInfo>();
                for (int i = 0; i < orgunits.Count; i++)
                {
                    LoopGetOuids(orgunits[i], temp);
                }
                for (int i = 0; i < orgunits.Count; i++)
                {
                    LoopSetManagerInstance(orgunits[i], temp);
                }

                tvOrgUnits.Invoke((Action)(() =>
                {
                    AddOrgUnitToTreeView(tvOrgUnits,orgunits);
                }));

            });
        }

        private static void LoopGetOuids(OrgUnitInfo ou, Dictionary<Guid, OrgUnitInfo> dic)
        {
            dic.Add(ou.Instance.ID, ou);

            if (ou.BaseType != EmploeeInfo.EntityModelID)
            {
                for (int i = 0; i < ou.Items.Count; i++)
                {
                    LoopGetOuids(ou.Items[i], dic);
                }
            }
        }

        private static void LoopSetManagerInstance(OrgUnitInfo ou, Dictionary<Guid, OrgUnitInfo> dic)
        {
            if (ou.ManagerID.HasValue)
            {
                ou.InitManager(dic[ou.ManagerID.Value]);
            }

            if (ou.BaseType != EmploeeInfo.EntityModelID)
            {
                for (int i = 0; i < ou.Items.Count; i++)
                {
                    LoopSetManagerInstance(ou.Items[i], dic);
                }
            }
        }

        private static void AddOrgUnitToTreeView(RadTreeView tvOrgUnits, dps.Data.Mapper.EntityList<OrgUnitInfo> source)
        {
            foreach (var item in source)
            {
                LoopAddOrgUnit(tvOrgUnits,null, item);
            }

            tvOrgUnits.Nodes[0].Expand();
            tvOrgUnits.Nodes[0].Selected = true;
        }

        private static void LoopAddOrgUnit(RadTreeView tvOrgUnits, RadTreeNode parent, OrgUnitInfo orgunit)
        {
            var node = new RadTreeNode(orgunit.Name);
            node.Image = orgunit.Image;
            node.Tag = orgunit;
            if (parent == null)
                tvOrgUnits.Nodes.Add(node);
            else
                parent.Nodes.Add(node);

            if (orgunit.Items.Count > 0)
            {
                foreach (var item in orgunit.Items)
                {
                    LoopAddOrgUnit(tvOrgUnits,node, item);
                }
            }
        }
        #endregion

        #region Load Permissions
        private void LoadPermissionTreeAsync()
        {
            System.Threading.ThreadPool.QueueUserWorkItem((s) =>
            {
                List<PermissionNode> list = new List<PermissionNode>();
                Dictionary<string, PermissionNode> apps = new Dictionary<string, PermissionNode>();
                Dictionary<Guid, PermissionNode> folders = new Dictionary<Guid, PermissionNode>();

                //----加载Apps----
                ApplicationModel[] appModels = SysService.Invoke(SysGloble.SysString,
                                               SysGloble.Service_PersistentService,
                                               "GetAllApplications", false) as ApplicationModel[];
                foreach (var app in appModels)
                {
                    PermissionNode appNode = new PermissionNode(app.LocalizedName.Value, PermissionNodeType.AppNode);
                    list.Add(appNode);
                    apps.Add(app.ID, appNode);
                }
                //----加载所有权限项的文件夹----
                MenuFolder[] menuFolders = SysService.Invoke(SysGloble.SysString, SysGloble.Service_PersistentService,
                                           "GetAllPermissionFolders") as MenuFolder[];
                foreach (var folder in menuFolders)
                {
                    this.LoopAddFolders(folder, null, apps, folders);
                }
                //----加载所有权限项并加入相应的文件夹----
                //注意：权限模型不在设计状态，提交到服务端保存时再变更为设计状态
                PermissionModel[] permissionModels = SysService.Invoke(SysGloble.SysString, SysGloble.Service_PersistentService,
                                                     "GetAllPermissions") as PermissionModel[];
                foreach (var pm in permissionModels)
                {
                    PermissionNode pn = new PermissionNode(pm.LocalizedName.Value, pm);
                    if (pm.FolderID.HasValue && folders.ContainsKey(pm.FolderID.Value))
                        folders[pm.FolderID.Value].Items.Add(pn);
                    else
                        apps[pm.AppID].Items.Add(pn);
                    _list.Add(pn);
                }

                //界面绑定数据源
                this.Invoke((Action)(() =>
                {
                    ////this.tvPermissions.ItemsSource = list;
                    for (int i = 0; i < list.Count; i++)
                    {
                        this.LoopAddPermissionNode(list[i], null);
                    }
                    //Todo:暂全部展开，因为勾选问题
                    this.tvPermissions.ExpandAll();
                }));
            });
        }

        private void LoopAddFolders(MenuFolder folder, PermissionNode parent,
                                    Dictionary<string, PermissionNode> apps,
                                    Dictionary<Guid, PermissionNode> folders)
        {
            if (parent == null)
                parent = apps[folder.AppID];
            PermissionNode folderItem = new PermissionNode(folder.LocalizedName.Value, PermissionNodeType.FolderNode);
            parent.Items.Add(folderItem);
            //加入索引
            folders.Add(folder.ID, folderItem);
            if (folder.HasSubItems)
            {
                foreach (var subItem in folder.Items)
                {
                    this.LoopAddFolders((MenuFolder)subItem, folderItem, apps, folders);
                }
            }
        }

        private void LoopAddPermissionNode(PermissionNode node, RadTreeNode parent)
        {
            RadTreeNode item = new RadTreeNode(node.Header);
            item.Tag = node;
            node.TargetTreeNode = item;


            if (parent == null)
                this.tvPermissions.Nodes.Add(item);
            else
                parent.Nodes.Add(item);

            //处理下级
            if (node.HasItems)
            {
                for (int i = 0; i < node.Items.Count; i++)
                {
                    this.LoopAddPermissionNode(node.Items[i], item);
                }
            }
        }

        void tvPermissions_NodeFormatting(object sender, TreeNodeFormattingEventArgs e)
        {
            if (e.Node.Parent == null)
            {
                e.NodeElement.ImageElement.Image = Properties.Resources.Application16;
            }
            else
            {
                if (e.Node.Nodes.Count > 0)
                {
                    if (e.Node.Expanded)
                        e.NodeElement.ImageElement.Image = Properties.Resources.folder_open;
                    else
                        e.NodeElement.ImageElement.Image = Properties.Resources.folder_close;
                }
                else
                    e.NodeElement.ImageElement.Image = Properties.Resources.GroupKey16;
            }
        }
        #endregion

        #region OrgUnits SelectedNodeChanged
        private void OnOrgUnitsSelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            _currentOU = this.tvOrgUnits.SelectedNode.Tag as OrgUnitInfo;
            if (_currentOU == null)
            {
                this.BindPermissions(null);
                return;
            }
            if (_currentOU.BaseType == BusinessUnitInfo.EntityModelID)
                this.btDelete.Enabled = false;
            else
                this.btDelete.Enabled = true;

            FlushAndUpdateNode();

            this._inBindingMode = true;
            this.BindPermissions(_currentOU);
            this._inBindingMode = false;

            this.pvInfo.Controls.Clear();
            this._currentView = null;
            switch (_currentOU.Base.ModelID)
            {
                case BusinessUnitInfo.EntityModelID:
                    this.pvInfo.Controls.Add(this.BusinessUnitView);
                    this._currentView = this.BusinessUnitView;
                    break;
                case WorkGroupInfo.EntityModelID:
                    this.pvInfo.Controls.Add(this.WorkgroupView);
                    this._currentView = this.WorkgroupView;
                    break;
                case EmploeeInfo.EntityModelID:
                    this.pvInfo.Controls.Add(this.EmploeeView);
                    this._currentView = this.EmploeeView;
                    break;
                default:
                    break;
            }

            if (_currentView != null)
            {
                _currentView.BindData(_currentOU);
            }
        }
        #endregion

        #region OrgUnits DragDrop
        private void OnOrgUnitsDragOverNode(object sender, RadTreeViewDragCancelEventArgs e)
        {
            OrgUnitInfo source = (OrgUnitInfo)e.Node.Tag;

            var treeViewItem = e.TargetNode;
            if (treeViewItem != null) //目标对象为RadTreeNode
            {
                OrgUnitInfo target = (OrgUnitInfo)treeViewItem.Tag;
                if (source.BaseType == BusinessUnitInfo.EntityModelID) //源为公司级
                {
                    if (e.DropPosition == DropPosition.AsChildNode) //不能移入任何目标对象的子级
                    { e.Cancel = true; return; }
                    if (target.BaseType != BusinessUnitInfo.EntityModelID) //如果目标不是公司，则都不允许
                    { e.Cancel = true; return; }
                }
                else //源为非公司级
                {
                    if (e.DropPosition == DropPosition.AsChildNode
                        && target.BaseType == EmploeeInfo.EntityModelID) //不能移入员工下级
                    { e.Cancel = true; return; }
                    else
                    {
                        if (target.BaseType == BusinessUnitInfo.EntityModelID) //上能移至跟公司同级
                        { e.Cancel = true; return; }
                    }
                }
            }
            else //目标对象为TreeView
            {
                //var treeview = e.Options.Destination as RadTreeView;
                //if (treeview != null && source.BaseType != BusinessUnitInfo.EntityModelID) //非公司级不能移动至树级
                //{ e.QueryResult = false; return; }
            }
        }

        private void OnOrgUnitsDragEnding(object sender, RadTreeViewDragCancelEventArgs e)
        {
            //先从旧的上级节点移除
            OrgUnitInfo source = (OrgUnitInfo)e.Node.Tag;
            OrgUnitInfo oldParent = (OrgUnitInfo)e.Node.Parent.Tag;
            oldParent.Items.Remove(source);

            switch (e.DropPosition)
            {
                case DropPosition.AfterNode:
                    {
                        var newParent = (OrgUnitInfo)e.TargetNode.Parent.Tag;
                        newParent.Items.Insert(e.TargetNode.Index + 1, source);
                        break;
                    }
                case DropPosition.AsChildNode:
                    {
                        var newParent = (OrgUnitInfo)e.TargetNode.Tag;
                        newParent.Items.Add(source);
                        break;
                    }
                case DropPosition.BeforeNode:
                    {
                        var newParent = (OrgUnitInfo)e.TargetNode.Parent.Tag;
                        newParent.Items.Insert(e.TargetNode.Index, source);
                        break;
                    }
                case DropPosition.None:
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Permission Methods
        /// <summary>
        /// 选中的组织单元改变时绑定权限树
        /// </summary>
        private void BindPermissions(OrgUnitInfo target)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                _list[i].BindPermission(target);
            }
        }

        private void tvPermissions_NodeCheckedChanged(object sender, TreeNodeCheckedEventArgs e)
        {
            if (this._inBindingMode)
                return;

            RadTreeNode node = e.Node;
            var pn = ((PermissionNode)node.Tag);
            //处理权限
            if (_currentOU == null || pn.PermissionModel == null)
                return;

            if (pn.IsChecked)
            {
                pn.PermissionModel.PermissionUsers.Add(_currentOU.Instance.ID);
                //1、往上提升：检查所有平级的权限节点是否都具有相同组织单元，是则自动提升
                this.LoopPromotion(_currentOU, pn);
                //2、往下删除：检查所有下级的权限节点是否具有相同组织单元，有则移除
                this.LoopRemoveOrgUnit(_currentOU, pn);
            }
            else
            {
                //向上降级：先判断是否继承下来的权限，是则降级上级组织单元的权限至其他平级
                this.LoopDemotion(_currentOU, pn);
                //注意：无需向下移除
            }
        }

        private void LoopPromotion(OrgUnitInfo ou, PermissionNode pn)
        {
            if (ou.Parent == null || ou.Parent.Items.Count == 1)
                return;

            bool allHas = true;
            for (int i = 0; i < ou.Parent.Items.Count; i++)
            {
                if (ou.Parent.Items[i].Instance.ID != ou.Instance.ID)
                {
                    bool curHas = false;
                    for (int j = 0; j < pn.PermissionModel.PermissionUsers.Count; j++)
                    {
                        if (pn.PermissionModel.HasAssingedOrgUnit(ou.Parent.Items[i].Instance.ID))
                        {
                            curHas = true;
                            break;
                        }
                    }
                    if (!curHas)
                    {
                        allHas = false;
                        break;
                    }
                }
            }

            if (allHas) //所有平级都包含相同的权限分配，则提升
            {
                //先删除所有平级的权限
                for (int i = 0; i < ou.Parent.Items.Count; i++)
                {
                    pn.PermissionModel.PermissionUsers.Remove(ou.Parent.Items[i].Instance.ID);
                }
                //加给上级组织单元
                pn.PermissionModel.PermissionUsers.Add(ou.Parent.Instance.ID);
                //最后向上递归上级的同层次
                this.LoopPromotion(ou.Parent, pn);
            }
        }

        private void LoopRemoveOrgUnit(OrgUnitInfo ou, PermissionNode pn)
        {
            for (int i = 0; i < ou.Items.Count; i++)
            {
                pn.PermissionModel.PermissionUsers.Remove(ou.Items[i].Instance.ID);
                //注意：必须往下递归查找
                this.LoopRemoveOrgUnit(ou.Items[i], pn);
            }
        }

        private void LoopDemotion(OrgUnitInfo ou, PermissionNode pn)
        {
            //先判断当前组织单元的权限是否继承而来
            if (pn.PermissionModel.HasAssingedOrgUnit(ou.Instance.ID))
            {
                pn.PermissionModel.PermissionUsers.Remove(ou.Instance.ID);
            }
            else
            {
                //勾选同层所有非当前的组织单元的权限
                for (int i = 0; i < ou.Parent.Items.Count; i++)
                {
                    if (ou.Parent.Items[i].Instance.ID != ou.Instance.ID)
                    {
                        pn.PermissionModel.PermissionUsers.Add(ou.Parent.Items[i].Instance.ID);
                    }
                }
                //继续往上递归
                this.LoopDemotion(ou.Parent, pn);
            }
        }

        internal PermissionModel[] GetChangedPermissionModels()
        {
            List<PermissionModel> list = new List<PermissionModel>();
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].PermissionModel.PersistentState != PersistentState.Unchanged)
                { list.Add(_list[i].PermissionModel); }
            }
            return list.ToArray();
        }
        #endregion

        #region Actions
        private void CreateEmploee(object sender, EventArgs e)
        {
            if (_currentOU == null)
            {
                RadMessageBox.Show(this, "请先选择上级节点再新建员工", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            //取根节点的BusinessUnit
            var rootOU = (OrgUnitInfo)this.tvOrgUnits.Nodes[0].Tag;

            var emploee = new EmploeeInfo();
            emploee.BusinessUnit = (BusinessUnitInfo)rootOU.Base;
            emploee.Name = "新员工";
            var orgUnit = new OrgUnitInfo(emploee);
            _currentOU.Items.Add(orgUnit);

            var node = new RadTreeNode(orgUnit.Name);
            node.Image = orgUnit.Image;
            node.Tag = orgUnit;
            this.tvOrgUnits.SelectedNode.Nodes.Add(node);
        }

        private void CreateGroup(object sender, EventArgs e)
        {
            if (_currentOU == null)
            {
                RadMessageBox.Show(this, "请先选择上级节点再新建部门或岗位", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            if (_currentOU.BaseType == EmploeeInfo.EntityModelID)
            {
                RadMessageBox.Show(this, "无法在员工节点下新建部门或岗位", "操作错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            //取根节点的BusinessUnit
            var rootOU = (OrgUnitInfo)this.tvOrgUnits.Nodes[0].Tag;

            var workgroup = new WorkGroupInfo();
            workgroup.BusinessUnit = (BusinessUnitInfo)rootOU.Base;
            workgroup.Name = "新部门";
            var orgUnit = new OrgUnitInfo(workgroup);
            _currentOU.Items.Add(orgUnit);

            var node = new RadTreeNode(orgUnit.Name);
            node.Image = orgUnit.Image;
            node.Tag = orgUnit;
            this.tvOrgUnits.SelectedNode.Nodes.Add(node);
        }

        private void FlushAndUpdateNode()
        {
            if (_currentView != null)
            {
                _currentView.FlushData();
                tvOrgUnits.SelectedNode.Text = _currentOU.Name;
            }
        }

        private void Save(object sender, EventArgs e)
        {
            //0、先更新当前的组织单元信息
            FlushAndUpdateNode();

            //1、先保存组织结构树
            try
            {
                SysService.Invoke(SysGloble.SysString, SysGloble.Service_PersistentService,
                                  SysGloble.Method_Persistent_Save, source.Target);
                //全部接受更改
                source.AcceptChanges();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, ex.Message, "保存组织架构失败", MessageBoxButtons.OK, RadMessageIcon.Error);
            }

            //2、再保存变更的权限模型
            try
            {
                PermissionModel[] changedPermissions = this.GetChangedPermissionModels();
                SysService.Invoke(SysGloble.SysString, SysGloble.Service_PersistentService,
                                                "SavePermissionModels", new object[] { changedPermissions });
                //全部接受更改
                for (int i = 0; i < changedPermissions.Length; i++)
                { changedPermissions[i].AcceptChanges(); }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, ex.Message, "保存权限失败", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            if (_currentOU == null)
                return;

            if (_currentOU.BaseType == BusinessUnitInfo.EntityModelID)
                return;

            var ouInfo = this.tvOrgUnits.SelectedNode.Tag as OrgUnitInfo;
            if (ouInfo == null)
                return;

            if (ouInfo.Items.Count > 0)
            {
                RadMessageBox.Show(this, "删除当前组织单元前请先删除所有下级组织单元", "操作失败", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            if (RadMessageBox.Show(this, "确认删除当前组织单元吗？", "确认删除", MessageBoxButtons.OKCancel, RadMessageIcon.Question) != DialogResult.OK)
                return;

            //如果是员工先删除用户
            if (ouInfo.BaseType == EmploeeInfo.EntityModelID)
            {
                var empInfo = (EmploeeInfo)ouInfo.Base;
                if (!string.IsNullOrEmpty(empInfo.Account))
                {
                    //尝试删除员工帐户
                    try
                    {
                        SysService.Invoke(SysGloble.SysString, SysGloble.Service_ManagementService, "RemoveUser", empInfo.Instance);
                    }
                    catch (Exception)
                    { }
                }
            }

            ouInfo.Parent.Items.Remove(ouInfo);

            this.Save(sender, e);

            tvOrgUnits.SelectedNode.Remove();

            //尝试删除对应的基类实例
            if (ouInfo.BaseType == EmploeeInfo.EntityModelID || ouInfo.BaseType == WorkGroupInfo.EntityModelID)
            {
                try
                {
                    ouInfo.Base.Instance.MarkDeleted();
                    SysService.Invoke(SysGloble.SysString, SysGloble.Service_PersistentService, "Save", ouInfo.Base.Instance);
                }
                catch (Exception)
                { }
            }
        }
        #endregion
    }
}
