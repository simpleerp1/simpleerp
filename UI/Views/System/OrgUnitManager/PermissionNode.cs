﻿using dps.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace mzerp.UI
{
    class PermissionNode : System.ComponentModel.INotifyPropertyChanged
    {

        #region ====Properties====
        private PermissionModel _permissionModel;
        public PermissionModel PermissionModel
        { get { return _permissionModel; } }

        private string _header;
        public string Header
        { get { return _header; } }

        public bool IsChecked
        {
            get
            {
                if (TargetTreeNode == null)
                    return false;
                else
                    return TargetTreeNode.Checked;
            }
            set
            {
                if (TargetTreeNode != null)
                {
                    TargetTreeNode.Checked = value;
                }
            }
        }

        public bool HasItems
        {
            get
            {
                return _items != null && _items.Count > 0;
            }
        }

        private List<PermissionNode> _items;
        public List<PermissionNode> Items
        {
            get
            {
                if (_items == null)
                    _items = new List<PermissionNode>();
                return _items;
            }
        }

        private RadTreeNode  _targetTreeNode;
        public RadTreeNode TargetTreeNode
        {
            get { return _targetTreeNode; }
            set { _targetTreeNode = value; }
        }
        #endregion

        #region ====Ctor====
        public PermissionNode(string header, PermissionNodeType nodeType)
        {
            _header = header;
        }

        public PermissionNode(string header, PermissionModel permissionModel)
            : this(header, PermissionNodeType.PermissionNode)
        {
            this._permissionModel = permissionModel;
        }
        #endregion

        #region ====INotifyPropertyChanged Implements====
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
        }
        #endregion

        #region ====BindPermission Methods====
        public void BindPermission(OrgUnitInfo target)
        {
            if (target == null)
            {
                this.IsChecked = false;
            }
            else
            {
                if (_permissionModel.PermissionUsers.Count == 0)
                {
                    this.IsChecked = false;
                }
                else
                {
                    for (int i = 0; i < _permissionModel.PermissionUsers.Count; i++)
                    {
                        if (LoopBindPermission(target, _permissionModel.PermissionUsers[i], 0))
                            break;
                    }
                }
            }
        }

        private bool LoopBindPermission(OrgUnitInfo target, Guid ouid, int level)
        {
            if (ouid == target.Instance.ID)
            {
                this.IsChecked = true;
                return true;
            }
            else
            {
                if (target.Parent == null) //在最顶层都没有找到相应的权限设置
                {
                    this.IsChecked = false;
                    return false;
                }
                else
                {
                    return this.LoopBindPermission(target.Parent, ouid, level + 1);
                }
            }
        }
        #endregion

    }

    enum PermissionNodeType
    {
        AppNode,
        FolderNode,
        PermissionNode
    }
}
