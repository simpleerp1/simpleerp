﻿namespace mzerp.UI
{
    partial class EmploeeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.btChangePassword = new Telerik.WinControls.UI.RadButton();
            this.btChangeAccount = new Telerik.WinControls.UI.RadButton();
            this.tbPassword = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbAccount = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btChangePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btChangeAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.btChangePassword);
            this.radGroupBox1.Controls.Add(this.btChangeAccount);
            this.radGroupBox1.Controls.Add(this.tbPassword);
            this.radGroupBox1.Controls.Add(this.label3);
            this.radGroupBox1.Controls.Add(this.tbAccount);
            this.radGroupBox1.Controls.Add(this.label2);
            this.radGroupBox1.Controls.Add(this.tbName);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "员工信息";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(374, 222);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "员工信息";
            // 
            // btChangePassword
            // 
            this.btChangePassword.Location = new System.Drawing.Point(278, 115);
            this.btChangePassword.Name = "btChangePassword";
            this.btChangePassword.Size = new System.Drawing.Size(69, 24);
            this.btChangePassword.TabIndex = 10;
            this.btChangePassword.Text = "更改密码";
            // 
            // btChangeAccount
            // 
            this.btChangeAccount.Location = new System.Drawing.Point(278, 80);
            this.btChangeAccount.Name = "btChangeAccount";
            this.btChangeAccount.Size = new System.Drawing.Size(69, 24);
            this.btChangeAccount.TabIndex = 9;
            this.btChangeAccount.Text = "更改帐号";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(98, 117);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(174, 20);
            this.tbPassword.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "密码：";
            // 
            // tbAccount
            // 
            this.tbAccount.Location = new System.Drawing.Point(98, 81);
            this.tbAccount.Name = "tbAccount";
            this.tbAccount.Size = new System.Drawing.Size(174, 20);
            this.tbAccount.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "帐号：";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(98, 41);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(174, 20);
            this.tbName.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "姓名：";
            // 
            // EmploeeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox1);
            this.Name = "EmploeeView";
            this.Size = new System.Drawing.Size(374, 222);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btChangePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btChangeAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton btChangePassword;
        private Telerik.WinControls.UI.RadButton btChangeAccount;
        private Telerik.WinControls.UI.RadTextBox tbPassword;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadTextBox tbAccount;
        private System.Windows.Forms.Label label2;
    }
}
