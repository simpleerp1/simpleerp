﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mzerp.UI
{
    public partial class WorkgroupView : UserControl, IOrgUnitView
    {

        private OrgUnitInfo _orgUnitInfo;

        public WorkgroupView()
        {
            InitializeComponent();
        }

        private void ClearBinding()
        {
            _orgUnitInfo = null;
            this.tbName.Text = string.Empty;
        }

        void IOrgUnitView.BindData(OrgUnitInfo ouInfo)
        {
            _orgUnitInfo = ouInfo;

            if (ouInfo == null)
            {
                ClearBinding();
                return;
            }

            //var bizUnitInfo = (BusinessUnitInfo)ouInfo.Base;
            this.tbName.Text = ouInfo.Name;
        }

        void IOrgUnitView.FlushData()
        {
            if (_orgUnitInfo == null)
                return;

            _orgUnitInfo.Name = this.tbName.Text;
            //var bizUnitInfo = (BusinessUnitInfo)_orgUnitInfo.Base;
        }
    }
}
