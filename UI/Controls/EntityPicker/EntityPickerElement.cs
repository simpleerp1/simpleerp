﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Design;
using Telerik.WinControls.Layouts;
using Telerik.WinControls.Primitives;
using Telerik.WinControls.UI;

namespace mzerp.UI.Controls
{
    public class EntityPickerElement : EditorBaseElement
    {

        //Todo:参照BaseComboBoxElement隐藏Textbox内的光标

        #region Fields
        private const int DefaultDropDownWidth = -1;
        private const int DefaultDropDownHeight = 106;

        private int dropDownWidth = DefaultDropDownWidth;
        private int dropDownHeight = DefaultDropDownHeight;

        protected RadTextBoxItem textBox;
        private RadTextBoxElement textBoxPanel;
        private FillPrimitive fillPrimitive;
        private BorderPrimitive borderPrimitive;
        private RadArrowButtonElement arrowButton;
        private ComboBoxEditorLayoutPanel layoutPanel;

        private Size dropDownMinSize = Size.Empty;
        private Size dropDownMaxSize = Size.Empty;

        private bool keyboardCommandIssued = false;

        private EntityPickerPopup popup;
        private bool isPopOpen;

        private string _displayMember;
        private dps.Data.Mapper.EntityBase _selectedEntity;

        #endregion

        #region Dependency properties
        private static RadProperty DropDownStyleProperty = RadProperty.Register(
            "DropDownStyle", typeof(RadDropDownStyle), typeof(EntityPickerElement), new RadElementPropertyMetadata(
                RadDropDownStyle.DropDown, ElementPropertyOptions.None));
        #endregion

        #region Properties
        public RadTextBoxItem TextBox
        {
            get { return textBox; }
        }

        public string DisplayMember
        {
            get { return _displayMember; }
            set { _displayMember = value; }
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get { return _selectedEntity; }
            set
            {
                if (_selectedEntity != value)
                {
                    _selectedEntity = value;
                    if (value == null)
                    {
                        this.Text = string.Empty;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(DisplayMember))
                        {
                            this.Text = _selectedEntity.Instance.ToString();
                        }
                        else
                        {
                            var v = _selectedEntity.Instance[DisplayMember].Value;
                            if (v == null)
                                this.Text = null;
                            else
                                this.Text = v.ToString();
                        }
                    }

                    var entityPicker = this.ElementTree.Control as EntityPicker;
                    if (entityPicker != null)
                        entityPicker.OnSelectedChanged();
                }
            }
        }

        public Guid? SelectedEntityID
        {
            get
            {
                if (SelectedEntity != null)
                    return SelectedEntity.Instance.ID;
                else
                    return null;
            }
        }

        protected EntityPickerPopup Popup
        {
            get { return  popup; }
        }

        public IEntityPickerView PickerView
        {
            get { return popup.PickerView; }
            set { popup.PickerView = value; }
        }

        /// <summary>
        /// Gets a value indicating whether a keyboard command has been issued.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool KeyboardCommandIssued
        {
            get { return keyboardCommandIssued; }
            set { keyboardCommandIssued = value; }
        }

        /// <summary>
        /// Gets the arrow button element.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public RadArrowButtonElement ArrowButton
        {
            get
            {
                return this.arrowButton;
            }
        }

        /// <summary>
        /// The popup form that hosts the RadGridView.
        /// </summary>
        //public EntityPickerPopupForm EntityPickerPopupForm
        //{
        //    get
        //    {
        //        return (this.PopupForm as EntityPickerPopupForm);
        //    }
        //}

        /// <summary>
        /// Gets or sets a value specifying the style of the combo box. 
        /// </summary>
        [Browsable(true), Category(RadDesignCategory.AppearanceCategory)]
        [RadPropertyDefaultValue("DropDownStyle", typeof(BaseComboBoxElement)),
        Description("Gets or sets a value specifying the style of the combo box."),
        RefreshProperties(RefreshProperties.Repaint)]
        public RadDropDownStyle DropDownStyle
        {
            get
            {
                return (RadDropDownStyle)this.GetValue(DropDownStyleProperty);
            }
            set
            {
                this.SetValue(DropDownStyleProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the height in pixels of the drop-down portion of the ComboBox. 
        /// </summary>
        [Browsable(true), Category(RadDesignCategory.BehaviorCategory)]
        [Description("Gets or sets the height in pixels of the drop-down portion of the ComboBox."),
        DefaultValue(DefaultDropDownHeight),
        EditorBrowsable(EditorBrowsableState.Always)]
        public int DropDownHeight
        {
            get
            {
                return this.dropDownHeight;
            }
            set
            {
                if (this.dropDownHeight != value)
                {
                    this.dropDownHeight = value;
                    this.OnNotifyPropertyChanged("DropDownHeight");
                }
            }
        }

        [Browsable(true), Category(RadDesignCategory.AppearanceCategory)]
        [Description("Gets or sets the drop down maximum size.")]
        [DefaultValue(typeof(Size), "0,0")]
        public Size DropDownMaxSize
        {
            get
            {
                return this.dropDownMaxSize;
            }
            set
            {
                if (this.dropDownMaxSize != value)
                {
                    this.dropDownMaxSize = value;

                    if (this.dropDownMinSize != Size.Empty)
                    {
                        if (this.dropDownMaxSize.Width < this.dropDownMinSize.Width)
                        {
                            this.dropDownMinSize.Width = this.dropDownMaxSize.Width;
                        }
                        if (this.dropDownMaxSize.Height < this.dropDownMinSize.Height)
                        {
                            this.dropDownMinSize.Height = this.dropDownMaxSize.Height;
                        }
                    }
                }
            }
        }

        public bool IsPopupOpen
        { get { return isPopOpen; } }
        #endregion

        #region Initialization & dispose
        static EntityPickerElement()
        {
            ItemStateManagerFactoryRegistry.AddStateManagerFactory(new RadTextBoxElementStateManager(), typeof(EntityPickerElement));
        }

        public EntityPickerElement()
        {
            this.AutoSizeMode = RadAutoSizeMode.FitToAvailableSize;
            if (!DesignMode)
            {
                this.popup = new EntityPickerPopup(this);
                if (SystemInformation.IsComboBoxAnimationEnabled)
                {
                    popup.ShowingAnimation = PopupAnimations.Slide | PopupAnimations.TopToBottom;
                    popup.HidingAnimation = PopupAnimations.Slide | PopupAnimations.BottomToTop;
                }
                else
                {
                    popup.ShowingAnimation = popup.HidingAnimation = PopupAnimations.None;
                }
                popup.Closed += OnPopupClosed;
            }
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            //fix - initially the textbox displays the end the of the (when long string used) instead of the beginning of it.
            //TFS ID# 123503
            //this.BeginUpdate();
            //string oldText = this.TextBoxElement.Text;
            //this.TextBoxElement.Text = string.Empty;
            //this.TextBoxElement.Text = oldText;
            //this.EndUpdate();
        }

        protected override void CreateChildElements()
        {
            this.arrowButton = new RadArrowButtonElement();
            this.arrowButton.Arrow.AutoSize = true;
            this.arrowButton.MinSize = new Size(RadArrowButtonElement.RadArrowButtonDefaultSize.Width, this.arrowButton.ArrowFullSize.Height);
            this.arrowButton.Class = "ComboBoxdropDownButton";
            this.arrowButton.ClickMode = ClickMode.Press;

            this.textBoxPanel = new RadTextBoxElement();
            this.textBoxPanel.ThemeRole = "ComboTextBoxElement";
            this.textBoxPanel.AutoSizeMode = RadAutoSizeMode.WrapAroundChildren;
            this.textBoxPanel.ShowBorder = false;
            this.textBoxPanel.Class = "ComboBoxTextEditor";

            this.textBox = this.textBoxPanel.TextBoxItem;
            this.textBox.Multiline = false;
            this.textBox.ReadOnly = true;
            this.textBox.Font = this.Font;
            this.textBox.KeyUp += OnTextBoxKeyUp;

            //this.WireEvents();

            this.borderPrimitive = new BorderPrimitive();
            this.borderPrimitive.Class = "ComboBoxBorder";
            this.borderPrimitive.ZIndex = 1;

            this.fillPrimitive = new FillPrimitive();
            this.fillPrimitive.BindProperty(FillPrimitive.AutoSizeModeProperty, this, RadElement.AutoSizeModeProperty, PropertyBindingOptions.TwoWay);
            this.fillPrimitive.Class = "ComboBoxFill";

            this.layoutPanel = new ComboBoxEditorLayoutPanel();
            this.layoutPanel.Content = textBoxPanel;
            this.layoutPanel.ArrowButton = this.arrowButton;

            this.Children.Add(this.fillPrimitive);
            this.Children.Add(this.borderPrimitive);
            this.Children.Add(this.layoutPanel);

            if (DesignMode)
            {
                this.textBox.TextBoxControl.Enabled = false;
                this.textBox.TextBoxControl.BackColor = Color.White;
            }

            //this.ArrowButton.Click += new EventHandler(ArrowButton_Click);
        }

        protected override void DisposeManagedResources()
        {
            ////if (this.ArrowButton != null)
            ////{
            ////    this.ArrowButton.Click -= new EventHandler(ArrowButton_Click);
            ////}
            this.textBox.KeyUp -= OnTextBoxKeyUp;
            if (this.popup != null)
                this.popup.Closed -= OnPopupClosed;

            this.DisposeChildren();
            this.Children.Clear();

            base.DisposeManagedResources();

            if (this.popup != null && !this.popup.IsDisposed)
                this.popup.Dispose();
            this.popup = null;
            //if (usedInRadGridView && this.PopupForm != null && !this.PopupForm.IsDisposed)
            //{
            //    this.PopupForm.Dispose();
            //}
        }

        #endregion

        #region Event Handlers
        private void OnPopupClosed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            isPopOpen = false;
        }

        private void OnTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("EntityPickerElement.OnTextBoxKeyUp:" + e.KeyCode.ToString());
            if (e.KeyCode == Keys.Enter)
            {
                if (!this.isPopOpen)
                {
                    this.ShowPopup();
                    //this.textBox.Focus();
                }
            }
        }

        protected override void OnBubbleEvent(RadElement sender, RoutedEventArgs args)
        {
            if (args.RoutedEvent == RadItem.MouseWheelEvent &&
                sender == this.textBox)
            {
                this.KeyboardCommandIssued = false;
                this.OnMouseWheel((MouseEventArgs)args.OriginalEventArgs);
            }

            if (args.RoutedEvent == RadElement.MouseDownEvent)
            {
                this.KeyboardCommandIssued = false;
                if ((sender == this.textBox && (this.DropDownStyle == RadDropDownStyle.DropDownList)) ||
                    (sender == this.arrowButton))
                {
                    if (!this.isPopOpen)
                    {
                        this.ShowPopup();
                        //this.textBox.Focus();
                    }
                }
            }

            base.OnBubbleEvent(sender, args);
        }

        ////private void ArrowButton_Click(object sender, EventArgs e)
        ////{
        ////    //if (this.AutoFilter)
        ////    //{
        ////    //    this.ClearFilter();
        ////    //}
        ////}
        #endregion

        #region Overrides
        protected override void OnPropertyChanged(RadPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == RadItem.TextProperty)
            {
                this.textBox.Text = (string)e.NewValue;
            }
        }

        ////protected virtual void ApplyThemeToPopupForm()
        ////{
        ////    //string popupThemeName = "ControlDefault";
        ////    //if (this.ElementTree != null && this.ElementTree.ComponentTreeHandler != null &&
        ////    //    !string.IsNullOrEmpty(this.ElementTree.ComponentTreeHandler.ThemeName))
        ////    //{
        ////    //    popupThemeName = this.ElementTree.ComponentTreeHandler.ThemeName;
        ////    //}

        ////    //RadPopupControlBase popupForm = this.PopupForm;
        ////    //if (popupForm.ThemeName != popupThemeName)
        ////    //{
        ////    //    popupForm.ThemeName = popupThemeName;
        ////    //    // Let the layout calculate the desired size of items
        ////    //    // because the new theme could change it
        ////    //    popupForm.RootElement.UpdateLayout();
        ////    //}
        ////}

        private void SetDropDownMinMaxSize()
        {
            //RadPopupControlBase popupForm = this.PopupForm;
            //if (this.DropDownSizingMode != SizingMode.None)
            //{
            //    RadSizablePopupControl sizablePopupForm = popupForm as RadSizablePopupControl;
            //    if (sizablePopupForm != null)
            //    {
            //        RadElement child = sizablePopupForm.SizingGrip.Children[3];
            //        Size fullSize = Size.Add(child.BoundingRectangle.Size, child.Margin.Size);

            //        sizablePopupForm.MinimumSize = LayoutUtils.UnionSizes(this.dropDownMinSize, fullSize);

            //        if (this.dropDownMaxSize != Size.Empty)
            //        {
            //            sizablePopupForm.MaximumSize = this.dropDownMaxSize;
            //        }
            //    }
            //}
            //else
            //{
            //    popupForm.MinimumSize = new Size(492, 318);//Size.Empty;
            //    popupForm.MaximumSize =new Size(492, 318);//Size.Empty;
            //}
        }

        protected virtual void ShowPopup()
        {
            popup.Show(this.ElementTree.Control);
        }

        ////private void ClosePopup()
        ////{
        ////    isPopOpen = false;
        ////    popup.Close();
        ////}

        protected override Type ThemeEffectiveType
        {
            get
            {
                return typeof(RadMultiColumnComboBoxElement);
            }
        }
        #endregion


    }
}
