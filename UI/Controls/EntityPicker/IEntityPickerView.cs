﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI.Controls
{
    public interface IEntityPickerView : IDisposable
    {

        dps.Data.Mapper.EntityBase SelectedEntity { get; }

        void LoadData();

        /// <summary>
        /// 设置输入焦点
        /// </summary>
        void FocusInput();

        void MoveUp();

        void MoveDown();

        //void PageUp();

        //void PageDown();

        //void MoveFirst();

        //void MoveLast();

    }
}
