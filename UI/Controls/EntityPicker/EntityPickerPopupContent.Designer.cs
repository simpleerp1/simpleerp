﻿namespace mzerp.UI.Controls
{
    partial class EntityPickerPopupContent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.plView = new System.Windows.Forms.Panel();
            this.btCancel = new Telerik.WinControls.UI.RadButton();
            this.btOK = new Telerik.WinControls.UI.RadButton();
            this.btClear = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btClear)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.btClear);
            this.radPanel1.Controls.Add(this.plView);
            this.radPanel1.Controls.Add(this.btCancel);
            this.radPanel1.Controls.Add(this.btOK);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(366, 308);
            this.radPanel1.TabIndex = 2;
            // 
            // plView
            // 
            this.plView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plView.Location = new System.Drawing.Point(3, 3);
            this.plView.Name = "plView";
            this.plView.Size = new System.Drawing.Size(360, 263);
            this.plView.TabIndex = 4;
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.Location = new System.Drawing.Point(298, 272);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(55, 24);
            this.btCancel.TabIndex = 3;
            this.btCancel.Text = "取消";
            // 
            // btOK
            // 
            this.btOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOK.Location = new System.Drawing.Point(237, 272);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(55, 24);
            this.btOK.TabIndex = 2;
            this.btOK.Text = "确认";
            // 
            // btClear
            // 
            this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClear.Location = new System.Drawing.Point(176, 272);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(55, 24);
            this.btClear.TabIndex = 5;
            this.btClear.Text = "清空";
            // 
            // EntityPickerPopupContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Name = "EntityPickerPopupContent";
            this.Size = new System.Drawing.Size(366, 308);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btClear)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.Panel plView;
        private Telerik.WinControls.UI.RadButton btCancel;
        private Telerik.WinControls.UI.RadButton btOK;
        private Telerik.WinControls.UI.RadButton btClear;

    }
}
