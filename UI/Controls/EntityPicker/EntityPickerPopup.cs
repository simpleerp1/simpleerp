﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mzerp.UI.Controls
{

    public class EntityPickerPopup : Popup
    {

        private EntityPickerPopupContent _content = null;
        private EntityPickerElement _owner;
        internal EntityPickerElement Owner
        {
            get { return _owner; }
        }

        public IEntityPickerView PickerView
        {
            get { return _content.PickerView; }
            set { _content.PickerView = value; }
        }

        #region Ctor
        public EntityPickerPopup(EntityPickerElement owner)
            : base(new EntityPickerPopupContent())
        {
            _owner = owner;
            _content = (EntityPickerPopupContent)base.Content;
            _content.Owner = this;
        }
        #endregion

        #region Overrides
        protected override void OnOpened(EventArgs e)
        {
            base.OnOpened(e);
            this.PickerView.FocusInput();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _owner = null;
                if (_content.PickerView != null)
                    _content.PickerView.Dispose();
                _content.Owner = null;
            }
            base.Dispose(disposing);
        }
        #endregion

    }

}
