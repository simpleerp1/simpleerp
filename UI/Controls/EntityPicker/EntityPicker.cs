﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI.Controls
{
    public class EntityPicker : RadControl
    {

        private EntityPickerElement entityPickerElement = null;

        public event EventHandler SelectedChanged;

        #region Properties
        public IEntityPickerView PickerView
        {
            get { return entityPickerElement.PickerView; }
            set { entityPickerElement.PickerView = value; }
        }

        public string DisplayMember
        {
            get { return entityPickerElement.DisplayMember; }
            set { entityPickerElement.DisplayMember = value; }
        }

        public Guid? SelectedEntityID
        {
            get { return entityPickerElement.SelectedEntityID; }
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get { return entityPickerElement.SelectedEntity; }
            set { entityPickerElement.SelectedEntity = value; }
        }
        #endregion

        #region Constructors, initialization & disposal
        public EntityPicker()
        {
            this.TabStop = false;
            this.SetStyle(ControlStyles.Selectable, true);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //this.UnwireEvents();
                this.entityPickerElement.Dispose();
                this.entityPickerElement = null;
            }
            base.Dispose(disposing);
        }

        protected override void CreateChildItems(RadElement parent)
        {
            this.entityPickerElement = new EntityPickerElement();
            this.entityPickerElement.ArrowButton.Arrow.AutoSize = true;
            this.entityPickerElement.AutoSizeMode = RadAutoSizeMode.WrapAroundChildren;

            //this.WireEvents();

            this.RootElement.Children.Add(this.entityPickerElement);

            base.CreateChildItems(parent);
        }

        internal void OnSelectedChanged()
        {
            if (SelectedChanged != null)
                SelectedChanged(this, EventArgs.Empty);
        }
        #endregion

        #region Theme
        public override string ThemeClassName
        {
            get
            {
                return typeof(Telerik.WinControls.UI.RadMultiColumnComboBox).FullName;
            }
        }
        #endregion

    }
}
