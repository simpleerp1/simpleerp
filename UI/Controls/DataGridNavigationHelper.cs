﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace mzerp.UI.Controls
{
    class DataGridNavigationHelper
    {

        public static void MoveUp(RadGridView grid)
        {
            if (grid.SelectedRows.Count == 0)
                return;

            var index = grid.SelectedRows[0].Index;
            if (index == 0)
                return;

            grid.ClearSelection();
            grid.Rows[index - 1].IsCurrent = true;
        }

        public static void MoveDown(RadGridView grid)
        {
            if (grid.SelectedRows.Count == 0)
                return;

            var index = grid.SelectedRows[0].Index;
            if (index == grid.Rows.Count - 1)
                return;

            grid.ClearSelection();
            grid.Rows[index + 1].IsCurrent = true;
        }

    }
}
