﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace mzerp.UI.Controls
{
    public class EnumCellElement : GridDataCellElement
    {

        public EnumCellElement(GridViewColumn column, GridRowElement row)
            : base(column, row)
        { }

        protected override void SetContentCore(object value)
        {
            if (value == null)
                this.Text = null;
            else
            {
                var column = (EnumColumn)this.DataColumnInfo;
                if (string.IsNullOrEmpty(column.EnumModelID))
                    throw new System.Exception("EnumColumn["+column.Name+"] has not set EnumModelID");
                var enumModel = dps.Common.SysService.GetEnumModel(column.EnumModelID, false);
                if (enumModel == null)
                    throw new System.Exception("EnumModel with id=" + column.EnumModelID + " not existed.");
                int v = (int)value;

                foreach (var item in enumModel.Items)
                {
                    if (item.Value == v)
                    {
                        this.Text = item.LocalizedName.Value;
                        return;
                    }
                }

                this.Text = v.ToString();
            }
        }

        public override bool IsCompatible(GridViewColumn data, object context)
        {
            return data is EnumColumn && context is GridDataRowElement;
        }

        protected override Type ThemeEffectiveType
        {
            get
            {
                return typeof(GridDataCellElement);
            }
        }

    }
}
