﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;
using dps.Common.Models;

namespace mzerp.UI.Controls
{
    public class EnumPicker : RadDropDownList
    {

        private string _enumModelID;
        public string EnumModelID
        {
            get { return _enumModelID; }
            set  {
                if (_enumModelID != value)
                {
                    _enumModelID = value;
                    if (!DesignMode)
                    {
                        var enumModel = dps.Common.SysService.GetEnumModel(_enumModelID, false);
                        if (enumModel == null)
                            throw new System.Exception("EnumModel with id=" + _enumModelID + " not existed.");
                       
                        base.DataSource = enumModel.Items;
                    }
                }
            }
        }

        public EnumPicker() :base()
        {
            base.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            base.DisplayMember = "LocalizedName.Value";
            base.ValueMember = "Value";
        }

        #region Theme
        public override string ThemeClassName
        {
            get
            {
                return typeof(Telerik.WinControls.UI.RadDropDownList).FullName;
            }
        }
        #endregion
    }
}
