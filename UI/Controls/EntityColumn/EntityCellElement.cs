﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace mzerp.UI.Controls
{
    public class EntityCellElement: GridDataCellElement
    {

        public EntityCellElement(GridViewColumn column, GridRowElement row)
            : base(column, row)
        { }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();
        }

        protected override void SetContentCore(object value)
        {
            if (value == null)
                this.Text = null;
            else
            {
                var column = (EntityColumn)this.DataColumnInfo;
                var entity = (dps.Data.Mapper.EntityBase)value;
                if (string.IsNullOrEmpty(column.DisplayMember))
                    this.Text = entity.ToString();
                else
                {
                    var v = entity.Instance[column.DisplayMember].Value;
                    if (v == null)
                        this.Text = null;
                    else
                        this.Text = v.ToString();
                } 
            }
        }

        public override bool IsCompatible(GridViewColumn data, object context)
        {
            return data is EntityColumn && context is GridDataRowElement;
        }

        protected override Type ThemeEffectiveType
        {
            get
            {
                return typeof(GridDataCellElement);
            }
        }

    }
}
