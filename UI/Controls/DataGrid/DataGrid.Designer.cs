﻿namespace mzerp.UI.Controls
{
    partial class DataGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.gvList = new Telerik.WinControls.UI.RadGridView();
            this.pageBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btMoveFirst = new Telerik.WinControls.UI.CommandBarButton();
            this.btMovePre = new Telerik.WinControls.UI.CommandBarButton();
            this.btMoveNext = new Telerik.WinControls.UI.CommandBarButton();
            this.btMoveLast = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel2 = new Telerik.WinControls.UI.CommandBarLabel();
            this.tbPageSize = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.tbTotalCount = new Telerik.WinControls.UI.CommandBarLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gvList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvList.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageBar)).BeginInit();
            this.SuspendLayout();
            // 
            // gvList
            // 
            this.gvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvList.Location = new System.Drawing.Point(0, 0);
            // 
            // gvList
            // 
            this.gvList.MasterTemplate.AllowAddNewRow = false;
            this.gvList.MasterTemplate.AllowColumnChooser = false;
            this.gvList.MasterTemplate.AllowDeleteRow = false;
            this.gvList.MasterTemplate.AllowEditRow = false;
            this.gvList.Name = "gvList";
            this.gvList.ReadOnly = true;
            this.gvList.ShowGroupPanel = false;
            this.gvList.Size = new System.Drawing.Size(657, 410);
            this.gvList.TabIndex = 0;
            this.gvList.Text = "radGridView1";
            // 
            // pageBar
            // 
            this.pageBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pageBar.Location = new System.Drawing.Point(0, 410);
            this.pageBar.Name = "pageBar";
            this.pageBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.pageBar.Size = new System.Drawing.Size(657, 55);
            this.pageBar.TabIndex = 2;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btMoveFirst,
            this.btMovePre,
            this.btMoveNext,
            this.btMoveLast,
            this.commandBarSeparator1,
            this.commandBarLabel2,
            this.tbPageSize,
            this.commandBarLabel1,
            this.tbTotalCount});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btMoveFirst
            // 
            this.btMoveFirst.AccessibleDescription = "commandBarButton1";
            this.btMoveFirst.AccessibleName = "commandBarButton1";
            this.btMoveFirst.DisplayName = "commandBarButton1";
            this.btMoveFirst.Image = global::mzerp.UI.Properties.Resources.moveFirst;
            this.btMoveFirst.Name = "btMoveFirst";
            this.btMoveFirst.Text = "commandBarButton1";
            this.btMoveFirst.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btMovePre
            // 
            this.btMovePre.AccessibleDescription = "commandBarButton2";
            this.btMovePre.AccessibleName = "commandBarButton2";
            this.btMovePre.DisplayName = "commandBarButton2";
            this.btMovePre.Image = global::mzerp.UI.Properties.Resources.previous;
            this.btMovePre.Name = "btMovePre";
            this.btMovePre.Text = "commandBarButton2";
            this.btMovePre.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btMoveNext
            // 
            this.btMoveNext.AccessibleDescription = "commandBarButton3";
            this.btMoveNext.AccessibleName = "commandBarButton3";
            this.btMoveNext.DisplayName = "commandBarButton3";
            this.btMoveNext.Image = global::mzerp.UI.Properties.Resources.next;
            this.btMoveNext.Name = "btMoveNext";
            this.btMoveNext.Text = "commandBarButton3";
            this.btMoveNext.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btMoveLast
            // 
            this.btMoveLast.AccessibleDescription = "commandBarButton4";
            this.btMoveLast.AccessibleName = "commandBarButton4";
            this.btMoveLast.DisplayName = "commandBarButton4";
            this.btMoveLast.Image = global::mzerp.UI.Properties.Resources.moveLast;
            this.btMoveLast.Name = "btMoveLast";
            this.btMoveLast.Text = "commandBarButton4";
            this.btMoveLast.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel2
            // 
            this.commandBarLabel2.AccessibleDescription = "每页记录数：";
            this.commandBarLabel2.AccessibleName = "每页记录数：";
            this.commandBarLabel2.DisplayName = "commandBarLabel2";
            this.commandBarLabel2.Name = "commandBarLabel2";
            this.commandBarLabel2.Text = "每页记录数：";
            this.commandBarLabel2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbPageSize
            // 
            this.tbPageSize.DisplayName = "commandBarDropDownList1";
            this.tbPageSize.DropDownAnimationEnabled = true;
            this.tbPageSize.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "10";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "20";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "50";
            radListDataItem3.TextWrap = true;
            this.tbPageSize.Items.Add(radListDataItem1);
            this.tbPageSize.Items.Add(radListDataItem2);
            this.tbPageSize.Items.Add(radListDataItem3);
            this.tbPageSize.MaxDropDownItems = 0;
            this.tbPageSize.MinSize = new System.Drawing.Size(45, 22);
            this.tbPageSize.Name = "tbPageSize";
            this.tbPageSize.Text = "";
            this.tbPageSize.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.AccessibleDescription = "记录总数：";
            this.commandBarLabel1.AccessibleName = "记录总数：";
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "记录总数：";
            this.commandBarLabel1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbTotalCount
            // 
            this.tbTotalCount.DisplayName = "commandBarLabel3";
            this.tbTotalCount.MaxSize = new System.Drawing.Size(50, 22);
            this.tbTotalCount.MinSize = new System.Drawing.Size(50, 22);
            this.tbTotalCount.Name = "tbTotalCount";
            this.tbTotalCount.Text = "";
            this.tbTotalCount.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tbTotalCount.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // DataGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gvList);
            this.Controls.Add(this.pageBar);
            this.Name = "DataGrid";
            this.Size = new System.Drawing.Size(657, 465);
            ((System.ComponentModel.ISupportInitialize)(this.gvList.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView gvList;
        private Telerik.WinControls.UI.RadCommandBar pageBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btMoveFirst;
        private Telerik.WinControls.UI.CommandBarButton btMovePre;
        private Telerik.WinControls.UI.CommandBarButton btMoveNext;
        private Telerik.WinControls.UI.CommandBarButton btMoveLast;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel2;
        private Telerik.WinControls.UI.CommandBarDropDownList tbPageSize;
        private Telerik.WinControls.UI.CommandBarLabel tbTotalCount;
    }
}
