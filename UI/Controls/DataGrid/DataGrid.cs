﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Drawing.Design;

namespace mzerp.UI.Controls
{
    public partial class DataGrid : UserControl
    {

        private int _pageIndex;
        private int _pageSize = 20;
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value;
                this.tbPageSize.Text = _pageSize.ToString();
            }
        }

        /// <summary>
        /// 是否允许分页显示数据
        /// </summary>
        public bool AllowPage
        {
            get { return _pageIndex != -1; }
            set
            {
                if (value)
                {
                    this.pageBar.Visible = true;
                    this._pageIndex = 0;
                }
                else
                {
                    this.pageBar.Visible = false;
                    this._pageIndex = -1;
                    this._pageSize = 0;
                }
            }
        }

        private int _totalPages = 1;

        public RadGridView GridView
        {
            get { return this.gvList; }
        }

        [Category("Data"), Editor("Telerik.WinControls.UI.Design.GridViewColumnCollectionEditor, Telerik.WinControls.UI.Design", typeof(UITypeEditor)), MergableProperty(false), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public GridViewColumnCollection Columns
        {
            get { return this.gvList.MasterTemplate.Columns; }
        }

        private string _appID;
        private string _serviceID;
        private string _method;
        public string QueryMethod
        {
            set
            {
                string[] sr = value.Split('.');
                if (sr.Length != 3)
                    throw new System.Exception("DataGrid.QueryMethod Error");

                _appID = sr[0];
                _serviceID = sr[1];
                _method = sr[2];
            }
        }

        private object[] _queryArgs;

        private Func<object[]> _getQueryArgsFunc;
        public Func<object[]> GetQueryArgsFunc
        {
            set { _getQueryArgsFunc = value; }
        }

        public Guid SelectedEntityID
        {
            get
            {
                if (this.gvList.SelectedRows.Count == 0)
                    return Guid.Empty;

                var row = this.gvList.SelectedRows[0].DataBoundItem as System.Data.DataRowView;
                if (row == null)
                    return Guid.Empty;
                return (Guid)row["ID"];
            }
        }

        public DataGrid()
        {
            InitializeComponent();

            this.tbPageSize.SelectedIndex = 1;
            this.tbTotalCount.Text = "0";
            this.tbPageSize.SelectedIndexChanged += OnPageSizeChanged;
            this.btMoveFirst.Click += OnMoveFirst;
            this.btMoveLast.Click += OnMoveLast;
            this.btMovePre.Click += OnMovePre;
            this.btMoveNext.Click += OnMoveNext;
        }

        private void OnMoveNext(object sender, EventArgs e)
        {
            this._pageIndex += 1;
            this.LoadDataInternal(false);
        }

        private void OnMovePre(object sender, EventArgs e)
        {
            this._pageIndex -= 1;
            this.LoadDataInternal(false);
        }

        private void OnMoveLast(object sender, EventArgs e)
        {
            this._pageIndex = _totalPages - 1;
            this.LoadDataInternal(false);
        }

        private void OnMoveFirst(object sender, EventArgs e)
        {
            this._pageIndex = 0;
            this.LoadDataInternal(false);
        }

        private void OnPageSizeChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            switch (this.tbPageSize.SelectedIndex)
            {
                case 0:
                    this._pageSize = 10;
                    break;
                case 1:
                    this._pageSize = 20;
                    break;
                case 2:
                    this._pageSize = 50;
                    break;
                default:
                    break;
            }
        }

        public void LoadData()
        {
            LoadDataInternal(true);
        }

        private void LoadDataInternal(bool reload)
        {
            if (string.IsNullOrEmpty(_method))
                return;

            if (reload && _pageIndex >= 0)
                _pageIndex = 0;

            if (_getQueryArgsFunc != null)
            {
                var args = _getQueryArgsFunc();
                if (this._queryArgs == null)
                {
                    if (args == null || args.Length == 0)
                        this._queryArgs = new object[2];
                    else
                        this._queryArgs = new object[args.Length + 2];
                }
                if (this._queryArgs.Length > 2)
                {
                    for (int i = 2; i < this._queryArgs.Length; i++)
                    {
                        this._queryArgs[i] = args[i - 2];
                    }
                }
            }
            else
            {
                if (this._queryArgs == null)
                    this._queryArgs = new object[2];
            }
            this._queryArgs[0] = _pageSize;
            this._queryArgs[1] = _pageIndex;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var result = dps.Common.SysService.Invoke(_appID, _serviceID, _method, _queryArgs) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果非数据表类型");
                    //计算总页数
                    if (_pageIndex <= -1)
                    {
                        _totalPages = 1;
                    }
                    else
                    {
                        _totalPages = result.TotalRows / _pageSize;
                        if (result.TotalRows % _pageSize > 0)
                            _totalPages++;
                    }
                    //UI线程更新信息
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.UpdateNavigationState(result.TotalRows);
                        try
                        {
                            this.gvList.DataSource = result;
                        }
                        catch (Exception ex)
                        {
                            RadMessageBox.Show(this, "查询绑定结果出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void UpdateNavigationState(int totalRows)
        {
            this.tbTotalCount.Text = totalRows.ToString();
            if (_totalPages <= 1)
            {
                this.btMoveFirst.Enabled = false;
                this.btMovePre.Enabled = false;
                this.btMoveNext.Enabled = false;
                this.btMoveLast.Enabled = false;
            }
            else //多页
            {
                if (_pageIndex == 0)
                {
                    this.btMoveFirst.Enabled = false;
                    this.btMovePre.Enabled = false;
                    this.btMoveNext.Enabled = true;
                    this.btMoveLast.Enabled = true;
                }
                else if (_pageIndex == _totalPages - 1)
                {
                    this.btMoveFirst.Enabled = true;
                    this.btMovePre.Enabled = true;
                    this.btMoveNext.Enabled = false;
                    this.btMoveLast.Enabled = false;
                }
                else
                {
                    this.btMoveFirst.Enabled = true;
                    this.btMovePre.Enabled = true;
                    this.btMoveNext.Enabled = true;
                    this.btMoveLast.Enabled = true;
                }
            }
        }

        
    }
}
