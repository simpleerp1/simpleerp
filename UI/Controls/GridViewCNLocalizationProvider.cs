﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI.Localization;

namespace mzerp.UI.Controls
{
    public class GridViewCNLocalizationProvider : RadGridLocalizationProvider
    {
        public override string GetLocalizedString(string id)
        {
            switch (id)
            {
                case RadGridStringId.FilterFunctionBetween: return "Between";
                case RadGridStringId.FilterFunctionContains: return "Contains";
                case RadGridStringId.FilterFunctionDoesNotContain: return "Does not contain";
                case RadGridStringId.FilterFunctionEndsWith: return "Ends with";
                case RadGridStringId.FilterFunctionEqualTo: return "Equals";
                case RadGridStringId.FilterFunctionGreaterThan: return "Greater than";
                case RadGridStringId.FilterFunctionGreaterThanOrEqualTo: return "Greater than or equal to";
                case RadGridStringId.FilterFunctionIsEmpty: return "Is empty";
                case RadGridStringId.FilterFunctionIsNull: return "Is null";
                case RadGridStringId.FilterFunctionLessThan: return "Less than";
                case RadGridStringId.FilterFunctionLessThanOrEqualTo: return "Less than or equal to";
                case RadGridStringId.FilterFunctionNoFilter: return "No filter";
                case RadGridStringId.FilterFunctionNotBetween: return "Not between";
                case RadGridStringId.FilterFunctionNotEqualTo: return "Not equal to";
                case RadGridStringId.FilterFunctionNotIsEmpty: return "Is not empty";
                case RadGridStringId.FilterFunctionNotIsNull: return "Is not null";
                case RadGridStringId.FilterFunctionStartsWith: return "Starts with";
                case RadGridStringId.FilterFunctionCustom: return "Custom";

                case RadGridStringId.FilterOperatorBetween: return "Between";
                case RadGridStringId.FilterOperatorContains: return "Contains";
                case RadGridStringId.FilterOperatorDoesNotContain: return "NotContains";
                case RadGridStringId.FilterOperatorEndsWith: return "EndsWith";
                case RadGridStringId.FilterOperatorEqualTo: return "Equals";
                case RadGridStringId.FilterOperatorGreaterThan: return "GreaterThan";
                case RadGridStringId.FilterOperatorGreaterThanOrEqualTo: return "GreaterThanOrEquals";
                case RadGridStringId.FilterOperatorIsEmpty: return "IsEmpty";
                case RadGridStringId.FilterOperatorIsNull: return "IsNull";
                case RadGridStringId.FilterOperatorLessThan: return "LessThan";
                case RadGridStringId.FilterOperatorLessThanOrEqualTo: return "LessThanOrEquals";
                case RadGridStringId.FilterOperatorNoFilter: return "No filter";
                case RadGridStringId.FilterOperatorNotBetween: return "NotBetween";
                case RadGridStringId.FilterOperatorNotEqualTo: return "NotEquals";
                case RadGridStringId.FilterOperatorNotIsEmpty: return "NotEmpty";
                case RadGridStringId.FilterOperatorNotIsNull: return "NotNull";
                case RadGridStringId.FilterOperatorStartsWith: return "StartsWith";
                case RadGridStringId.FilterOperatorIsLike: return "Like";
                case RadGridStringId.FilterOperatorNotIsLike: return "NotLike";
                case RadGridStringId.FilterOperatorIsContainedIn: return "ContainedIn";
                case RadGridStringId.FilterOperatorNotIsContainedIn: return "NotContainedIn";
                case RadGridStringId.FilterOperatorCustom: return "Custom";

                case RadGridStringId.CustomFilterMenuItem: return "Custom";
                case RadGridStringId.CustomFilterDialogCaption: return "RadGridView Filter Dialog [{0}]";
                case RadGridStringId.CustomFilterDialogLabel: return "Show rows where:";
                case RadGridStringId.CustomFilterDialogRbAnd: return "And";
                case RadGridStringId.CustomFilterDialogRbOr: return "Or";
                case RadGridStringId.CustomFilterDialogBtnOk: return "OK";
                case RadGridStringId.CustomFilterDialogBtnCancel: return "Cancel";
                case RadGridStringId.CustomFilterDialogCheckBoxNot: return "Not";
                case RadGridStringId.CustomFilterDialogTrue: return "True";
                case RadGridStringId.CustomFilterDialogFalse: return "False";

                case RadGridStringId.FilterMenuAvailableFilters: return "Available Filters";
                case RadGridStringId.FilterMenuSearchBoxText: return "Search...";
                case RadGridStringId.FilterMenuClearFilters: return "Clear Filter";
                case RadGridStringId.FilterMenuButtonOK: return "OK";
                case RadGridStringId.FilterMenuButtonCancel: return "Cancel";
                case RadGridStringId.FilterMenuSelectionAll: return "All";
                case RadGridStringId.FilterMenuSelectionAllSearched: return "All Search Result";
                case RadGridStringId.FilterMenuSelectionNull: return "Null";
                case RadGridStringId.FilterMenuSelectionNotNull: return "Not Null";

                case RadGridStringId.FilterFunctionSelectedDates: return "Filter by specific dates:";
                case RadGridStringId.FilterFunctionToday: return "Today";
                case RadGridStringId.FilterFunctionYesterday: return "Yesterday";
                case RadGridStringId.FilterFunctionDuringLast7days: return "During last 7 days";

                case RadGridStringId.FilterLogicalOperatorAnd: return "AND";
                case RadGridStringId.FilterLogicalOperatorOr: return "OR";
                case RadGridStringId.FilterCompositeNotOperator: return "NOT";

                case RadGridStringId.DeleteRowMenuItem: return "删除行";
                case RadGridStringId.SortAscendingMenuItem: return "Sort Ascending";
                case RadGridStringId.SortDescendingMenuItem: return "Sort Descending";
                case RadGridStringId.ClearSortingMenuItem: return "Clear Sorting";
                case RadGridStringId.ConditionalFormattingMenuItem: return "Conditional Formatting";
                case RadGridStringId.GroupByThisColumnMenuItem: return "Group by this column";
                case RadGridStringId.UngroupThisColumn: return "Ungroup this column";
                case RadGridStringId.ColumnChooserMenuItem: return "Column Chooser";
                case RadGridStringId.HideMenuItem: return "隐藏列";
                case RadGridStringId.HideGroupMenuItem: return "隐藏分组";
                case RadGridStringId.UnpinMenuItem: return "不锁定列";
                case RadGridStringId.UnpinRowMenuItem: return "不锁定行";
                case RadGridStringId.PinMenuItem: return "锁定状态";
                case RadGridStringId.PinAtLeftMenuItem: return "左侧锁定";
                case RadGridStringId.PinAtRightMenuItem: return "右侧锁定";
                case RadGridStringId.PinAtBottomMenuItem: return "底部锁定";
                case RadGridStringId.PinAtTopMenuItem: return "顶部锁定";
                case RadGridStringId.BestFitMenuItem: return "适合大小";
                case RadGridStringId.PasteMenuItem: return "粘贴";
                case RadGridStringId.EditMenuItem: return "编辑";
                case RadGridStringId.ClearValueMenuItem: return "清除值";
                case RadGridStringId.CopyMenuItem: return "复制";
                case RadGridStringId.CutMenuItem: return "剪切";
                case RadGridStringId.AddNewRowString: return "点击此处添加新行";
                case RadGridStringId.ConditionalFormattingSortAlphabetically: return "Sort columns alphabetically";
                case RadGridStringId.ConditionalFormattingCaption: return "Conditional Formatting Rules Manager";
                case RadGridStringId.ConditionalFormattingLblColumn: return "Format only cells with";
                case RadGridStringId.ConditionalFormattingLblName: return "Rule name";
                case RadGridStringId.ConditionalFormattingLblType: return "Cell value";
                case RadGridStringId.ConditionalFormattingLblValue1: return "Value 1";
                case RadGridStringId.ConditionalFormattingLblValue2: return "Value 2";
                case RadGridStringId.ConditionalFormattingGrpConditions: return "Rules";
                case RadGridStringId.ConditionalFormattingGrpProperties: return "Rule Properties";
                case RadGridStringId.ConditionalFormattingChkApplyToRow: return "Apply this formatting to entire row";
                case RadGridStringId.ConditionalFormattingChkApplyOnSelectedRows: return "Apply this formatting if the row is selected";
                case RadGridStringId.ConditionalFormattingBtnAdd: return "Add new rule";
                case RadGridStringId.ConditionalFormattingBtnRemove: return "Remove";
                case RadGridStringId.ConditionalFormattingBtnOK: return "OK";
                case RadGridStringId.ConditionalFormattingBtnCancel: return "Cancel";
                case RadGridStringId.ConditionalFormattingBtnApply: return "Apply";
                case RadGridStringId.ConditionalFormattingRuleAppliesOn: return "Rule applies to";
                case RadGridStringId.ConditionalFormattingCondition: return "Condition";
                case RadGridStringId.ConditionalFormattingExpression: return "Expression";
                case RadGridStringId.ConditionalFormattingChooseOne: return "[Choose one]";
                case RadGridStringId.ConditionalFormattingEqualsTo: return "equals to [Value1]";
                case RadGridStringId.ConditionalFormattingIsNotEqualTo: return "is not equal to [Value1]";
                case RadGridStringId.ConditionalFormattingStartsWith: return "starts with [Value1]";
                case RadGridStringId.ConditionalFormattingEndsWith: return "ends with [Value1]";
                case RadGridStringId.ConditionalFormattingContains: return "contains [Value1]";
                case RadGridStringId.ConditionalFormattingDoesNotContain: return "does not contain [Value1]";
                case RadGridStringId.ConditionalFormattingIsGreaterThan: return "is greater than [Value1]";
                case RadGridStringId.ConditionalFormattingIsGreaterThanOrEqual: return "is greater than or equal [Value1]";
                case RadGridStringId.ConditionalFormattingIsLessThan: return "is less than [Value1]";
                case RadGridStringId.ConditionalFormattingIsLessThanOrEqual: return "is less than or equal to [Value1]";
                case RadGridStringId.ConditionalFormattingIsBetween: return "is between [Value1] and [Value2]";
                case RadGridStringId.ConditionalFormattingIsNotBetween: return "is not between [Value1] and [Value1]";
                case RadGridStringId.ConditionalFormattingLblFormat: return "Format";

                case RadGridStringId.ConditionalFormattingBtnExpression: return "Expression editor";
                case RadGridStringId.ConditionalFormattingTextBoxExpression: return "Expression";

                case RadGridStringId.ConditionalFormattingPropertyGridCaseSensitive: return "CaseSensitive";
                case RadGridStringId.ConditionalFormattingPropertyGridCellBackColor: return "CellBackColor";
                case RadGridStringId.ConditionalFormattingPropertyGridCellForeColor: return "CellForeColor";
                case RadGridStringId.ConditionalFormattingPropertyGridEnabled: return "Enabled";
                case RadGridStringId.ConditionalFormattingPropertyGridRowBackColor: return "RowBackColor";
                case RadGridStringId.ConditionalFormattingPropertyGridRowForeColor: return "RowForeColor";
                case RadGridStringId.ConditionalFormattingPropertyGridRowTextAlignment: return "RowTextAlignment";
                case RadGridStringId.ConditionalFormattingPropertyGridTextAlignment: return "TextAlignment";

                case RadGridStringId.ColumnChooserFormCaption: return "Column Chooser";
                case RadGridStringId.ColumnChooserFormMessage: return "Drag a column header from the\ngrid here to remove it from\nthe current view.";
                case RadGridStringId.GroupingPanelDefaultMessage: return "将列标题拖动到这里分组显示.";
                case RadGridStringId.GroupingPanelHeader: return "分组:";
                case RadGridStringId.NoDataText: return "No data to display";
                case RadGridStringId.CompositeFilterFormErrorCaption: return "Filter Error";
                case RadGridStringId.CompositeFilterFormInvalidFilter: return "The composite filter descriptor is not valid.";

                case RadGridStringId.ExpressionMenuItem: return "Expression";
                case RadGridStringId.ExpressionFormTitle: return "Expression Builder";
                case RadGridStringId.ExpressionFormFunctions: return "Functions";
                case RadGridStringId.ExpressionFormFunctionsText: return "Text";
                case RadGridStringId.ExpressionFormFunctionsAggregate: return "Aggregate";
                case RadGridStringId.ExpressionFormFunctionsDateTime: return "Date-Time";
                case RadGridStringId.ExpressionFormFunctionsLogical: return "Logical";
                case RadGridStringId.ExpressionFormFunctionsMath: return "Math";
                case RadGridStringId.ExpressionFormFunctionsOther: return "Other";
                case RadGridStringId.ExpressionFormOperators: return "Operators";
                case RadGridStringId.ExpressionFormConstants: return "Constants";
                case RadGridStringId.ExpressionFormFields: return "Fields";
                case RadGridStringId.ExpressionFormDescription: return "Description";
                case RadGridStringId.ExpressionFormResultPreview: return "Result preview";
                case RadGridStringId.ExpressionFormTooltipPlus: return "Plus";
                case RadGridStringId.ExpressionFormTooltipMinus: return "Minus";
                case RadGridStringId.ExpressionFormTooltipMultiply: return "Multiply";
                case RadGridStringId.ExpressionFormTooltipDivide: return "Divide";
                case RadGridStringId.ExpressionFormTooltipModulo: return "Modulo";
                case RadGridStringId.ExpressionFormTooltipEqual: return "Equal";
                case RadGridStringId.ExpressionFormTooltipNotEqual: return "Not Equal";
                case RadGridStringId.ExpressionFormTooltipLess: return "Less";
                case RadGridStringId.ExpressionFormTooltipLessOrEqual: return "Less Or Equal";
                case RadGridStringId.ExpressionFormTooltipGreaterOrEqual: return "Greater Or Equal";
                case RadGridStringId.ExpressionFormTooltipGreater: return "Greater";
                case RadGridStringId.ExpressionFormTooltipAnd: return "Logical \"AND\"";
                case RadGridStringId.ExpressionFormTooltipOr: return "Logical \"OR\"";
                case RadGridStringId.ExpressionFormTooltipNot: return "Logical \"NOT\"";
                case RadGridStringId.ExpressionFormAndButton: return string.Empty; //if empty, default button image is used
                case RadGridStringId.ExpressionFormOrButton: return string.Empty; //if empty, default button image is used
                case RadGridStringId.ExpressionFormNotButton: return string.Empty; //if empty, default button image is used
                case RadGridStringId.ExpressionFormOKButton: return "OK";
                case RadGridStringId.ExpressionFormCancelButton: return "Cancel";
            }

            return string.Empty;
        }
    }
}
