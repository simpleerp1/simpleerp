﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    class Module
    {

        public string Name { get; set; }

        private List<MenuItemGroup> _groups = new List<MenuItemGroup>();
        public List<MenuItemGroup> Groups
        {
            get { return _groups; }
        }

        public Module(string name)
        {
            this.Name = name;
        }
    }
}
