﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    class MenuItemGroup
    {

        public string Name { get; set; }

        private List<MenuItem> _items = new List<MenuItem>();
        public List<MenuItem> Items
        {
            get { return _items; }
        }

        public MenuItemGroup(string name)
        {
            this.Name = name;
        }

        public void AddButton(string name, string viewTitle, dps.Data.Mapper.Permission permission, Type viewType, System.Drawing.Bitmap image)
        {
            var mi = new MenuItem(MenuItemType.Button, name, viewTitle, permission, viewType, image);
            Items.Add(mi);
        }

        public MenuItem AddDropDown(string name,System.Drawing.Bitmap image)
        {
            var mi = new MenuItem(MenuItemType.DropDown, name, null, null, null, image);
            Items.Add(mi);
            return mi;
        }

    }
}
