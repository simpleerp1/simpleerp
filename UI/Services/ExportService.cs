﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;

namespace mzerp.UI
{
    class ExportService
    {

        /// <summary>
        /// 导出GridView至Excel
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="sheetName"></param>
        public static void ExportGridViewToExcel(RadGridView gridView, string sheetName)
        {
            var saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            saveFileDialog.Filter = "Excel (*.xls)|*.xls";
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
            { return; }
            if (saveFileDialog.FileName.Equals(String.Empty))
            {
                RadMessageBox.SetThemeName(gridView.ThemeName);
                RadMessageBox.Show("请输入正确的Excel文件名.");
                return;
            }
            string fileName = saveFileDialog.FileName;

            bool openExportFile = false;
            RunExportToExcelML(gridView, fileName, sheetName, ref openExportFile);

            if (openExportFile)
            {
                try
                { System.Diagnostics.Process.Start(fileName); }
                catch (Exception ex)
                {
                    string message = String.Format("不能打开导出的文件.\n错误信息: {0}", ex.Message);
                    RadMessageBox.Show(message, "打开导出文件", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private static void RunExportToExcelML(RadGridView gridView, string fileName, string sheetName, ref bool openExportFile)
        {
            ExportToExcelML excelExporter = new ExportToExcelML(gridView);
            excelExporter.SheetName = sheetName;
            excelExporter.SummariesExportOption = SummariesOption.ExportAll;
            //set max sheet rows            
            //excelExporter.SheetMaxRows = ExcelMaxRows._1048576; }
            excelExporter.SheetMaxRows = ExcelMaxRows._65536;
            //set exporting visual settings             
            excelExporter.ExportVisualSettings = true;
            try
            {
                excelExporter.RunExport(fileName);
                RadMessageBox.SetThemeName(gridView.ThemeName);
                DialogResult dr = RadMessageBox.Show("导出成功，是否需要立即打开文件？", "导出至Excel", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                if (dr == DialogResult.Yes)
                { openExportFile = true; }
            }
            catch (IOException ex)
            {
                RadMessageBox.SetThemeName(gridView.ThemeName);
                RadMessageBox.Show(ex.Message, "导出失败", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }



    }
}
