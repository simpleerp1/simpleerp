﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    static class SystemService
    {

        private static sys.Entities.Emploee _currentEmploee;
        public static sys.Entities.Emploee CurrentEmploee
        {
            get
            {
                if (dps.Client.dpsClient.Default.CurrentOrgUnit == null)
                    return null;

                if (_currentEmploee == null)
                {
                    var emp = dps.Client.dpsClient.Default.CurrentOrgUnit["Base"].EntityValue;
                    _currentEmploee = new sys.Entities.Emploee(emp);
                }
                return _currentEmploee;
            }
        }

        public static void ResetCurrentEmpolee()
        {
            _currentEmploee = null;
        }


    }
}
