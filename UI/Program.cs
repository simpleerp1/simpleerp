﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI.Localization;

namespace UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var loginForm = new mzerp.UI.LoginForm();
            var result = loginForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                loginForm.Dispose();

                //设置控件的本地化语言
                RadGridLocalizationProvider.CurrentProvider = new mzerp.UI.Controls.GridViewCNLocalizationProvider();
                
                mzerp.UI.MainForm.Instance = new mzerp.UI.MainForm();
                Application.Run(mzerp.UI.MainForm.Instance);
            }
                 
        }
    }
}
