﻿using mzerp.UI.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace mzerp.UI
{
    public partial class MainForm : Telerik.WinControls.UI.RadRibbonForm
    {

        private static MainForm _instance;
        public static MainForm Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        public MainForm()
        {
            InitializeComponent();

            this.CreateMenuItems();

            this.pvMain.ViewElement.ShowItemCloseButton = true;
            this.pvMain.PageRemoved += OnPageRemoved;
            this.ShowHomeView();

            this.tbCompany.Text = Global.Company;
            this.tbUser.Text = SystemService.CurrentEmploee.Base.Name;
        }

        #region View Actions
        private void OnPageRemoved(object sender, RadPageViewEventArgs e)
        {
            e.Page.Controls[0].Dispose();
            e.Page.Controls.Clear();
            e.Page.Dispose();
        }

        public void ShowHomeView()
        {
            this.ShowView(new HomeView(), "主页");
        }

        public void ShowView(Control view, string title)
        {
            var pv = new Telerik.WinControls.UI.RadPageViewPage(title);
            view.Dock = DockStyle.Fill;
            pv.Controls.Add(view);

            this.pvMain.Pages.Add(pv);
            this.pvMain.SelectedPage = pv;
        }

        private void ShowView(Type viewType, string title)
        {
            if (viewType == null)
                return;

            foreach (var item in this.pvMain.Pages)
            {
                if (item.Controls[0].GetType() == viewType)
                {
                    this.pvMain.SelectedPage = item;
                    return;
                }
            }

            var view = System.Activator.CreateInstance(viewType) as UserControl;
            if (view == null)
                return;

            ShowView(view, title);
        }

        public void CloseCurrentView()
        {
            this.pvMain.Pages.Remove(this.pvMain.SelectedPage);
        }
        #endregion

        #region MenuItems Methods
        private void CreateMenuItems()
        {
            List<Module> modules = new List<Module>();
            //库存管理
            var m = new Module("库存管理");
            modules.Add(m);
            var g = new MenuItemGroup("成品库");
            g.AddButton("成品入库", "成品入库管理", Permissions.ProductInstock_View, typeof(ProductInstockList), Resources.PackageAdd32);
            g.AddButton("成品出库", "成品出库管理", Permissions.ProductOutstock_View, typeof(ProductOutstockList), Resources.PackageDel32);
            g.AddButton("库存查询", "成品库存查询", Permissions.ProductStockQuery, typeof(ProductStockQuery), Resources.Search);
            //g.AddButton("客户汇总", "客户出入库汇总表", Permissions.ProductCustomerStockQuery, typeof(CustomerStockSummary), Resources.Search);
            var reports = g.AddDropDown("分析报表", Resources.ChartPie32);
            reports.AddMenuItem("成品总账表（月）", "成品总账表（月）", Permissions.Admin, typeof(ProductStockSummaryView), null);
            reports.AddMenuItem("成品库存变动分析", "成品库存变动分析", Permissions.Admin, typeof(ProductStockChangeView), null);
            reports.AddMenuItem("客户出入库汇总", "客户出入库汇总", Permissions.ProductCustomerStockQuery, typeof(CustomerStockSummary), null);
            reports.AddMenuItem("成品产出汇总（月）", "成品产出汇总（月）", Permissions.Admin, typeof(MakeInSummary), null);
            m.Groups.Add(g);
            g = new MenuItemGroup("原料库");
            g.AddButton("采购入库", "采购入库管理", Permissions.PurchaseInstock_View, typeof(PurchaseInstockList), Resources.PurchaseIn32);
            g.AddButton("退料入库", "退料入库管理", Permissions.MakeReturn_View, typeof(MakeReturnList), Resources.ToolsAdd32);
            g.AddButton("其他入库", "其他入库管理", Permissions.OtherInstock_View, typeof(OtherInstockList), Resources.OtherInstock);
            g.AddButton("采购退货", "采购退货管理", Permissions.PurchaseReturn_View, typeof(PurchaseReturnList), Resources.PurchaseOut32);
            g.AddButton("领料出库", "领料出库管理", Permissions.MakeOutstock_View, typeof(MakeOutstockList), Resources.ToolsRemove32);
            g.AddButton("其他出库", "其他出库管理", Permissions.OtherOutstock_View, typeof(OtherOutstockList), Resources.OtherOutstock);
            g.AddButton("库存查询", "物料库存查询", Permissions.MaterialStockQuery, typeof(MaterialStockQuery), Resources.Search);
            reports = g.AddDropDown("分析报表", Resources.ChartPie32);
            reports.AddMenuItem("存货总账表（月）", "存货总账表（月）", Permissions.Admin, typeof(MaterialStockSummaryView), null);
            m.Groups.Add(g);

            //成本核算
            m = new Module("存货核算");
            modules.Add(m);
            g = new MenuItemGroup("基础设置");
            g.AddButton("会计期间", "会计期间管理", Permissions.Admin, typeof(PeriodListView), Resources.DateLink32);
            g.AddButton("成本中心", "成本中心管理", null, typeof(CostCenterList), Resources.Project32);
            m.Groups.Add(g);
            g = new MenuItemGroup("成本差异");
            g.AddButton("采购暂估", "采购暂估管理", Permissions.Admin, null, Resources.CheckList32);
            g.AddButton("成本差异", "成本差异管理", Permissions.Admin, typeof(CostVarianceList), Resources.ChartLineEdit32);
            //g.AddButton("期末结算", "期末结算管理", Permissions.Admin, null, Resources.Calculator32);
            m.Groups.Add(g);
            g = new MenuItemGroup("分析报表");
            g.AddButton("物料消耗汇总", "物料消耗汇总", Permissions.Admin, typeof(MaterialConsumeSummaryView), Resources.ToolsRemove32);
            g.AddButton("物料消耗明细", "物料消耗明细", Permissions.Admin, typeof(MaterialConsumeDetailView), Resources.ToolsRemove32);
            m.Groups.Add(g);
            //质量管理
            m = new Module("质量管理");
            modules.Add(m);

            //基础数据
            m = new Module("基础数据");
            modules.Add(m);
            g = new MenuItemGroup("产品基础数据");
            g.AddButton("产品材料", "产品材料管理", Permissions.ProductMaterial_View, typeof(ProductMaterialList), Resources.material32);
            g.AddButton("产品规格", "产品规格管理", Permissions.ProductSpec_View, typeof(ProductSpecList), Resources.Measure32);
            g.AddButton("产品规值", "产品规值管理", Permissions.ProductSpec_View, typeof(ProductSpecValue1List), Resources.Spec132);
            g.AddButton("产品分规值", "产品分规值管理", Permissions.ProductSpec_View, typeof(ProductSpecValue2List), Resources.Spec232);
            g.AddButton("产品级别", "产品级别管理", Permissions.ProductLevel_View, typeof(ProductLevelList), Resources.level32);
            m.Groups.Add(g);
            g = new MenuItemGroup("业务基础数据");
            g.AddButton("业务单位", "业务单位管理", null, typeof(BizPartnerList), Resources.Customer32);
            m.Groups.Add(g);
            g = new MenuItemGroup("物料基础数据");
            g.AddButton("仓库管理", "仓库管理", null, typeof(WarehouseList), Resources.Warehouse32);
            g.AddButton("物料分类", "物料分类管理", null, typeof(MaterialCatalogList), Resources.Categories32);
            g.AddButton("物料管理", "物料管理", null, typeof(MaterialList), Resources.MaterialItem32);
            m.Groups.Add(g);

            m = new Module("系统管理");
            modules.Add(m);
            g = new MenuItemGroup("组织架构管理");
            g.AddButton("组织架构", "组织架构管理", Permissions.Admin, typeof(OrgUnitManager), Resources.Orgunit);
            m.Groups.Add(g);
            g = new MenuItemGroup("测试");
            g.AddButton("控件测试", "测试", Permissions.Admin, typeof(TestControls), Resources.Notepad);
            m.Groups.Add(g);

            //添加至MainMenu
            this.mainMenu.SuspendLayout();
            foreach (var module in modules)
            {
                RibbonTab tab = new RibbonTab(module.Name);
                foreach (var group in module.Groups)
                {
                    RadRibbonBarGroup bg = new RadRibbonBarGroup();
                    bg.Text = group.Name;
                    foreach (var menu in group.Items)
                    {
                        if (menu.Type == MenuItemType.Button)
                        {
                            RadButtonElement bt = new RadButtonElement(menu.Name, menu.Image);
                            bt.Enabled = menu.Permission == null ? true : menu.Permission.IsOwn;
                            bt.Click += (s, e) => { this.ShowView(menu.ViewType, menu.ViewTitle); };
                            bt.ImageAlignment = ContentAlignment.MiddleCenter;
                            bt.TextImageRelation = TextImageRelation.ImageAboveText;
                            bg.Items.Add(bt);
                        }
                        else if (menu.Type == MenuItemType.DropDown)
                        {
                            RadDropDownButtonElement bt = new RadDropDownButtonElement();
                            bt.Image = menu.Image;
                            bt.ImageAlignment = ContentAlignment.MiddleCenter;
                            bt.TextImageRelation = TextImageRelation.ImageAboveText;
                            bt.Text = menu.Name;
                            bg.Items.Add(bt);

                            foreach (var item in menu.Items) //Todo:暂只加一层
                            {
                                RadMenuItem si = new RadMenuItem(item.Name);
                                si.Enabled = item.Permission == null ? true : item.Permission.IsOwn;
                                si.Click += (s, e) => { this.ShowView(item.ViewType, item.ViewTitle); };
                                bt.Items.Add(si);
                            }
                        }
                    }
                    tab.Items.Add(bg);
                }

                this.mainMenu.CommandTabs.Add(tab);
            }
            this.mainMenu.ResumeLayout();
        }

        #endregion

    }
}
