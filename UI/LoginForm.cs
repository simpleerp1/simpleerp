﻿using dps.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace mzerp.UI
{
    public partial class LoginForm : Telerik.WinControls.UI.ShapedForm
    {
        public LoginForm()
        {
            InitializeComponent();

            this.lbCompany.Text = Global.Company;

            //设定本地缓存块
            dps.Common.Cache.ByteBuffer.Init(dps.Common.Messages.BufferHelper.BlockSize, 32);
            //设定通信方式
            IPEndPoint ep = new System.Net.IPEndPoint(
                IPAddress.Parse(Properties.Settings.Default.AppServerIP),
                Properties.Settings.Default.AppServerPort);
            //IPEndPoint[] eps = new IPEndPoint[] { ep };
            dpsClient.Default.UseTcpChannel(ep);

#if TEST
            this.tbUser.Text = "admin";
            this.tbPassword.Text = "dev9284";
#endif

            this.btLogin.Click += btLogin_Click;
            this.btCancel.Click += btCancel_Click;
        }

        void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        void btLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.tbUser.Text)
              || string.IsNullOrEmpty(this.tbPassword.Text))
            {
                RadMessageBox.Show(this,"请输入用户名及密码", "登录错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            
            //dpsClient.UseUdpChannel(ep, new IPEndPoint(IPAddress.Any, 27108), 50, 512);
            dpsClient.Default.DomainName = Properties.Settings.Default.Domain;
            dpsClient.Default.UserName = this.tbUser.Text;
            dpsClient.Default.PassWord = this.tbPassword.Text;
            try
            {
                dpsClient.Default.Login();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this,ex.Message, "登录错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
