﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.UI
{
    class MenuItem
    {

        public MenuItemType Type { get; set; }

        public string Name { get; set; }

        public dps.Data.Mapper.Permission Permission { get; set; }

        public Type ViewType { get; set; }

        public string ViewTitle { get; set; }

        public System.Drawing.Bitmap Image { get; set; }

        private List<MenuItem> _items;
        public List<MenuItem> Items
        {
            get
            {
                if (_items == null)
                    _items = new List<MenuItem>();
                return _items;
            }
        }

        public MenuItem(MenuItemType type, string name, string viewTitle, dps.Data.Mapper.Permission permission, Type viewType, System.Drawing.Bitmap image)
        {
            this.Type = type;
            this.Name = name;
            this.Permission = permission;
            this.ViewType = viewType;
            this.ViewTitle = viewTitle;
            this.Image = image;
        }

        public MenuItem AddMenuItem(string name, string viewTitle, dps.Data.Mapper.Permission permission, Type viewType, System.Drawing.Bitmap image)
        {
            var mi = new MenuItem(MenuItemType.MenuItem, name, viewTitle, permission, viewType, image);
            this.Items.Add(mi);
            return mi;
        }

    }


    enum MenuItemType
    {
        Button = 0,
        DropDown = 1,
        MenuItem =2
    }
}
