﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mzerp.Enums
{
    public enum CostVarianceSource
    {
        PurchaseEstimated = 0,
        PurchaseReturned = 1,
        OtherWarehouse = 2
    }
}
