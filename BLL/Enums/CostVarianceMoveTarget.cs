﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mzerp.Enums
{
    public enum CostVarianceMoveTarget
    {
        Stock = 0,
        CostCenter = 1,
        OtherWarehouse = 2
    }
}
