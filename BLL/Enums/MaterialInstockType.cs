﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mzerp.Enums
{
    public enum MaterialInstockType
    {
        Purchase = 0,
        MakeReturn = 1,
        SaleReturn = 2,
        MakeIn = 3,
        CostAdjustIn = 4,
        InitIn = 5,
        OtherIn=7
    }
}
