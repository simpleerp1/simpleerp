﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mzerp.Enums
{
    public enum InventoryValuationMethod
    {
        MonthlyAverage =0,
        MovingAverage=1,
        FIFO=2,
        SpecificIdentification=3
    }
}
