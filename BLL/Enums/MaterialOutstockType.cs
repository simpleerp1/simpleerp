﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mzerp.Enums
{
    public enum MaterialOutstockType
    {
        Make=0,
        PurchaseReturn=1,
        OtherOut=7
    }
}
