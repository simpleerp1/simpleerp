﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mzerp.Enums
{
    public enum ProductOutstockType
    { 
        Sale=0,
        Test=1,
        Compact=2,
        Returned=3,
        ChangePackage=4,
        Oddments=5,
        Other=6
    }
}
