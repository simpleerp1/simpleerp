﻿namespace mzerp.Enums
{
    public enum ProductInstockType
    {
        Manufacture = 0,
        Returned = 1
    }
}