﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dps.Data.Mapper;

namespace mzerp
{

    public static class Permissions
    {
        //库存管理--产品库存
        public static readonly Permission ProductInstock_View = new Permission("mzerp.ProductInstock_View");
        public static readonly Permission ProductInstock_Create = new Permission("mzerp.ProductInstock_Create");
        public static readonly Permission ProductInstock_Edit = new Permission("mzerp.ProductInstock_Edit");
        public static readonly Permission ProductInstock_Delete = new Permission("mzerp.ProductInstock_Delete");
        public static readonly Permission ProductOutstock_View = new Permission("mzerp.ProductOutstock_View");
        public static readonly Permission ProductOutstock_Create = new Permission("mzerp.ProductOutstock_Create");
        public static readonly Permission ProductOutstock_Edit = new Permission("mzerp.ProductOutstock_Edit");
        public static readonly Permission ProductOutstock_Delete = new Permission("mzerp.ProductOutstock_Delete");
        public static readonly Permission ProductStockQuery = new Permission("mzerp.ProductStockQuery");
        public static readonly Permission ProductCustomerStockQuery = new Permission("mzerp.ProductCustomerStockQuery");
        //库存管理--物料库存
        public static readonly Permission PurchaseInstock_View = new Permission("mzerp.PurchaseInstock_View");
        //public static readonly Permission ProductInstock_Create = new Permission("mzerp.ProductInstock_Create");
        public static readonly Permission PurchaseInstock_Edit = new Permission("mzerp.PurchaseInstock_Edit");
        public static readonly Permission MakeReturn_View = new Permission("mzerp.MakeReturn_View");
        public static readonly Permission MakeReturn_Edit = new Permission("mzerp.MakeReturn_Edit");
        public static readonly Permission OtherInstock_View = new Permission("mzerp.OtherInstock_View");
        public static readonly Permission OtherInstock_Edit = new Permission("mzerp.OtherInstock_Edit");
        public static readonly Permission PurchaseReturn_View = new Permission("mzerp.PurchaseReturn_View");
        public static readonly Permission PurchaseReturn_Edit = new Permission("mzerp.PurchaseReturn_Edit");
        public static readonly Permission MakeOutstock_View = new Permission("mzerp.MakeOutstock_View");
        public static readonly Permission MakeOutstock_Edit = new Permission("mzerp.MakeOutstock_Edit");
        public static readonly Permission OtherOutstock_View = new Permission("mzerp.OtherOutstock_View");
        public static readonly Permission OtherOutstock_Edit = new Permission("mzerp.OtherOutstock_Edit");
        public static readonly Permission MaterialStockQuery = new Permission("mzerp.MaterialStockQuery");

        //基础数据--产品基础数据
        public static readonly Permission ProductMaterial_View = new Permission("mzerp.ProductMaterial_View");
        public static readonly Permission ProductMaterial_Edit = new Permission("mzerp.ProductMaterial_Edit");
        public static readonly Permission ProductLevel_View = new Permission("mzerp.ProductLevel_View");
        public static readonly Permission ProductLevel_Edit = new Permission("mzerp.ProductLevel_Edit");
        public static readonly Permission ProductSpec_View = new Permission("mzerp.ProductSpec_View");
        public static readonly Permission ProductSpec_Edit = new Permission("mzerp.ProductSpec_Edit");
        //基础数据--业务基础数据
        public static readonly Permission Customer_View = new Permission("mzerp.Customer_View");
        public static readonly Permission Customer_Edit = new Permission("mzerp.Customer_Edit");
        public static readonly Permission Vendor_View = new Permission("mzerp.Vendor_View");
        public static readonly Permission Vendor_Edit = new Permission("mzerp.Vendor_Edit");

        //系统
        public static readonly Permission Admin = new Permission("sys.Admin");
    }

}
