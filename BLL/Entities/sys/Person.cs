﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace sys.Entities
{
    public class Person : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "sys.Person";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public sys.Enums.Gender Gender
        {
            get { return (sys.Enums.Gender)Instance["Gender"].IntegerValue; }
            set { Instance["Gender"].IntegerValue = (int)value; }
        }

        public DateTime? Birthday
        {
            get
            {
                if (Instance["Birthday"].HasValue)
                    return Instance["Birthday"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Birthday"].Value = null;
                else
                    Instance["Birthday"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Person() : base() { }

        public Person(Entity instance) : base(instance) { }
        #endregion

    }
}
