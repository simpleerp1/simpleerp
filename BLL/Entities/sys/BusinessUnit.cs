﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace sys.Entities
{
    public class BusinessUnit : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "sys.BusinessUnit";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String ShortName
        {
            get { return Instance["ShortName"].StringValue; }
            set { Instance["ShortName"].StringValue = value; }
        }

        public Boolean? IsCustomer
        {
            get
            {
                if (Instance["IsCustomer"].HasValue)
                    return Instance["IsCustomer"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsCustomer"].Value = null;
                else
                    Instance["IsCustomer"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsVendor
        {
            get
            {
                if (Instance["IsVendor"].HasValue)
                    return Instance["IsVendor"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsVendor"].Value = null;
                else
                    Instance["IsVendor"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsConsignee
        {
            get
            {
                if (Instance["IsConsignee"].HasValue)
                    return Instance["IsConsignee"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsConsignee"].Value = null;
                else
                    Instance["IsConsignee"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsInnerCompany
        {
            get
            {
                if (Instance["IsInnerCompany"].HasValue)
                    return Instance["IsInnerCompany"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsInnerCompany"].Value = null;
                else
                    Instance["IsInnerCompany"].BooleanValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public BusinessUnit() : base() { }

        public BusinessUnit(Entity instance) : base(instance) { }
        #endregion

    }
}
