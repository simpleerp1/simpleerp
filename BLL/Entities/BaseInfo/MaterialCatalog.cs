﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialCatalog : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialCatalog";

        #region ====Properties====
        public String Code
        {
            get { return Instance["Code"].StringValue; }
            set { Instance["Code"].StringValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialCatalog() : base() { }

        public MaterialCatalog(Entity instance) : base(instance) { }
        #endregion

    }
}
