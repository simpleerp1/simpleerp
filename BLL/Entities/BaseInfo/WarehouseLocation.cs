﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class WarehouseLocation : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.WarehouseLocation";

        #region ====Properties====
        private mzerp.Entities.Warehouse _Warehouse;
        public mzerp.Entities.Warehouse Warehouse
        {
            get
            {
                if (_Warehouse == null)
                {
                    Entity value = Instance["Warehouse"].EntityValue;
                    if (value == null)
                        return null;
                    _Warehouse = new mzerp.Entities.Warehouse(value);
                }
                return _Warehouse;
            }
            set
            {
                _Warehouse = value;
                if (value == null)
                    Instance["Warehouse"].EntityValue = null;
                else
                    Instance["Warehouse"].EntityValue = _Warehouse.Instance;
            }
        }

        public Guid WarehouseID
        {
            get { return Instance["WarehouseID"].GuidValue; }
            set { Instance["WarehouseID"].GuidValue = value; }
        }

        public String Code
        {
            get { return Instance["Code"].StringValue; }
            set { Instance["Code"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public WarehouseLocation() : base() { }

        public WarehouseLocation(Entity instance) : base(instance) { }
        #endregion

    }
}
