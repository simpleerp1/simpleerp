﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class ProductSpecValue1 : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductSpecValue1";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Int32 Value
        {
            get { return Instance["Value"].IntegerValue; }
            set { Instance["Value"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductSpecValue1() : base() { }

        public ProductSpecValue1(Entity instance) : base(instance) { }
        #endregion

    }
}
