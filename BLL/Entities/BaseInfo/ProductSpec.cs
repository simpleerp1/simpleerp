﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class ProductSpec : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductSpec";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Decimal BoxQuantity
        {
            get { return Instance["BoxQuantity"].DecimalValue; }
            set { Instance["BoxQuantity"].DecimalValue = value; }
        }

        public Decimal Value
        {
            get { return Instance["Value"].DecimalValue; }
            set { Instance["Value"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductSpec() : base() { }

        public ProductSpec(Entity instance) : base(instance) { }
        #endregion

    }
}
