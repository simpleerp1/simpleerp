﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class ProductSpecValue2 : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductSpecValue2";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Decimal Value
        {
            get { return Instance["Value"].DecimalValue; }
            set { Instance["Value"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductSpecValue2() : base() { }

        public ProductSpecValue2(Entity instance) : base(instance) { }
        #endregion

    }
}
