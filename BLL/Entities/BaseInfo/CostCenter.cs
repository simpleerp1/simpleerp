﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class CostCenter : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.CostCenter";

        #region ====Properties====
        public String Code
        {
            get { return Instance["Code"].StringValue; }
            set { Instance["Code"].StringValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public CostCenter() : base() { }

        public CostCenter(Entity instance) : base(instance) { }
        #endregion

    }
}
