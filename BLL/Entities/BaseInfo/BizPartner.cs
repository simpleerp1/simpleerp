﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class BizPartner : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "sys.BusinessUnit";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String LegalPerson
        {
            get { return Instance["LegalPerson"].StringValue; }
            set { Instance["LegalPerson"].StringValue = value; }
        }

        public bool IsCustomer
        {
            get { return Instance["IsCustomer"].BooleanValue; }
            set { Instance["IsCustomer"].BooleanValue = value; }
        }

        public bool IsVendor
        {
            get { return Instance["IsVendor"].BooleanValue; }
            set { Instance["IsVendor"].BooleanValue = value; }
        }

        public String Tel1
        {
            get { return Instance["Tel1"].StringValue; }
            set { Instance["Tel1"].StringValue = value; }
        }

        public String Fax
        {
            get { return Instance["Fax"].StringValue; }
            set { Instance["Fax"].StringValue = value; }
        }

        public String Address
        {
            get { return Instance["Address"].StringValue; }
            set { Instance["Address"].StringValue = value; }
        }

        public String Bank
        {
            get { return Instance["Bank"].StringValue; }
            set { Instance["Bank"].StringValue = value; }
        }

        public String BankAccount
        {
            get { return Instance["BankAccount"].StringValue; }
            set { Instance["BankAccount"].StringValue = value; }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public BizPartner() : base() {
        }

        public BizPartner(Entity instance) : base(instance) { }
        #endregion

    }
}
