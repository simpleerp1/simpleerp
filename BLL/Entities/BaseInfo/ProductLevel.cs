﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    [Serializable]
    public class ProductLevel : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductLevel";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductLevel() : base() { }

        public ProductLevel(Entity instance) : base(instance) { }
        #endregion

    }
}
