﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class Material : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.Material";

        #region ====Properties====
        public String Code
        {
            get { return Instance["Code"].StringValue; }
            set { Instance["Code"].StringValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String Spec
        {
            get { return Instance["Spec"].StringValue; }
            set { Instance["Spec"].StringValue = value; }
        }

        private mzerp.Entities.MaterialCatalog _Catalog;
        public mzerp.Entities.MaterialCatalog Catalog
        {
            get
            {
                if (_Catalog == null)
                {
                    Entity value = Instance["Catalog"].EntityValue;
                    if (value == null)
                        return null;
                    _Catalog = new mzerp.Entities.MaterialCatalog(value);
                }
                return _Catalog;
            }
            set
            {
                _Catalog = value;
                if (value == null)
                    Instance["Catalog"].EntityValue = null;
                else
                    Instance["Catalog"].EntityValue = _Catalog.Instance;
            }
        }

        public Guid CatalogID
        {
            get { return Instance["CatalogID"].GuidValue; }
            set { Instance["CatalogID"].GuidValue = value; }
        }

        public String MeasureUnit
        {
            get { return Instance["MeasureUnit"].StringValue; }
            set { Instance["MeasureUnit"].StringValue = value; }
        }

        public Boolean Enabled
        {
            get { return Instance["Enabled"].BooleanValue; }
            set { Instance["Enabled"].BooleanValue = value; }
        }

        public Decimal StandardUnitPrice
        {
            get { return Instance["StandardUnitPrice"].DecimalValue; }
            set { Instance["StandardUnitPrice"].DecimalValue = value; }
        }

        public mzerp.Enums.InventoryValuationMethod ValuationMethod
        {
            get { return (mzerp.Enums.InventoryValuationMethod)Instance["ValuationMethod"].IntegerValue; }
            set { Instance["ValuationMethod"].IntegerValue = (int)value; }
        }

        public Decimal LowerNotifyQuantity
        {
            get { return Instance["LowerNotifyQuantity"].DecimalValue; }
            set { Instance["LowerNotifyQuantity"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Material() : base() { }

        public Material(Entity instance) : base(instance) { }
        #endregion

    }
}
