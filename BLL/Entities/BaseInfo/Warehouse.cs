﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class Warehouse : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.Warehouse";

        #region ====Properties====
        public String Code
        {
            get { return Instance["Code"].StringValue; }
            set { Instance["Code"].StringValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        private mzerp.Entities.WarehouseLocation _DefaultLocation;
        public mzerp.Entities.WarehouseLocation DefaultLocation
        {
            get
            {
                if (_DefaultLocation == null)
                {
                    Entity value = Instance["DefaultLocation"].EntityValue;
                    if (value == null)
                        return null;
                    _DefaultLocation = new mzerp.Entities.WarehouseLocation(value);
                }
                return _DefaultLocation;
            }
            set
            {
                _DefaultLocation = value;
                if (value == null)
                    Instance["DefaultLocation"].EntityValue = null;
                else
                    Instance["DefaultLocation"].EntityValue = _DefaultLocation.Instance;
            }
        }

        public Guid DefaultLocationID
        {
            get { return Instance["DefaultLocationID"].GuidValue; }
            set { Instance["DefaultLocationID"].GuidValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Warehouse() : base() { }

        public Warehouse(Entity instance) : base(instance) { }
        #endregion

    }
}
