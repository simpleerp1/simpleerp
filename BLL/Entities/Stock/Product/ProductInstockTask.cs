﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class ProductInstockTask : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductInstockTask";

        #region ====Properties====
        public DateTime InstockDate
        {
            get { return Instance["InstockDate"].DateValue; }
            set { Instance["InstockDate"].DateValue = value; }
        }

        public String TaskNo
        {
            get { return Instance["TaskNo"].StringValue; }
        }

        private sys.Entities.Emploee _CreateBy;
        public sys.Entities.Emploee CreateBy
        {
            get
            {
                if (_CreateBy == null)
                {
                    Entity value = Instance["CreateBy"].EntityValue;
                    if (value == null)
                        return null;
                    _CreateBy = new sys.Entities.Emploee(value);
                }
                return _CreateBy;
            }
            set
            {
                _CreateBy = value;
                if (value == null)
                    Instance["CreateBy"].EntityValue = null;
                else
                    Instance["CreateBy"].EntityValue = _CreateBy.Instance;
            }
        }

        public Guid CreateByID
        {
            get { return Instance["CreateByID"].GuidValue; }
            set { Instance["CreateByID"].GuidValue = value; }
        }

        public DateTime CreateTime
        {
            get { return Instance["CreateTime"].DateValue; }
            set { Instance["CreateTime"].DateValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.ProductInstockTaskItem> _Items;
        public dps.Data.Mapper.EntityList<mzerp.Entities.ProductInstockTaskItem> Items
        {
            get
            {
                if (_Items == null)
                    _Items = new dps.Data.Mapper.EntityList<mzerp.Entities.ProductInstockTaskItem>(Instance["Items"].EntityListValue);
                return _Items;
            }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        public mzerp.Enums.ProductInstockType TaskType
        {
            get { return (mzerp.Enums.ProductInstockType)Instance["TaskType"].IntegerValue; }
            set { Instance["TaskType"].IntegerValue = (int)value; }
        }

        private mzerp.Entities.BizPartner _Customer;
        public mzerp.Entities.BizPartner Customer
        {
            get
            {
                if (_Customer == null)
                {
                    Entity value = Instance["Customer"].EntityValue;
                    if (value == null)
                        return null;
                    _Customer = new mzerp.Entities.BizPartner(value);
                }
                return _Customer;
            }
            set
            {
                _Customer = value;
                if (value == null)
                    Instance["Customer"].EntityValue = null;
                else
                    Instance["Customer"].EntityValue = _Customer.Instance;
            }
        }

        public Guid? CustomerID
        {
            get
            {
                if (Instance["CustomerID"].HasValue)
                    return Instance["CustomerID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["CustomerID"].Value = null;
                else
                    Instance["CustomerID"].GuidValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductInstockTask() : base() { }

        public ProductInstockTask(Entity instance) : base(instance) { }
        #endregion

    }
}
