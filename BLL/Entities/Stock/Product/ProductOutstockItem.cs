﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public partial class ProductOutstockItem : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductOutstockItem";

        #region ====Properties====
        private mzerp.Entities.ProductOutstockTask _OutstockTask;
        public mzerp.Entities.ProductOutstockTask OutstockTask
        {
            get
            {
                if (_OutstockTask == null)
                {
                    Entity value = Instance["OutstockTask"].EntityValue;
                    if (value == null)
                        return null;
                    _OutstockTask = new mzerp.Entities.ProductOutstockTask(value);
                }
                return _OutstockTask;
            }
            set
            {
                _OutstockTask = value;
                if (value == null)
                    Instance["OutstockTask"].EntityValue = null;
                else
                    Instance["OutstockTask"].EntityValue = _OutstockTask.Instance;
            }
        }

        public Guid OutstockTaskID
        {
            get { return Instance["OutstockTaskID"].GuidValue; }
            set { Instance["OutstockTaskID"].GuidValue = value; }
        }

        private mzerp.Entities.ProductMaterial _Material;
        public mzerp.Entities.ProductMaterial Material
        {
            get
            {
                if (_Material == null)
                {
                    Entity value = Instance["Material"].EntityValue;
                    if (value == null)
                        return null;
                    _Material = new mzerp.Entities.ProductMaterial(value);
                }
                return _Material;
            }
            set
            {
                _Material = value;
                if (value == null)
                    Instance["Material"].EntityValue = null;
                else
                    Instance["Material"].EntityValue = _Material.Instance;
            }
        }

        public Guid MaterialID
        {
            get { return Instance["MaterialID"].GuidValue; }
            set { Instance["MaterialID"].GuidValue = value; }
        }

        public String BatchNo
        {
            get { return Instance["BatchNo"].StringValue; }
            set { Instance["BatchNo"].StringValue = value; }
        }

        private mzerp.Entities.ProductLevel _Level;
        public mzerp.Entities.ProductLevel Level
        {
            get
            {
                if (_Level == null)
                {
                    Entity value = Instance["Level"].EntityValue;
                    if (value == null)
                        return null;
                    _Level = new mzerp.Entities.ProductLevel(value);
                }
                return _Level;
            }
            set
            {
                _Level = value;
                if (value == null)
                    Instance["Level"].EntityValue = null;
                else
                    Instance["Level"].EntityValue = _Level.Instance;
            }
        }

        public Guid LevelID
        {
            get { return Instance["LevelID"].GuidValue; }
            set { Instance["LevelID"].GuidValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        private mzerp.Entities.ProductSpec _Spec;
        public mzerp.Entities.ProductSpec Spec
        {
            get
            {
                if (_Spec == null)
                {
                    Entity value = Instance["Spec"].EntityValue;
                    if (value == null)
                        return null;
                    _Spec = new mzerp.Entities.ProductSpec(value);
                }
                return _Spec;
            }
            set
            {
                _Spec = value;
                if (value == null)
                    Instance["Spec"].EntityValue = null;
                else
                    Instance["Spec"].EntityValue = _Spec.Instance;
            }
        }

        public Guid SpecID
        {
            get { return Instance["SpecID"].GuidValue; }
            set { Instance["SpecID"].GuidValue = value; }
        }

        private mzerp.Entities.ProductSpecValue1 _SpecValue1;
        public mzerp.Entities.ProductSpecValue1 SpecValue1
        {
            get
            {
                if (_SpecValue1 == null)
                {
                    Entity value = Instance["SpecValue1"].EntityValue;
                    if (value == null)
                        return null;
                    _SpecValue1 = new mzerp.Entities.ProductSpecValue1(value);
                }
                return _SpecValue1;
            }
            set
            {
                _SpecValue1 = value;
                if (value == null)
                    Instance["SpecValue1"].EntityValue = null;
                else
                    Instance["SpecValue1"].EntityValue = _SpecValue1.Instance;
            }
        }

        public Guid SpecValue1ID
        {
            get { return Instance["SpecValue1ID"].GuidValue; }
            set { Instance["SpecValue1ID"].GuidValue = value; }
        }

        private mzerp.Entities.ProductSpecValue2 _SpecValue2;
        public mzerp.Entities.ProductSpecValue2 SpecValue2
        {
            get
            {
                if (_SpecValue2 == null)
                {
                    Entity value = Instance["SpecValue2"].EntityValue;
                    if (value == null)
                        return null;
                    _SpecValue2 = new mzerp.Entities.ProductSpecValue2(value);
                }
                return _SpecValue2;
            }
            set
            {
                _SpecValue2 = value;
                if (value == null)
                    Instance["SpecValue2"].EntityValue = null;
                else
                    Instance["SpecValue2"].EntityValue = _SpecValue2.Instance;
            }
        }

        public Guid SpecValue2ID
        {
            get { return Instance["SpecValue2ID"].GuidValue; }
            set { Instance["SpecValue2ID"].GuidValue = value; }
        }

        public Int32 Boxes
        {
            get { return Instance["Boxes"].IntegerValue; }
            set { Instance["Boxes"].IntegerValue = value; }
        }

        public Decimal Change
        {
            get { return Instance["Change"].DecimalValue; }
            set { Instance["Change"].DecimalValue = value; }
        }

        public Decimal QuantityOfBox
        {
            get { return Instance["QuantityOfBox"].DecimalValue; }
            set { Instance["QuantityOfBox"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductOutstockItem() : base() { }

        public ProductOutstockItem(Entity instance) : base(instance) { }
        #endregion

    }
}
