﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class ProductOutstockTask : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.ProductOutstockTask";

        #region ====Properties====
        public DateTime OutstockDate
        {
            get { return Instance["OutstockDate"].DateValue; }
            set { Instance["OutstockDate"].DateValue = value; }
        }

        public String TaskNo
        {
            get { return Instance["TaskNo"].StringValue; }
        }

        private sys.Entities.Emploee _CreateBy;
        public sys.Entities.Emploee CreateBy
        {
            get
            {
                if (_CreateBy == null)
                {
                    Entity value = Instance["CreateBy"].EntityValue;
                    if (value == null)
                        return null;
                    _CreateBy = new sys.Entities.Emploee(value);
                }
                return _CreateBy;
            }
            set
            {
                _CreateBy = value;
                if (value == null)
                    Instance["CreateBy"].EntityValue = null;
                else
                    Instance["CreateBy"].EntityValue = _CreateBy.Instance;
            }
        }

        public Guid CreateByID
        {
            get { return Instance["CreateByID"].GuidValue; }
            set { Instance["CreateByID"].GuidValue = value; }
        }

        public DateTime CreateTime
        {
            get { return Instance["CreateTime"].DateValue; }
            set { Instance["CreateTime"].DateValue = value; }
        }

        private mzerp.Entities.BizPartner _Customer;
        public mzerp.Entities.BizPartner Customer
        {
            get
            {
                if (_Customer == null)
                {
                    Entity value = Instance["Customer"].EntityValue;
                    if (value == null)
                        return null;
                    _Customer = new mzerp.Entities.BizPartner(value);
                }
                return _Customer;
            }
            set
            {
                _Customer = value;
                if (value == null)
                    Instance["Customer"].EntityValue = null;
                else
                    Instance["Customer"].EntityValue = _Customer.Instance;
            }
        }

        public Guid? CustomerID
        {
            get
            {
                if (Instance["CustomerID"].HasValue)
                    return Instance["CustomerID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["CustomerID"].Value = null;
                else
                    Instance["CustomerID"].GuidValue = value.Value;
            }
        }

        public mzerp.Enums.ProductOutstockType TaskType
        {
            get { return (mzerp.Enums.ProductOutstockType)Instance["TaskType"].IntegerValue; }
            set { Instance["TaskType"].IntegerValue = (int)value; }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.ProductOutstockItem> _Items;
        public dps.Data.Mapper.EntityList<mzerp.Entities.ProductOutstockItem> Items
        {
            get
            {
                if (_Items == null)
                    _Items = new dps.Data.Mapper.EntityList<mzerp.Entities.ProductOutstockItem>(Instance["Items"].EntityListValue);
                return _Items;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ProductOutstockTask() : base() { }

        public ProductOutstockTask(Entity instance) : base(instance) { }
        #endregion

    }
}
