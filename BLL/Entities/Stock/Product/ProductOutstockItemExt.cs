﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mzerp.Entities
{

    public partial class ProductOutstockItem
    {

        /// <summary>
        /// 总箱数，零头数大于0算一箱
        /// </summary>
        public int TotalBoxes
        {
            get
            {
                if (this.Change > 0)
                    return this.Boxes + 1;
                else
                    return this.Boxes;
            }
        }

    }

}
