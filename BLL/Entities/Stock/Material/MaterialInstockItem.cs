﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialInstockItem : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialInstockItem";

        #region ====Properties====
        private mzerp.Entities.MaterialInstockTask _InstockTask;
        public mzerp.Entities.MaterialInstockTask InstockTask
        {
            get
            {
                if (_InstockTask == null)
                {
                    Entity value = Instance["InstockTask"].EntityValue;
                    if (value == null)
                        return null;
                    _InstockTask = new mzerp.Entities.MaterialInstockTask(value);
                }
                return _InstockTask;
            }
            set
            {
                _InstockTask = value;
                if (value == null)
                    Instance["InstockTask"].EntityValue = null;
                else
                    Instance["InstockTask"].EntityValue = _InstockTask.Instance;
            }
        }

        public Guid InstockTaskID
        {
            get { return Instance["InstockTaskID"].GuidValue; }
            set { Instance["InstockTaskID"].GuidValue = value; }
        }

        private mzerp.Entities.Material _Material;
        public mzerp.Entities.Material Material
        {
            get
            {
                if (_Material == null)
                {
                    Entity value = Instance["Material"].EntityValue;
                    if (value == null)
                        return null;
                    _Material = new mzerp.Entities.Material(value);
                }
                return _Material;
            }
            set
            {
                _Material = value;
                if (value == null)
                    Instance["Material"].EntityValue = null;
                else
                    Instance["Material"].EntityValue = _Material.Instance;
            }
        }

        public Guid MaterialID
        {
            get { return Instance["MaterialID"].GuidValue; }
            set { Instance["MaterialID"].GuidValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public Decimal ReturnedQuantity
        {
            get { return Instance["ReturnedQuantity"].DecimalValue; }
            set { Instance["ReturnedQuantity"].DecimalValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.MaterialInstockReturnedItem> _ReturnedItems;
        public dps.Data.Mapper.EntityList<mzerp.Entities.MaterialInstockReturnedItem> ReturnedItems
        {
            get
            {
                if (_ReturnedItems == null)
                    _ReturnedItems = new dps.Data.Mapper.EntityList<mzerp.Entities.MaterialInstockReturnedItem>(Instance["ReturnedItems"].EntityListValue);
                return _ReturnedItems;
            }
        }

        public Decimal TotalPrice
        {
            get { return Instance["TotalPrice"].DecimalValue; }
            set { Instance["TotalPrice"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialInstockItem() : base() { }

        public MaterialInstockItem(Entity instance) : base(instance) { }
        #endregion

    }
}
