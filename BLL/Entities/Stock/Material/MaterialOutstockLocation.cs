﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialOutstockLocation : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialOutstockLocation";

        #region ====Properties====
        private mzerp.Entities.MaterialOutstockItem _OutstockItem;
        public mzerp.Entities.MaterialOutstockItem OutstockItem
        {
            get
            {
                if (_OutstockItem == null)
                {
                    Entity value = Instance["OutstockItem"].EntityValue;
                    if (value == null)
                        return null;
                    _OutstockItem = new mzerp.Entities.MaterialOutstockItem(value);
                }
                return _OutstockItem;
            }
            set
            {
                _OutstockItem = value;
                if (value == null)
                    Instance["OutstockItem"].EntityValue = null;
                else
                    Instance["OutstockItem"].EntityValue = _OutstockItem.Instance;
            }
        }

        public Guid OutstockItemID
        {
            get { return Instance["OutstockItemID"].GuidValue; }
            set { Instance["OutstockItemID"].GuidValue = value; }
        }

        private mzerp.Entities.WarehouseLocation _Location;
        public mzerp.Entities.WarehouseLocation Location
        {
            get
            {
                if (_Location == null)
                {
                    Entity value = Instance["Location"].EntityValue;
                    if (value == null)
                        return null;
                    _Location = new mzerp.Entities.WarehouseLocation(value);
                }
                return _Location;
            }
            set
            {
                _Location = value;
                if (value == null)
                    Instance["Location"].EntityValue = null;
                else
                    Instance["Location"].EntityValue = _Location.Instance;
            }
        }

        public Guid LocationID
        {
            get { return Instance["LocationID"].GuidValue; }
            set { Instance["LocationID"].GuidValue = value; }
        }

        public String LotNo
        {
            get { return Instance["LotNo"].StringValue; }
            set { Instance["LotNo"].StringValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public Decimal? Value
        {
            get
            {
                if (Instance["Value"].HasValue)
                    return Instance["Value"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Value"].Value = null;
                else
                    Instance["Value"].DecimalValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialOutstockLocation() : base() { }

        public MaterialOutstockLocation(Entity instance) : base(instance) { }
        #endregion

    }
}
