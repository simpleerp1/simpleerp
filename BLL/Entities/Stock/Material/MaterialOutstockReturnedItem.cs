﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialOutstockReturnedItem : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialOutstockReturnedItem";

        #region ====Properties====
        private mzerp.Entities.MaterialOutstockItem _OutstockItem;
        public mzerp.Entities.MaterialOutstockItem OutstockItem
        {
            get
            {
                if (_OutstockItem == null)
                {
                    Entity value = Instance["OutstockItem"].EntityValue;
                    if (value == null)
                        return null;
                    _OutstockItem = new mzerp.Entities.MaterialOutstockItem(value);
                }
                return _OutstockItem;
            }
            set
            {
                _OutstockItem = value;
                if (value == null)
                    Instance["OutstockItem"].EntityValue = null;
                else
                    Instance["OutstockItem"].EntityValue = _OutstockItem.Instance;
            }
        }

        public Guid OutstockItemID
        {
            get { return Instance["OutstockItemID"].GuidValue; }
            set { Instance["OutstockItemID"].GuidValue = value; }
        }

        private mzerp.Entities.MaterialInstockItem _InstockItem;
        public mzerp.Entities.MaterialInstockItem InstockItem
        {
            get
            {
                if (_InstockItem == null)
                {
                    Entity value = Instance["InstockItem"].EntityValue;
                    if (value == null)
                        return null;
                    _InstockItem = new mzerp.Entities.MaterialInstockItem(value);
                }
                return _InstockItem;
            }
            set
            {
                _InstockItem = value;
                if (value == null)
                    Instance["InstockItem"].EntityValue = null;
                else
                    Instance["InstockItem"].EntityValue = _InstockItem.Instance;
            }
        }

        public Guid InstockItemID
        {
            get { return Instance["InstockItemID"].GuidValue; }
            set { Instance["InstockItemID"].GuidValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialOutstockReturnedItem() : base() { }

        public MaterialOutstockReturnedItem(Entity instance) : base(instance) { }
        #endregion

    }
}
