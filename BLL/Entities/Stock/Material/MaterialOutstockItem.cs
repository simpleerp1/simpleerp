﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialOutstockItem : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialOutstockItem";

        #region ====Properties====
        private mzerp.Entities.MaterialOutstockTask _OutstockTask;
        public mzerp.Entities.MaterialOutstockTask OutstockTask
        {
            get
            {
                if (_OutstockTask == null)
                {
                    Entity value = Instance["OutstockTask"].EntityValue;
                    if (value == null)
                        return null;
                    _OutstockTask = new mzerp.Entities.MaterialOutstockTask(value);
                }
                return _OutstockTask;
            }
            set
            {
                _OutstockTask = value;
                if (value == null)
                    Instance["OutstockTask"].EntityValue = null;
                else
                    Instance["OutstockTask"].EntityValue = _OutstockTask.Instance;
            }
        }

        public Guid OutstockTaskID
        {
            get { return Instance["OutstockTaskID"].GuidValue; }
            set { Instance["OutstockTaskID"].GuidValue = value; }
        }

        private mzerp.Entities.Material _Material;
        public mzerp.Entities.Material Material
        {
            get
            {
                if (_Material == null)
                {
                    Entity value = Instance["Material"].EntityValue;
                    if (value == null)
                        return null;
                    _Material = new mzerp.Entities.Material(value);
                }
                return _Material;
            }
            set
            {
                _Material = value;
                if (value == null)
                    Instance["Material"].EntityValue = null;
                else
                    Instance["Material"].EntityValue = _Material.Instance;
            }
        }

        public Guid MaterialID
        {
            get { return Instance["MaterialID"].GuidValue; }
            set { Instance["MaterialID"].GuidValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public Decimal ReturnedQuantity
        {
            get { return Instance["ReturnedQuantity"].DecimalValue; }
            set { Instance["ReturnedQuantity"].DecimalValue = value; }
        }

        public Decimal TotalPrice
        {
            get { return Instance["TotalPrice"].DecimalValue; }
            set { Instance["TotalPrice"].DecimalValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockReturnedItem> _ReturnedItems;
        public dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockReturnedItem> ReturnedItems
        {
            get
            {
                if (_ReturnedItems == null)
                    _ReturnedItems = new dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockReturnedItem>(Instance["ReturnedItems"].EntityListValue);
                return _ReturnedItems;
            }
        }

        public Decimal? ReturnedTotalPrice
        {
            get
            {
                if (Instance["ReturnedTotalPrice"].HasValue)
                    return Instance["ReturnedTotalPrice"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ReturnedTotalPrice"].Value = null;
                else
                    Instance["ReturnedTotalPrice"].DecimalValue = value.Value;
            }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockLocation> _Locations;
        public dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockLocation> Locations
        {
            get
            {
                if (_Locations == null)
                    _Locations = new dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockLocation>(Instance["Locations"].EntityListValue);
                return _Locations;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialOutstockItem() : base() { }

        public MaterialOutstockItem(Entity instance) : base(instance) { }
        #endregion

    }
}
