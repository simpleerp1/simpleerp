﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialInstockTask : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialInstockTask";

        #region ====Properties====
        public String TaskNo
        {
            get { return Instance["TaskNo"].StringValue; }
        }

        public DateTime InstockDate
        {
            get { return Instance["InstockDate"].DateValue; }
            set { Instance["InstockDate"].DateValue = value; }
        }

        private sys.Entities.Emploee _PurchaseBy;
        public sys.Entities.Emploee PurchaseBy
        {
            get
            {
                if (_PurchaseBy == null)
                {
                    Entity value = Instance["PurchaseBy"].EntityValue;
                    if (value == null)
                        return null;
                    _PurchaseBy = new sys.Entities.Emploee(value);
                }
                return _PurchaseBy;
            }
            set
            {
                _PurchaseBy = value;
                if (value == null)
                    Instance["PurchaseBy"].EntityValue = null;
                else
                    Instance["PurchaseBy"].EntityValue = _PurchaseBy.Instance;
            }
        }

        public Guid? PurchaseByID
        {
            get
            {
                if (Instance["PurchaseByID"].HasValue)
                    return Instance["PurchaseByID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["PurchaseByID"].Value = null;
                else
                    Instance["PurchaseByID"].GuidValue = value.Value;
            }
        }

        private mzerp.Entities.BizPartner _BizPartner;
        public mzerp.Entities.BizPartner BizPartner
        {
            get
            {
                if (_BizPartner == null)
                {
                    Entity value = Instance["BizPartner"].EntityValue;
                    if (value == null)
                        return null;
                    _BizPartner = new mzerp.Entities.BizPartner(value);
                }
                return _BizPartner;
            }
            set
            {
                _BizPartner = value;
                if (value == null)
                    Instance["BizPartner"].EntityValue = null;
                else
                    Instance["BizPartner"].EntityValue = _BizPartner.Instance;
            }
        }

        public Guid? BizPartnerID
        {
            get
            {
                if (Instance["BizPartnerID"].HasValue)
                    return Instance["BizPartnerID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["BizPartnerID"].Value = null;
                else
                    Instance["BizPartnerID"].GuidValue = value.Value;
            }
        }

        private mzerp.Entities.Warehouse _Warehouse;
        public mzerp.Entities.Warehouse Warehouse
        {
            get
            {
                if (_Warehouse == null)
                {
                    Entity value = Instance["Warehouse"].EntityValue;
                    if (value == null)
                        return null;
                    _Warehouse = new mzerp.Entities.Warehouse(value);
                }
                return _Warehouse;
            }
            set
            {
                _Warehouse = value;
                if (value == null)
                    Instance["Warehouse"].EntityValue = null;
                else
                    Instance["Warehouse"].EntityValue = _Warehouse.Instance;
            }
        }

        public Guid WarehouseID
        {
            get { return Instance["WarehouseID"].GuidValue; }
            set { Instance["WarehouseID"].GuidValue = value; }
        }

        private mzerp.Entities.CostCenter _CostCenter;
        public mzerp.Entities.CostCenter CostCenter
        {
            get
            {
                if (_CostCenter == null)
                {
                    Entity value = Instance["CostCenter"].EntityValue;
                    if (value == null)
                        return null;
                    _CostCenter = new mzerp.Entities.CostCenter(value);
                }
                return _CostCenter;
            }
            set
            {
                _CostCenter = value;
                if (value == null)
                    Instance["CostCenter"].EntityValue = null;
                else
                    Instance["CostCenter"].EntityValue = _CostCenter.Instance;
            }
        }

        public Guid? CostCenterID
        {
            get
            {
                if (Instance["CostCenterID"].HasValue)
                    return Instance["CostCenterID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["CostCenterID"].Value = null;
                else
                    Instance["CostCenterID"].GuidValue = value.Value;
            }
        }

        public mzerp.Enums.MaterialInstockType InstockType
        {
            get { return (mzerp.Enums.MaterialInstockType)Instance["InstockType"].IntegerValue; }
            set { Instance["InstockType"].IntegerValue = (int)value; }
        }

        public String RefTaskNo
        {
            get { return Instance["RefTaskNo"].StringValue; }
            set { Instance["RefTaskNo"].StringValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.MaterialInstockItem> _Items;
        public dps.Data.Mapper.EntityList<mzerp.Entities.MaterialInstockItem> Items
        {
            get
            {
                if (_Items == null)
                    _Items = new dps.Data.Mapper.EntityList<mzerp.Entities.MaterialInstockItem>(Instance["Items"].EntityListValue);
                return _Items;
            }
        }

        private sys.Entities.Emploee _CreateBy;
        public sys.Entities.Emploee CreateBy
        {
            get
            {
                if (_CreateBy == null)
                {
                    Entity value = Instance["CreateBy"].EntityValue;
                    if (value == null)
                        return null;
                    _CreateBy = new sys.Entities.Emploee(value);
                }
                return _CreateBy;
            }
            set
            {
                _CreateBy = value;
                if (value == null)
                    Instance["CreateBy"].EntityValue = null;
                else
                    Instance["CreateBy"].EntityValue = _CreateBy.Instance;
            }
        }

        public Guid CreateByID
        {
            get { return Instance["CreateByID"].GuidValue; }
            set { Instance["CreateByID"].GuidValue = value; }
        }

        public DateTime CreateTime
        {
            get { return Instance["CreateTime"].DateValue; }
            set { Instance["CreateTime"].DateValue = value; }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialInstockTask() : base() { }

        public MaterialInstockTask(Entity instance) : base(instance) { }
        #endregion

    }
}
