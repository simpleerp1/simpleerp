﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class MaterialOutstockTask : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.MaterialOutstockTask";

        #region ====Properties====
        public String TaskNo
        {
            get { return Instance["TaskNo"].StringValue; }
        }

        public DateTime OutstockDate
        {
            get { return Instance["OutstockDate"].DateValue; }
            set { Instance["OutstockDate"].DateValue = value; }
        }

        private mzerp.Entities.Warehouse _Warehouse;
        public mzerp.Entities.Warehouse Warehouse
        {
            get
            {
                if (_Warehouse == null)
                {
                    Entity value = Instance["Warehouse"].EntityValue;
                    if (value == null)
                        return null;
                    _Warehouse = new mzerp.Entities.Warehouse(value);
                }
                return _Warehouse;
            }
            set
            {
                _Warehouse = value;
                if (value == null)
                    Instance["Warehouse"].EntityValue = null;
                else
                    Instance["Warehouse"].EntityValue = _Warehouse.Instance;
            }
        }

        public Guid WarehouseID
        {
            get { return Instance["WarehouseID"].GuidValue; }
            set { Instance["WarehouseID"].GuidValue = value; }
        }

        private mzerp.Entities.CostCenter _CostCenter;
        public mzerp.Entities.CostCenter CostCenter
        {
            get
            {
                if (_CostCenter == null)
                {
                    Entity value = Instance["CostCenter"].EntityValue;
                    if (value == null)
                        return null;
                    _CostCenter = new mzerp.Entities.CostCenter(value);
                }
                return _CostCenter;
            }
            set
            {
                _CostCenter = value;
                if (value == null)
                    Instance["CostCenter"].EntityValue = null;
                else
                    Instance["CostCenter"].EntityValue = _CostCenter.Instance;
            }
        }

        public Guid? CostCenterID
        {
            get
            {
                if (Instance["CostCenterID"].HasValue)
                    return Instance["CostCenterID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["CostCenterID"].Value = null;
                else
                    Instance["CostCenterID"].GuidValue = value.Value;
            }
        }

        public mzerp.Enums.MaterialOutstockType OutstockType
        {
            get { return (mzerp.Enums.MaterialOutstockType)Instance["OutstockType"].IntegerValue; }
            set { Instance["OutstockType"].IntegerValue = (int)value; }
        }

        public String RefTaskNo
        {
            get { return Instance["RefTaskNo"].StringValue; }
            set { Instance["RefTaskNo"].StringValue = value; }
        }

        private sys.Entities.Emploee _CreateBy;
        public sys.Entities.Emploee CreateBy
        {
            get
            {
                if (_CreateBy == null)
                {
                    Entity value = Instance["CreateBy"].EntityValue;
                    if (value == null)
                        return null;
                    _CreateBy = new sys.Entities.Emploee(value);
                }
                return _CreateBy;
            }
            set
            {
                _CreateBy = value;
                if (value == null)
                    Instance["CreateBy"].EntityValue = null;
                else
                    Instance["CreateBy"].EntityValue = _CreateBy.Instance;
            }
        }

        public Guid CreateByID
        {
            get { return Instance["CreateByID"].GuidValue; }
            set { Instance["CreateByID"].GuidValue = value; }
        }

        public DateTime CreateTime
        {
            get { return Instance["CreateTime"].DateValue; }
            set { Instance["CreateTime"].DateValue = value; }
        }

        public String Memo
        {
            get { return Instance["Memo"].StringValue; }
            set { Instance["Memo"].StringValue = value; }
        }

        private sys.Entities.Emploee _TakeBy;
        public sys.Entities.Emploee TakeBy
        {
            get
            {
                if (_TakeBy == null)
                {
                    Entity value = Instance["TakeBy"].EntityValue;
                    if (value == null)
                        return null;
                    _TakeBy = new sys.Entities.Emploee(value);
                }
                return _TakeBy;
            }
            set
            {
                _TakeBy = value;
                if (value == null)
                    Instance["TakeBy"].EntityValue = null;
                else
                    Instance["TakeBy"].EntityValue = _TakeBy.Instance;
            }
        }

        public Guid? TakeByID
        {
            get
            {
                if (Instance["TakeByID"].HasValue)
                    return Instance["TakeByID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["TakeByID"].Value = null;
                else
                    Instance["TakeByID"].GuidValue = value.Value;
            }
        }

        private mzerp.Entities.BizPartner _BizPartner;
        public mzerp.Entities.BizPartner BizPartner
        {
            get
            {
                if (_BizPartner == null)
                {
                    Entity value = Instance["BizPartner"].EntityValue;
                    if (value == null)
                        return null;
                    _BizPartner = new mzerp.Entities.BizPartner(value);
                }
                return _BizPartner;
            }
            set
            {
                _BizPartner = value;
                if (value == null)
                    Instance["BizPartner"].EntityValue = null;
                else
                    Instance["BizPartner"].EntityValue = _BizPartner.Instance;
            }
        }

        public Guid? BizPartnerID
        {
            get
            {
                if (Instance["BizPartnerID"].HasValue)
                    return Instance["BizPartnerID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["BizPartnerID"].Value = null;
                else
                    Instance["BizPartnerID"].GuidValue = value.Value;
            }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockItem> _Items;
        public dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockItem> Items
        {
            get
            {
                if (_Items == null)
                    _Items = new dps.Data.Mapper.EntityList<mzerp.Entities.MaterialOutstockItem>(Instance["Items"].EntityListValue);
                return _Items;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public MaterialOutstockTask() : base() { }

        public MaterialOutstockTask(Entity instance) : base(instance) { }
        #endregion

    }
}
