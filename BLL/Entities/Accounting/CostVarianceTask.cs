﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class CostVarianceTask : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.CostVarianceTask";

        #region ====Properties====
        public mzerp.Enums.CostVarianceSource Source
        {
            get { return (mzerp.Enums.CostVarianceSource)Instance["Source"].IntegerValue; }
            set { Instance["Source"].IntegerValue = (int)value; }
        }

        public String TaskNo
        {
            get { return Instance["TaskNo"].StringValue; }
        }

        private sys.Entities.Emploee _CreateBy;
        public sys.Entities.Emploee CreateBy
        {
            get
            {
                if (_CreateBy == null)
                {
                    Entity value = Instance["CreateBy"].EntityValue;
                    if (value == null)
                        return null;
                    _CreateBy = new sys.Entities.Emploee(value);
                }
                return _CreateBy;
            }
            set
            {
                _CreateBy = value;
                if (value == null)
                    Instance["CreateBy"].EntityValue = null;
                else
                    Instance["CreateBy"].EntityValue = _CreateBy.Instance;
            }
        }

        public Guid CreateByID
        {
            get { return Instance["CreateByID"].GuidValue; }
            set { Instance["CreateByID"].GuidValue = value; }
        }

        public DateTime CreateTime
        {
            get { return Instance["CreateTime"].DateValue; }
            set { Instance["CreateTime"].DateValue = value; }
        }

        public Guid SourceTaskID
        {
            get { return Instance["SourceTaskID"].GuidValue; }
            set { Instance["SourceTaskID"].GuidValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.CostVarianceItem> _Items;
        public dps.Data.Mapper.EntityList<mzerp.Entities.CostVarianceItem> Items
        {
            get
            {
                if (_Items == null)
                    _Items = new dps.Data.Mapper.EntityList<mzerp.Entities.CostVarianceItem>(Instance["Items"].EntityListValue);
                return _Items;
            }
        }

        private mzerp.Entities.Warehouse _Warehouse;
        public mzerp.Entities.Warehouse Warehouse
        {
            get
            {
                if (_Warehouse == null)
                {
                    Entity value = Instance["Warehouse"].EntityValue;
                    if (value == null)
                        return null;
                    _Warehouse = new mzerp.Entities.Warehouse(value);
                }
                return _Warehouse;
            }
            set
            {
                _Warehouse = value;
                if (value == null)
                    Instance["Warehouse"].EntityValue = null;
                else
                    Instance["Warehouse"].EntityValue = _Warehouse.Instance;
            }
        }

        public Guid WarehouseID
        {
            get { return Instance["WarehouseID"].GuidValue; }
            set { Instance["WarehouseID"].GuidValue = value; }
        }

        private sys.Entities.Emploee _ProcessBy;
        public sys.Entities.Emploee ProcessBy
        {
            get
            {
                if (_ProcessBy == null)
                {
                    Entity value = Instance["ProcessBy"].EntityValue;
                    if (value == null)
                        return null;
                    _ProcessBy = new sys.Entities.Emploee(value);
                }
                return _ProcessBy;
            }
            set
            {
                _ProcessBy = value;
                if (value == null)
                    Instance["ProcessBy"].EntityValue = null;
                else
                    Instance["ProcessBy"].EntityValue = _ProcessBy.Instance;
            }
        }

        public Guid? ProcessByID
        {
            get
            {
                if (Instance["ProcessByID"].HasValue)
                    return Instance["ProcessByID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ProcessByID"].Value = null;
                else
                    Instance["ProcessByID"].GuidValue = value.Value;
            }
        }

        public DateTime? ProcessTime
        {
            get
            {
                if (Instance["ProcessTime"].HasValue)
                    return Instance["ProcessTime"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ProcessTime"].Value = null;
                else
                    Instance["ProcessTime"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public CostVarianceTask() : base() { }

        public CostVarianceTask(Entity instance) : base(instance) { }
        #endregion

    }
}
