﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class CostVarianceMove : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.CostVarianceMove";

        #region ====Properties====
        private mzerp.Entities.CostVarianceItem _CostVarianceItem;
        public mzerp.Entities.CostVarianceItem CostVarianceItem
        {
            get
            {
                if (_CostVarianceItem == null)
                {
                    Entity value = Instance["CostVarianceItem"].EntityValue;
                    if (value == null)
                        return null;
                    _CostVarianceItem = new mzerp.Entities.CostVarianceItem(value);
                }
                return _CostVarianceItem;
            }
            set
            {
                _CostVarianceItem = value;
                if (value == null)
                    Instance["CostVarianceItem"].EntityValue = null;
                else
                    Instance["CostVarianceItem"].EntityValue = _CostVarianceItem.Instance;
            }
        }

        public Guid CostVarianceItemID
        {
            get { return Instance["CostVarianceItemID"].GuidValue; }
            set { Instance["CostVarianceItemID"].GuidValue = value; }
        }

        public mzerp.Enums.CostVarianceMoveTarget MoveTarget
        {
            get { return (mzerp.Enums.CostVarianceMoveTarget)Instance["MoveTarget"].IntegerValue; }
            set { Instance["MoveTarget"].IntegerValue = (int)value; }
        }

        public Guid TargetID
        {
            get { return Instance["TargetID"].GuidValue; }
            set { Instance["TargetID"].GuidValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public Decimal Value
        {
            get { return Instance["Value"].DecimalValue; }
            set { Instance["Value"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public CostVarianceMove() : base() { }

        public CostVarianceMove(Entity instance) : base(instance) { }
        #endregion

    }
}
