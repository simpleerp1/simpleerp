﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace mzerp.Entities
{
    public class CostVarianceItem : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "mzerp.CostVarianceItem";

        #region ====Properties====
        private mzerp.Entities.Material _Material;
        public mzerp.Entities.Material Material
        {
            get
            {
                if (_Material == null)
                {
                    Entity value = Instance["Material"].EntityValue;
                    if (value == null)
                        return null;
                    _Material = new mzerp.Entities.Material(value);
                }
                return _Material;
            }
            set
            {
                _Material = value;
                if (value == null)
                    Instance["Material"].EntityValue = null;
                else
                    Instance["Material"].EntityValue = _Material.Instance;
            }
        }

        public Guid MaterialID
        {
            get { return Instance["MaterialID"].GuidValue; }
            set { Instance["MaterialID"].GuidValue = value; }
        }

        public Decimal Quantity
        {
            get { return Instance["Quantity"].DecimalValue; }
            set { Instance["Quantity"].DecimalValue = value; }
        }

        public Decimal Variance
        {
            get { return Instance["Variance"].DecimalValue; }
            set { Instance["Variance"].DecimalValue = value; }
        }

        private mzerp.Entities.CostVarianceTask _CostVarianceTask;
        public mzerp.Entities.CostVarianceTask CostVarianceTask
        {
            get
            {
                if (_CostVarianceTask == null)
                {
                    Entity value = Instance["CostVarianceTask"].EntityValue;
                    if (value == null)
                        return null;
                    _CostVarianceTask = new mzerp.Entities.CostVarianceTask(value);
                }
                return _CostVarianceTask;
            }
            set
            {
                _CostVarianceTask = value;
                if (value == null)
                    Instance["CostVarianceTask"].EntityValue = null;
                else
                    Instance["CostVarianceTask"].EntityValue = _CostVarianceTask.Instance;
            }
        }

        public Guid CostVarianceTaskID
        {
            get { return Instance["CostVarianceTaskID"].GuidValue; }
            set { Instance["CostVarianceTaskID"].GuidValue = value; }
        }

        public String LotNo
        {
            get { return Instance["LotNo"].StringValue; }
            set { Instance["LotNo"].StringValue = value; }
        }

        private dps.Data.Mapper.EntityList<mzerp.Entities.CostVarianceMove> _MoveTargets;
        public dps.Data.Mapper.EntityList<mzerp.Entities.CostVarianceMove> MoveTargets
        {
            get
            {
                if (_MoveTargets == null)
                    _MoveTargets = new dps.Data.Mapper.EntityList<mzerp.Entities.CostVarianceMove>(Instance["MoveTargets"].EntityListValue);
                return _MoveTargets;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public CostVarianceItem() : base() { }

        public CostVarianceItem(Entity instance) : base(instance) { }
        #endregion

    }
}
